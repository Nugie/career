@extends('layouts.app')

@section('content')

<div class="page-content row">
	<div class="page-content-wrapper m-t">

		<div class="sbox">
			<div class="sbox-title">
				 <h4>{{ $pageTitle }} <small> {{ $pageNote }} </small> </h4> 
			</div>
			<div class="sbox-content">

<ul class="nav nav-tabs" >
  <li class="active"><a href="#applicantdocument" data-toggle="tab">{{ Lang::get('core.applicantdocument') }} </a></li>
   <li><a href="#fileattachment" data-toggle="tab">{{ Lang::get('core.fileattachment') }} </a></li>
  <li><a href="#info" data-toggle="tab"> {{ Lang::get('core.personalinfo') }} </a></li>
  <li ><a href="#pass" data-toggle="tab">{{ Lang::get('core.changepassword') }} </a></li>
<!--   <li><a href="#fileattachment" data-toggle="tab">{{ Lang::get('core.fileattachment') }} </a></li> -->
  
</ul>	

<div class="tab-content">
  <div class="tab-pane m-t" id="info">
	{!! Form::open(array('url'=>'user/saveprofile/', 'class'=>'form-horizontal validated' ,'files' => true)) !!}  
	  <div class="form-group">
		<label for="ipt" class=" control-label col-md-4"> Username </label>
		<div class="col-md-8">
		<input name="username" type="text" id="username" disabled="disabled" class="form-control input-sm" required  value="{{ $info->username }}" />  
		 </div> 
	  </div>  
	  <div class="form-group">
		<label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.email') }} </label>
		<div class="col-md-8">
		<input name="email" type="text" id="email"  class="form-control input-sm" value="{{ $info->email }}" /> 
		 </div> 
	  </div> 	  
	  
	  <div class="form-group">
		<label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.lastname') }} </label>
		<div class="col-md-8">
		<input name="last_name" type="text" id="last_name" class="form-control input-sm" required value="{{ $info->last_name }}" />  
		<h6 style="font-size: 10px;">(untuk pengisian nama dengan 1 suku kata isi pada kolom lastname)</h6>
		 </div> 
	  </div> 
  
	  <div class="form-group">
		<label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.firstname') }} (optional)</label>
		<div class="col-md-8">
		<input name="first_name" type="text" id="first_name" class="form-control input-sm" value="{{ $info->first_name }}" />
		<h6 style="font-size: 10px;">(untuk pengisian nama dengan 3 suku kata atau lebih maka firstname isi dengan suku kata pertama, sisanya pada lastname)</h6> 
		 </div> 
	  </div>    
	  
	  <div class="form-group">
		<label for="ipt" class=" control-label col-md-4">{{ Lang::get('core.ApplicantStatus') }} </label>
		<div class="col-md-8">
		<input name="ApplicantStatus" type="text" id="ApplicantStatus" disabled="disabled" class="form-control input-sm"  value="{{ !empty($applicantData->ApplicantStatus) ? $applicantData->ApplicantStatus : '' }}" /> 
		 </div> 
	  </div>  

	  <div class="form-group">
		<label for="sub" class=" control-label col-md-4">Subscribe Email Vacancy </label>
		<div class="col-md-8">
		<input type="radio" name="sub" value="subscribe" required <?php if($info->subscribe == 'subscribe'){
            echo "checked"; }?>> Subscribe<br>
  		<input type="radio" name="sub" value="unsubscribe" required <?php if($info->subscribe == 'unsubscribe'){
            echo "checked"; }?>> Unsubscribe<br>
		 </div> 
	  </div>  
	  
	  <div class="form-group  " >
		<label for="ipt" class=" control-label col-md-4 text-right"> Avatar </label>
		<div class="col-md-8">
			
			<input type="file" name="avatar" id="avatar" class="inputfile" />
			<label for="avatar"><i class="fa fa-upload"></i> Choose a file</label>
			<div class="avatar_preview"></div>

		 Image Dimension 80 x 80 px <br />
		 	<?php if( file_exists( './uploads/users/'.$info->avatar) && $info->avatar !='') { ?>
            <img src="{{  url('uploads/users').'/'.$info->avatar }} " border="0" width="60" class="img-circle" />
            <?php  } else { ?> 
            <img alt="" src="http://www.gravatar.com/avatar/{{ md5($info->email) }}" width="60" class="img-circle" />
            <?php } ?>

		
		
		 </div> 
	  </div>  

	  <div class="form-group">
		<label for="ipt" class=" control-label col-md-4">&nbsp;</label>
		<div class="col-md-8">
			<button class="btn btn-success" type="submit"> {{ Lang::get('core.sb_savechanges') }}</button>
		 </div> 
	  </div> 	
	
	{!! Form::close() !!}	
  </div>

  <div class="tab-pane  m-t" id="pass">
	{!! Form::open(array('url'=>'user/savepassword/', 'class'=>'form-horizontal ')) !!}    

	  <div class="form-group">
		<label for="ipt" class=" control-label col-md-4"> {{ Lang::get('core.oldpassword') }} </label>
		<div class="col-md-8">
		<input name="old_password" type="password" id="old_password" class="form-control input-sm" value="" required /> 
		 </div> 
	  </div>  
	  
	  <div class="form-group">
		<label for="ipt" class=" control-label col-md-4"> {{ Lang::get('core.newpassword') }} </label>
		<div class="col-md-8">
		<input name="password" type="password" id="password" class="form-control input-sm" value="" /> 
		 </div> 
	  </div>  
	  
	  <div class="form-group">
		<label for="ipt" class=" control-label col-md-4"> {{ Lang::get('core.conewpassword') }}  </label>
		<div class="col-md-8">
		<input name="password_confirmation" type="password" id="password_confirmation" class="form-control input-sm" value="" />  
		 </div> 
	  </div>    
	 
	
	  <div class="form-group">
		<label for="ipt" class=" control-label col-md-4">&nbsp;</label>
		<div class="col-md-8">
			<button class="btn btn-danger" type="submit"> {{ Lang::get('core.sb_savechanges') }} </button>
		 </div> 
	  </div>   
	{!! Form::close() !!}	
  </div>
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

  
  <!-- applicant document -->
  <div class="tab-pane active m-t" id="applicantdocument">
	{!! Form::open(array('url'=>'user/saveapplicantdata/', 'class'=>'form-horizontal ','id' => 'cstForm')) !!}    
	@if (count($applicantData) > 0)
	{!! Form::hidden('id', $applicantData->id) !!}
	@endif
	{!! Form::hidden('id_user', \Session::get('uid')) !!}
<?php /* ?>	{!! Form::hidden('id_inaddinfo', $interestaddinfo->id_interest_addinfo) !!} <?php */ ?>
<?php 
	if(Auth::check()){ 
		$userId = Auth::user()->id; 
		$full_profile= AccHelpers::flag_profile($userId);
		if($full_profile){
			echo "<p style='font-size:14px;padding:0 10px;background-color:#fce8b0;border-radius:10px;'><i class='fa fa-exclamation' style='color:#ff4d4d;'></i> Please complete your Personal Details and Educational Background</p>";
		}
	}
?>		
<h5 class="headerlebel" style="margin-left: 40px;  background-color: #d0cece;
    color: #333333;border-radius: 10px;padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px;">Personal Details</h5>

    <!-- start Personal Details -->
   @if (count($applicantData) > 0)
	<div class="form-group  " >
		<label for="title_profile" class=" control-label col-md-4"> {{ Lang::get('core.title_profile')}} <span class="asterix"> * </span></label>
			<div class="col-md-8">
				<?php $title_profile = explode(',',$applicantData->title_profile);
				$title_opt = array( 'Mr.' => 'Mr.' ,  'Mrs.' => 'Mrs.' ,  'Ms.' => 'Ms.' ,); ?>
 					<select name='title_profile' rows='5' required  class='select2 ' id="title_profile" > 
						<?php 
						foreach($title_opt as $key=>$val)
					{
					    echo "<option class='title_option' value ='$key' ".($applicantData->title_profile == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?>
					</select>  					
				</div> 
	</div>
	<div class="form-group">
		<label for="gender" class=" control-label col-md-4">{{ Lang::get('core.gender') }} <span class="asterix"> * </span></label>
		<div class="col-md-8">
		<input type="radio" name="gender" value="male" required <?php if(in_array('male', $explode_gender)){
            echo "checked"; }?>> Male<br>
  		<input type="radio" name="gender" value="female" required <?php if(in_array('female', $explode_gender)){
            echo "checked"; }?>> Female<br>
		<!--<input name="gender" type= id="gender" class="form-control input-sm" required value="{{ $applicantData->gender }}" />-->		 
		 </div> 
	  </div>
	  <div class="form-group  " >
		<label for="dateofbirth" class=" control-label col-md-4">{{ Lang::get('core.dateofbirth') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='date' name='dateofbirth' id='dateofbirth' value="{{ $applicantData->dateofbirth }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="provinceofbirth" class=" control-label col-md-4">{{ Lang::get('core.provinceofbirth') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='provinceofbirth' id='provinceofbirth' value="{{ $applicantData->provinceofbirth }}" pattern="^[A-Za-z -]+$" title="Province of Birth harus Huruf semua"   required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="cityofbirth" class=" control-label col-md-4">{{ Lang::get('core.cityofbirth') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='cityofbirth' id='cityofbirth' pattern="^[A-Za-z -]+$" title="City Of Birth harus Huruf semua" value="{{ $applicantData->cityofbirth }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="identitycardnumber" class=" control-label col-md-4">{{ Lang::get('core.identitycardnumber') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='identitycardnumber' id='identitycardnumber' value="{{ $applicantData->identitycardnumber }}" pattern="[0-9]{16}" title="Identity Card Number harus berupa angka dengan panjang 16 karakter" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="homephone" class=" control-label col-md-4">{{ Lang::get('core.homephone') }} (optional)</label>
			<div class="col-md-8">
			<input  type='text' name='homephone' id='homephone' pattern="[0-9]*" title="Homephone harus Angka" value="{{ $applicantData->homephone }}"     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="mobilephone" class=" control-label col-md-4">{{ Lang::get('core.mobilephone') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='mobilephone' id='mobilephone' pattern="[0-9]*" title="Mobile Phone harus Angka" value="{{ $applicantData->mobilephone }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="npwp" class=" control-label col-md-4">{{ Lang::get('core.npwp') }} <span class="asterix">  </span></label>
			<div class="col-md-8">
			<input  type='text' name='npwp' pattern="[0-9]{15}" title="NPWP harus Angka dengan panjang 15 karakter" value="{{ $applicantData->npwp}}"      
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	 <div class="form-group  " >
		<label for="maritalstatus" class=" control-label col-md-4">{{ Lang::get('core.maritalstatus') }}<span class="asterix"> * </span></label>
		<div class="col-md-8">
			<?php $default = "Single";
				$maritalstatus = explode(',',$applicantData->maritalstatus);
			     $maritalstatus_opt = array( 'Widowed' => 'Widowed' ,  'Divorced' => 'Divorced' ,  'Married' => 'Married' , 'Single' => 'Single'); ?>
					<select name='maritalstatus' id="maritalstatus" rows='5' required  class='select2 ' onchange="yesnoCheck(this);" > 
						<?php 
						foreach($maritalstatus_opt as $key=>$val)
						{
						    echo "<option class='marital_option' value ='$key' ".($applicantData->maritalstatus == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
			?></select> 
		</div> 
	</div>
	    	<div id="ifYes" style="display: none;" class="form-group  "> 
	    	<label for="marrieddate" class=" control-label col-md-4">{{ Lang::get('core.marrieddate') }} <span class="asterix"> * </span></label>
              <div class="col-md-8">                 
        			<input  type='date' name='marrieddate' id='marrieddate' value="{{$applicantData->marrieddate}}" 
        			    class='form-control input-sm ' />
              </div> 
                </div>
	<div class="form-group  " >
		<label for="religion" class=" control-label col-md-4">{{ Lang::get('core.religion') }}<span class="asterix"> * </span></label>
		<div class="col-md-8">
			<?php $religion = explode(',',$applicantData->religion);
			$religion_opt = array( 'Hindu' => 'Hindu' ,  'Buddha' => 'Buddha' ,  'Catholic' => 'Catholic' , 'Christian' => 'Christian' , 'Konghucu' => 'Konghucu' ,  'Moslem' => 'Moslem'); ?>
					<select name='religion' rows='5' required  class='select2 ' id='religion' > 
						<?php 
						foreach($religion_opt as $key=>$val)
						{
						    echo "<option class='rg_option' value ='$key' ".($applicantData->religion == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
			?></select> 
		</div> 
	</div>
	
	<!-- End Personal Details -->  
 
	
	@else
<div class="form-group  " >
		<label for="title_profile" class=" control-label col-md-4"> {{ Lang::get('core.title_profile')}} <span class="asterix"> * </span></label>
			<div class="col-md-8">
				<?php 
				$title_opt = array( 'Mr.' => 'Mr.' ,  'Mrs.' => 'Mrs.' ,  'Ms.' => 'Ms.' ,); ?>
 					<select name='title_profile' rows='5' required  class='select2 ' id="title_profile" > 
						<?php 
						foreach($title_opt as $key=>$val)
					   {
					    echo "<option  value ='$key' 
                                ".($val == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?>
					</select>  					
				</div> 
	</div>
	
	
	<div class="form-group">
		<label for="gender" class=" control-label col-md-4">{{ Lang::get('core.gender') }} <span class="asterix"> * </span></label>
		<div class="col-md-8">
		<input type="radio" name="gender" value="male"> Male<br>
  		<input type="radio" name="gender" value="female"> Female<br>		 
		 </div> 
	  </div>
	  	  
	  
	  <div class="form-group  " >
		<label for="dateofbirth" class=" control-label col-md-4">{{ Lang::get('core.dateofbirth') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='date' name='dateofbirth' id='dateofbirth' value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="provinceofbirth" class=" control-label col-md-4">{{ Lang::get('core.provinceofbirth') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='provinceofbirth' id='provinceofbirth' pattern="^[A-Za-z -]+$" title="Add Province Of Birth Must Contain only letters." value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="cityofbirth" class=" control-label col-md-4">{{ Lang::get('core.cityofbirth') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='cityofbirth' id='cityofbirth' pattern="^[A-Za-z -]+$" title="Add City Of Birth Must Contain only letters." value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="identitycardnumber" class=" control-label col-md-4">{{ Lang::get('core.identitycardnumber') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='identitycardnumber' id='identitycardnumber' pattern="[0-9]{16}" title="Identity Card Number harus berupa angka dengan panjang 16 karakter" value="" required     
										  class='form-control input-sm ' /> 
			</div> 
			<p id="demo"></p>
	  </div>
	  <div class="form-group  " >
		<label for="homephone" class=" control-label col-md-4">{{ Lang::get('core.homephone') }} (optional)</label>
			<div class="col-md-8">
			<input  type='text' name='homephone' id='homephone' pattern="[0-9]*" title="Home Phone harus Angka" value=""     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="mobilephone" class=" control-label col-md-4">{{ Lang::get('core.mobilephone') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='mobilephone' id='mobilephone' pattern="[0-9]*" title="Mobile Phone harus Angka" value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	   <div class="form-group  " >
		<label for="npwp" class=" control-label col-md-4">{{ Lang::get('core.npwp') }} <span class="asterix">  </span></label>
			<div class="col-md-8">
			<input  type='text' name='npwp' id='npwp' pattern="[0-9]{15}" title="NPWP harus Angka dengan panjang 15 karakter" value="" 
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	 <div class="form-group  " >
		<label for="maritalstatus" class=" control-label col-md-4">{{ Lang::get('core.maritalstatus') }}<span class="asterix"> * </span></label>
		<div class="col-md-8">
			<?php
				$default = "Single";
			     $maritalstatus_opt = array( 'Widowed' => 'Widowed' ,  'Divorced' => 'Divorced' ,  'Married' => 'Married' , 'Single' => 'Single'); ?>
					<select name='maritalstatus' id="maritalstatus" rows='5' required  class='select2 ' onchange="yesnoCheck(this);" > 
						<?php 
						foreach($maritalstatus_opt as $key=>$val)
						{
						    echo "<option class='marital_option' value ='$key' ".($val == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
			?></select> 
		</div> 
	</div>
	    	<div id="ifYes" style="display: none;" class="form-group  "> 
	    	<label for="marrieddate" class=" control-label col-md-4">{{ Lang::get('core.marrieddate') }} <span class="asterix"> * </span></label>
              <div class="col-md-8">                 
        			<input  type='date' name='marrieddate' id='marrieddate' value="" 
        			    class='form-control input-sm ' />
              </div> 
                </div>
	 <div class="form-group  " >
		<label for="religion" class=" control-label col-md-4">{{ Lang::get('core.religion') }}<span class="asterix"> * </span></label>
		<div class="col-md-8">
			<?php
			$religion_opt = array( 'Hindu' => 'Hindu' ,  'Buddha' => 'Buddha' ,  'Catholic' => 'Catholic' , 'Christian' => 'Christian' , 'Konghucu' => 'Konghucu' ,  'Moslem' => 'Moslem'); ?>
					<select name='religion' rows='5' required  class='select2 ' id="religion" > 
						<?php 
						foreach($religion_opt as $key=>$val)
						{
						    echo "<option class='rg_option' value ='$key' ".($val == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
			?></select> 
		</div> 
	</div>
	
	@endif
	

	
	
	
	
	
	
	
	
	
	    <!-- start home address -->
	  <h5 class="headerlebel" style="margin-left: 40px;  background-color: #d0cece;
    color: #333333;border-radius: 10px;padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px;">Home Address</h5>
	  	  @if (count($applicantData) > 0)
	  	  <div class="form-group  " >
		<label for="country" class=" control-label col-md-4">{{ Lang::get('core.country') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='country' id='country' value="Indonesia" required     
										  class='form-control input-sm ' disabled /> 
			</div> 
	  </div>
	  <!--value country indonesia, disabled-->
	  <div class="form-group  " >
		<label for="address" class=" control-label col-md-4" >{{ Lang::get('core.address') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='address' id='address' value="{{ $applicantData->address }}" required     
										  class='form-control input-sm ' /> 
			<!-- <textarea class="field" name="notes" cols="111" rows="5" style="width:605px" value="{{ $applicantData->address }}"></textarea> -->
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="subdisctrict" class=" control-label col-md-4">{{ Lang::get('core.subdisctrict') }} (Kecamatan)<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='subdisctrict' id='subdisctrict' pattern="^[A-Za-z -]+$" title="Sub Disctrict Must Contain only letters." value="{{ $applicantData->subdisctrict }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="cityorregency" class=" control-label col-md-4">{{ Lang::get('core.cityorregency') }} (Kota / Kabupaten)<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='cityorregency' id='cityorregency' pattern="^[A-Za-z -]+$" title="City or Regency Must Contain only letters." value="{{ $applicantData->cityorregency }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="province" class=" control-label col-md-4">{{ Lang::get('core.province') }} (Provinsi)<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='province' id='province' pattern="^[A-Za-z -]+$" title="Province Must Contain only letters." value="{{ $applicantData->province }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="postalcode" class=" control-label col-md-4">{{ Lang::get('core.postalcode') }} (Kode POS) <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='postalcode' id='postalcode' pattern="[0-9]{5}" title="Postal Code Harus angka dengan 5 karakter" value="{{ $applicantData->postalcode }}" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  	  @else
	  	  
	  	  <div class="form-group  " >
		<label for="country" class=" control-label col-md-4">{{ Lang::get('core.country') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='country' id='country' value="Indonesia" required     
										  class='form-control input-sm ' disabled /> 
			</div> 
	  </div>
	  <!--value country indonesia, disabled-->
	  <div class="form-group  " >
		<label for="address" class=" control-label col-md-4" >{{ Lang::get('core.address') }} <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='address' id='address' value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="subdisctrict" class=" control-label col-md-4">{{ Lang::get('core.subdisctrict') }} (Kecamatan)<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='subdisctrict' id='subdisctrict' pattern="^[A-Za-z -]+$" title="Sub Disctrict Must Contain only letters." value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="cityorregency" class=" control-label col-md-4">{{ Lang::get('core.cityorregency') }} (Kota / Kabupaten)<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='cityorregency' id='cityorregency' pattern="^[A-Za-z -]+$" title="City or Regency Must Contain only letters." value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="province" class=" control-label col-md-4">{{ Lang::get('core.province') }} (Provinsi)<span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='province' id='province' pattern="^[A-Za-z -]+$" title="Province Must Contain only letters." value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  <div class="form-group  " >
		<label for="postalcode" class=" control-label col-md-4">{{ Lang::get('core.postalcode') }} (Kode POS) <span class="asterix"> * </span></label>
			<div class="col-md-8">
			<input  type='text' name='postalcode' id='postalcode'  pattern="[0-9]{5}" title="Add Postal Code Must Contain only Number with 5 charaters" value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>
	  	  
	  	  @endif
		  	  	 <!-- start educational background -->
    	
 <h5 class="headerlebel" style="margin-left: 40px;  background-color: #d0cece;
    color: #333333;border-radius: 10px;padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px;">Educational Background</h5>
	  	      <div class="form-group">	
		<div class="table-responsive" style=" padding-right: 20px; padding-left: 20px;">
		<table class="table table-striped">
		<thead>
                       <tr>
                               <th>Last Education</th>
                               <th>University or School</th>
                               <th>Others</th>
                               <th>Faculty</th>
                               <th>Major</th>
                               <th>Gpa</th>
                               <th>From</th>
                               <th>Until</th>
                               <th colspan="2">Action</th>
                       </tr>
               </thead>
               @if (count($educationalbackData) >= 1)
			<tbody>
			 @foreach($educationalbackData as $items_edu)
			<tr>
                               <td>{{$items_edu->lasteducation}}</td>
                               <td>{{$items_edu->name}}</td>
                               <td>{{$items_edu->otherunivorschool}}</td>
                               <td>{{$items_edu->faculty}}</td>
                               <td>{{$items_edu->major}}</td>
                               <td>{{$items_edu->gpa}}</td>
                               <td>{{$items_edu->startdate}}</td>
                               <td>{{$items_edu->endate}}</td>
                               <td><a href="{{ url('profile/edit_edu_exp/'. $items_edu->id_edu_back) }}" class="fa fa-pencil"></a></td>                               
                               <td><a type="button" onclick="SximoConfirmDelete('{{ url('user/deleteedu/'.$items_edu->id_edu_back) }}')" class="fa fa-trash"></a></td>
                               
             </tr>
             @endforeach
			</tbody>
			  @else
			<tbody>
				<tr><td colspan="9">No Data</td></tr>
			</tbody> 
    	  @endif
		</table>
        </div>      
  </div>
          <div class="form-group example">
		<label for="ipt" class=" control-label col-md-10">&nbsp;</label>
		 <div class="col-md-1" style="padding-left: 50px;padding-right: 0px;">
			<a href="{{ url('profile/add_edu_exp') }}" style="color:white!important"><button type="button" name="addnew_edu" id="addnew_edu" class="btn btn-success" style="color:white!important">Add Others </button></a>
		 </div>
	  </div>

    <!-- end educational background -->	
    
  <!-- Organizational EXP -->
  	  <h5 class="headerlebel" style="margin-left: 40px;  background-color: #d0cece;
    color: #333333;border-radius: 10px;padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px;">Organizational Experiences</h5>  
    
	  	      <div class="form-group">	
		<div class="table-responsive" style=" padding-right: 20px; padding-left: 20px;">
		<table class="table table-striped">
		<thead>
                       <tr>
                               <th>Name</th>
                               <th>Scope</th>
                               <th>Startdate</th>
                               <th>Endate</th>
                               <th>Role Function</th>
                               <th>Role or Position</th>
                               <th colspan="2">Action</th>
                       </tr>
               </thead>
               @if (count($organiexpData) >= 1)
			<tbody>
			 @foreach($organiexpData as $items)
			<tr>
                               <td>{{$items->organizationname}}</td>
                               <td>{{$items->organizationscope}}</td>
                               <td>{{$items->organization_experience_startdate}}</td>
                               <td>{{$items->organization_experience_endate}}</td>
                               <td>{{$items->rolefunction}}</td>
                               <td>{{$items->roleorposition}}</td>
                               <td><a href="{{ url('profile/organi_exp/'. $items->id_org_exp) }}" class="fa fa-pencil"></a></td>
                               <td><a type="button" onclick="SximoConfirmDelete('{{ url('user/deleteorg/'.$items->id_org_exp) }}')" class="fa fa-trash"></a></td>
                               
             </tr>
             @endforeach
			</tbody>
			  @else
    		<tbody>
				<tr><td colspan="7">No Data</td></tr>
			</tbody>  
    	  @endif
		</table>
        </div>      
  </div>
         <div class="form-group example">
		<label for="ipt" class=" control-label col-md-10">&nbsp;</label>
		 <div class="col-md-1" style="padding-left: 50px;padding-right: 0px;">
			<a href="{{ url('profile/add_organi_exp') }}" style="color:white!important"><button type="button" name="addnew_org" id="addnew_org" class="btn btn-success" style="color:white!important">Add Others</button></a>
		 </div>
	  </div>

	 <!-- Organizational EXP -->
	 
	<!-- Working Experience -->  
	 
	   <h5 class="headerlebel" style="margin-left: 40px;  background-color: #d0cece;
    color: #333333;border-radius: 10px;padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px;">Working Experiences</h5>
    
	  	      <div class="form-group">	
		<div class="table-responsive" style=" padding-right: 20px; padding-left: 20px;">
		<table class="table table-striped">
		<thead>
                       <tr>
                                <th>Company</th>
                               <th>Start Date</th>
                               <th>End Date</th>
                               <th>Position</th>
                               <th>Category</th>
                               <th>Status</th>
                               <th>Job Description</th>
                               <!--<th>Salary</th>-->
                               <th>Reason of leaving</th>
                               <th colspan="2">Action</th>
                       </tr>
               </thead>
              @if (count($workexpData) >= 1)
			<tbody>
			@foreach($workexpData as $items)
			<tr>
                               <td>{{$items->company}}</td>
                               <td>{{$items->workingexperienceperiodstartdate}}</td>
                               <td>{{$items->workingexperienceperiodenddate}}</td>
                               <td>{{$items->position}}</td>
                               <td>{{$items->category}}</td>
                               <td>{{$items->status_working_experience}}</td>
                               <td>{{$items->jobdescription}}</td>
                               <!--<td>{{$items->salary}}</td>-->
                               <td>{{$items->Reasonofleaving}}</td>
                               <td><a href="{{ url('profile/edit_work_exp/'. $items->id_wor_exp) }}" class="fa fa-pencil"></a></td>
                               <td><a type="button" onclick="SximoConfirmDelete('{{ url('user/delete/'.$items->id_wor_exp) }}')" class="fa fa-trash"></a></td>
                               
             </tr>
             @endforeach
			</tbody>
			  @else
    			<tbody>
				<tr><td colspan="10">No Data</td></tr>
			</tbody>
    	  @endif
		</table>
        </div>      
  </div>
       <div class="form-group example">
		<label for="ipt" class=" control-label col-md-10">&nbsp;</label>
		 <div class="col-md-1" style="padding-left: 50px;padding-right: 0px;">
			<a href="{{ url('profile/add_work_exp') }}" style="color:white!important"><button type="button" name="addnew_work" id="addnew_work" class="btn btn-success" style="color:white!important">Add Others</button></a>
		 </div>
	  </div>
	  
        <!-- Working Experience -->	  	          
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <!-- your interest -->      
        
        	<h5 class="headerlebel" style="margin-left: 40px;  background-color: #d0cece;
    color: #333333;border-radius: 10px;padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px;">Your Interests</h5>
    <h5 style="margin-left: 40px; padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px; font-style:italic;">Career Preference (Thick Max. 5 preferences)</h5>   	          
    	         

     <div class="form-group" >    
    	<div class="col-md-8" id="yourinterests" style="width: 461px; margin-left: 77px; margin-right: 26px;">
		@if(count($jobfield) > 0)
			@foreach($jobfield as $cat)
				   <div>
						<input type="checkbox" name="careerpreference[]" value={{ $cat->id }} class="cps_options" <?php if(in_array($cat->id, $explode_career)){
            echo "checked"; }?> >
						<label for={{ $cat->id }}>{{ $cat->jobfield }}</label>
					</div>
			@endforeach
		@endif
        </div>   
        </div>   
          
        <!-- your interest -->
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
      
	  
	  <!-- expected salary --> 
	  @if (count($applicantData) > 0)
	   <div class="form-group  " >
<!-- edit format number -->
	   <div class="row" id="showsalary" class="col-md-5">
		   <label for="expectedsalaryfrom_view" class=" control-label col-md-2" style="width:316px;">{{ Lang::get('core.expectedsalaryfrom') }} <span class="asterix"> * </span></label>
			<div class="col-md-2">					
			<input  type='text' name='expectedsalaryfrom_view' id='expectedsalaryfrom_view' value="{{ number_format($applicantData->expectedsalaryfrom,2) }}" required     
										  class='form-control input-sm ' /> 
			</div>
			
			<label for="expectedsalaryupto_view" class=" control-label col-md-3" style="width: 161px;;">{{ Lang::get('core.expectedsalaryupto') }} <span class="asterix"> * </span></label>
			<div class="col-md-3">
				<input  type='text' name='expectedsalaryupto_view' id='expectedsalaryupto_view' value="{{ number_format($applicantData->expectedsalaryupto, 2) }}" required     
										  class='form-control input-sm ' /> 
			</div>
			<td><a class="fa fa-pencil" id='clicker'></a></td> 
			</div>
							
			
			<div class="row" id="sendsalary" class="col-md-5">
			    			<label for="expectedsalaryfrom" class=" control-label col-md-2" style="width:316px;">{{ Lang::get('core.expectedsalaryfrom') }} <span class="asterix"> * </span></label>
			<div class="col-md-2">					
    			<input  type='text' name='expectedsalaryfrom' id='expectedsalaryfrom' pattern="[0-9]*" title="Expected Salary From harus Angka tanpa Koma dan titik" value="{{$applicantData->expectedsalaryfrom}}" required     
    										  class='form-control input-sm ' /> 
			</div>	
			    			<label for="expectedsalaryupto" class=" control-label col-md-3" style="width: 161px;;">{{ Lang::get('core.expectedsalaryupto') }} <span class="asterix"> * </span></label>
			<div class="col-md-3">
    			<input  type='text' name='expectedsalaryupto' id='expectedsalaryupto ' pattern="[0-9]*" title="Expected Salary Up To harus Angka tanpa Koma dan titik" value="{{$applicantData->expectedsalaryupto}}" required     
    										  class='form-control input-sm ' /> 
			</div> 
			<td><a class="fa fa-floppy-o" id='save'></a></td>
			</div>	
			<!-- edit format number -->	
	  </div>   
	  @else
	   <div class="form-group  " >
		<label for="expectedsalaryfrom" class=" control-label col-md-3" style="width:316px;">{{ Lang::get('core.expectedsalaryfrom') }} <span class="asterix"> * </span></label>
			<div class="col-md-3">			
			<input  type='number' name='expectedsalaryfrom' id='expectedsalaryfrom' pattern="[0-9]*"  title="Expected Salary From harus Angka tanpa Koma dan titik" value="" required     
										  class='form-control input-sm ' /> 
			</div>
			<label for="expectedsalaryupto" class=" control-label col-md-3" style="width: 161px;;">{{ Lang::get('core.expectedsalaryupto') }} <span class="asterix"> * </span></label>
			<div class="col-md-3">
			<input  type='number' name='expectedsalaryupto' id='expectedsalaryupto' pattern="[0-9]*" title="Expected Salary Up To harus Angka tanpa Koma dan titik" value="" required     
										  class='form-control input-sm ' /> 
			</div> 
	  </div>   
	  
	  @endif
	  <!-- expected salary -->
	  
	  
	  <!-- additional information -->
	  <h5 class="headerlebel" style="margin-left: 40px;  background-color: #d0cece;
        color: #333333;border-radius: 10px;padding-left: 30px;padding-right: 30px;padding-top: 1px;padding-bottom: 1px;">Additional Information</h5>
	  @if (count($applicantData) > 0)
	  
	  <div class="form-group  " >
		<label for="optyfrom" class=" control-label col-md-4">{{ Lang::get('core.optyfrom') }}<span class="asterix"> * </span></label>
			<div class="col-md-8">
				<?php $optyfrom = explode(',',$applicantData->optyfrom);
				$optyfrom_opt = array( 'Campus/University Career Center' => 'Campus/University Career Center' ,  'Company/Brand Website' => 'Company/Brand Website' ,  'Print Advertisement' => 'Print Advertisement' , 'Friends/Relatives' => 'Friends / Relatives' ,  'ACC Employee' => 'ACC Employee' , 'Others' => 'Others' ,); ?>
					<select name='optyfrom' rows='5' required  class='select2 ' id='optyfrom' > 
						<?php 
						foreach($optyfrom_opt as $key=>$val)
						{
						    echo "<option  value ='$key' ".($applicantData->optyfrom == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
				</div> 
			</div>
	  
	  @else
    	<div class="form-group  " >
    		<label for="optyfrom" class=" control-label col-md-4">{{ Lang::get('core.optyfrom') }}<span class="asterix"> * </span></label>
    			<div class="col-md-8">
    				<?php 
    				$optyfrom_opt = array( 'Campus/University Career Center' => 'Campus/University Career Center' ,  'Company/Brand Website' => 'Company/Brand Website' ,  'Print Advertisement' => 'Print Advertisement' , 'Friends/Relatives' => 'Friends / Relatives' ,  'ACC Employee' => 'ACC Employee' , 'Others' => 'Others' ,); ?>
    					<select name='optyfrom' rows='5' required  class='select2 ' id='optyfrom' > 
    						<?php 
    						foreach($optyfrom_opt as $key=>$val)
    						{
    						    echo "<option  value ='$key' ".($val == $key ? " selected='selected' " : '' ).">$val</option>"; 
    						}						
    						?></select> 
    				</div> 
    			</div>
	  @endif
	  
	  <!-- additional information --> 
        
		<div class="form-group  " >
		<div class="col-md-12" style="margin-left: 77px; margin-right: 26px;">
        <input type="checkbox" name="terms" value="agreed" id="terms" onclick="if(!this.form.terms.checked){alert('You must agree to the terms first.');return false}" required /> Here by I delare that all the information above are true and correct. I am responsible for any untrue statement.
        </div>
        </div>
	  
	<div class="form-group">
		<label for="ipt" class=" control-label col-md-4">&nbsp;</label>
		<div class="col-md-8 example">
			<button class="btn btn-danger" type="submit" id="submit"> {{ Lang::get('core.sb_savechanges') }} </button>
		 </div> 
	  </div>   
 
	{!! Form::close() !!}	
  </div>
  <!--backup applicantdocument-->  
  
  
  
  
  
  
    <!-- file attachment -->
  <div class="tab-pane  m-t" id="fileattachment" style="height:1000px !important;">
	{!! Form::open(array('route' => 'filestore','files'=>true)) !!} 
    
    <div class="col-md-12 form-group">
	<h6 style="color: green">Please upload a valid image or file. Size of image or file should not be more than 2MB.</h6>
			<br> 
		<?php $jenisFile = ['surat_lamaran','cv','ijazah','transkrip_nilai','sertifikat_training','surat_keterangan_kerja','skck','ktp','sim','paspor','akte_nikah','kartu_keluarga','akte_kelahiran_diri_sendiri','akte_kelahiran_anak_1','akte_kelahiran_anak_2','akte_kelahiran_anak_3','npwp','bpjs','foto_latar_belakang_orange','nomor_rekening_permata','surat_pernyataan_orang_tua'];?>
		<div class="table-responsive">
		<table class="table table-striped">
			<tbody>
		@foreach($jenisFile as $jns)
		<?php $label = strtoupper(str_replace("_"," ",$jns)); ?>
			<tr>
				<td width='20%' class='label-view'>
					@if(count($fileattachData) > 0)
								@foreach($fileattachData as $file)
									@if($file->type_file == $jns)
										@if(!empty($file->link_file))
									<i class="fa fa-check-square-o" style="color: green;font-size: 21px;"></i>  
										@endif	
									@endif								
								@endforeach
				@endif
				{{ $label }}

				@if($label == "IJAZAH")
				<span class="asterix"> * </span>
				@endif
				@if($label == "SKCK")
				<span class="asterix"> * </span>
				@endif
				@if($label == "KTP")
				<span class="asterix"> * </span>
				@endif
				@if($label == "KARTU KELUARGA")
				<span class="asterix"> * </span>
				@endif
				@if($label == "NPWP")
				<span class="asterix"> * </span>
				@endif
				@if($label == "NOMOR REKENING PERMATA")
				<span class="asterix"> * </span>
				@endif
				@if($label == "FOTO LATAR BELAKANG ORANGE")
				<span class="asterix"> * </span>
				@endif
				@if($label == "AKTE KELAHIRAN DIRI SENDIRI")
				<span class="asterix"> * </span>
				@endif
			</td>
                <td>  
							@if(count($fileattachData) > 0)
								@foreach($fileattachData as $file)
									@if($file->type_file == $jns)
										@if(!empty($file->link_file))
									<a href={{ $file->link_file }} title="Download file" ><i class="fa fa-download"></i></a>
									<a type="button" onclick="SximoConfirmDelete('{{ url('user/deletefileattchmentprofile/'.$file->id_file) }}')">
									<i class="fa fa-trash" style="color: red;font-size:12px!important;"></i>
									</a>
										@endif
									@endif
								@endforeach
							@endif		
                </td>   
				<td>
                  	 <input type="file" name={{ $jns }} id={{ $jns }} class="inputfile"/>
                </td>   
				</tr>
		@endforeach
			</tbody>
		</table>
        </div>
       <br>
       <div class="col-md-12 imgdiv" style="text-align: center">
            <button type="submit" class="btn btn-primary">Submit All</button>
       </div>
       
  </div>

    
    {!! Form::close() !!}
  </div>
  <!-- file attachment -->
  
  
  
  
  
  
  
  
  
   
			</div>
		</div>
	</div>
</div>

<style>
@media only screen and (max-width: 600px) {
    .example {
    text-align: center;
    }
    .headerlebel{
    margin-left: 0px!important;
    }
}
</style>

<script type="text/javascript">

	
$(document).ready(function () {
	
		$("input[name='identitycardnumber']").change(function(){
			var ktp = $("input[name='identitycardnumber']").val();
				$.ajax({
						url     : "{{ url('check-id') }}",
						method  : "POST",
						data    : {"data" : ktp} ,
						success : function(data){
							if(data == "yes"){
								$("input[name='identitycardnumber']").val('');
								alert('ID card number already exist');
							}
						}
					}); 
		});

		$("input[name='old_password']").change(function(){
			var oldPassword = $("input[name='old_password']").val();
				$.ajax({
						url     : "{{ url('check-password') }}",
						method  : "POST",
						data    : {"data" : oldPassword} ,
						success : function(data){
							if(data == "no"){
								$("input[name='old_password']").val('');
								alert('Wrong password');
							}
						},
						error : function(data){
							$("input[name='old_password']").val('');
							alert("Cannot check old password now");
						}
					}); 
		});

	   $("input[name='careerpreference[]']").change(function () {
	      var maxAllowed = 5;
	      var cnt = $("input[name='careerpreference[]']:checked").length;
	      if (cnt > maxAllowed)
	      {
	         $(this).prop("checked", "");
	         alert('Select maximum ' + maxAllowed + ' Career Preference!');
	     }
	  });
	});

$(document).ready(function() {

	   if($('#maritalstatus').val() != "Single"){
		   $('#ifYes').show();
	   }

$(function() {
	   $('#maritalstatus').change(function(){
		   if($('#maritalstatus').val() == "Single"){
			   $('#marrieddate').val('yyyy-MM-dd');
		   }  	 
	   });	   
	   
	 });
	 
});

function yesnoCheck(that) {
    if (that.value != "Single") {
        document.getElementById("ifYes").style.display = "block";
    } else {
        document.getElementById("ifYes").style.display = "none";
    }
}

//tambahan untuk salaryupto yang gaada datanya

//var esf = document.getElementById("expectedsalaryfrom_view").value;
//var est = document.getElementById("expectedsalaryupto_view").value;

var esf ,
elementesf = document.getElementById('expectedsalaryfrom_view');
if (elementesf != null) {
    esf = elementesf.value;
}
else {
    esf = null;
}

var est ,
elementest = document.getElementById('expectedsalaryupto_view');
if (elementest  != null) {
    est = elementest.value;
}
else {
    est = null;
}

var ss ,
elementss = document.getElementById('expectedsalaryupto_view');
if (elementss != null) {
    ss = elementss.value;
}
else {
    ss = null;
}


if(esf != null){
document.getElementById("expectedsalaryfrom_view").disabled = true; } 
if(est != null){
document.getElementById("expectedsalaryupto_view").disabled = true;}
if(ss != null){
document.getElementById("sendsalary").style.display = "none";}


$("#clicker").click(function(){	
	document.getElementById("expectedsalaryfrom_view").disabled = false;
	document.getElementById("expectedsalaryupto_view").disabled = false;
	document.getElementById("sendsalary").style.display = "block";
	document.getElementById("showsalary").style.display = "none";
	document.getElementById("save").style.display = "none"; // savenya di ilangin	
	
});

$("#save").click(function(){	
	document.getElementById("expectedsalaryfrom_view").disabled = true;
	document.getElementById("expectedsalaryupto_view").disabled = true;
	document.getElementById("showsalary").style.display = "block";
	document.getElementById("sendsalary").style.display = "none";

});
//tambahan untuk salaryupto yang ada datanya

//add validation for data type upload fileattachment
$(".inputfile").change(function () {
        var fileExtension = ['jpeg', 'jpg', 'png', 'docx', 'doc','pdf'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Format file harus : "+fileExtension.join(', '));
        }
    });
	
//auto save
		var fieldpob = document.getElementById("provinceofbirth");
		var fieldcob = document.getElementById("cityofbirth");
		var fieldicn = document.getElementById("identitycardnumber");
		var fieldttl = document.getElementById("title_profile");
		var fieldhp = document.getElementById("homephone");
		var fieldmp = document.getElementById("mobilephone");
		var fieldnpwp = document.getElementById("npwp");
		var fieldmtss = document.getElementById("maritalstatus");
		var fieldrg = document.getElementById("religion");
		var fieldaddr = document.getElementById("address");
		var fieldsds = document.getElementById("subdisctrict");
		var fieldcrr = document.getElementById("cityorregency");
		var fieldpv = document.getElementById("province");
		var fieldpc = document.getElementById("postalcode");
		var fieldopt = document.getElementById("optyfrom");
		
		//title_profile
        	fieldttl.onchange = function(e) {
                localStorage.setItem('tp', e.target.value);
            }
            var tp = localStorage.getItem('tp');
            if (tp) {
//                 debugger;
				var titleOptions = document.getElementsByClassName('title_option');
				var titleProfile = document.getElementById("title_profile");
				for (var i in titleOptions) {
					console.log(i);
				}				
				for (var i = 0; i < titleOptions.length; i++) {
					if (titleOptions[i].value == tp) {
						titleProfile.selectedIndex = i;
						break;
					}
				}
            }
    	
	    //gender
            $(function() {
            	$("input[type=\"radio\"]").click(function(){
            		var thisElem = $(this);
            		var value = thisElem.val();
            		$("."+value).show();
            		//localStorage:
            		localStorage.setItem("option", value);
                });
            	//localStorage:
            	var itemValue = localStorage.getItem("option");
            	if (itemValue !== null) {
            		$("input[value=\""+itemValue+"\"]").click();
            	}            
            });
            
	    //date of birth
		var dateInput = document.querySelector('input[name="dateofbirth"]');

            dateInput.addEventListener('change', function(e) {
              localStorage.setItem(dateInput.name, dateInput.value);
            });            
            document.addEventListener("DOMContentLoaded", function() {
              var date = localStorage.getItem(dateInput.name);
              if (date) {
                 dateInput.value = date;
              }
            });

    	//province of birth
    	if (sessionStorage.getItem("autosavepob")) {
    		fieldpob.value = sessionStorage.getItem("autosavepob");	  
    	}    	
    	fieldpob.addEventListener("change", function() {
          sessionStorage.setItem("autosavepob", fieldpob.value);
        });    
        
     	//city of birth	
    	if (sessionStorage.getItem("autosavecob")) {
    		fieldcob.value = sessionStorage.getItem("autosavecob");	  
    	}   
    	fieldcob.addEventListener("change", function() {
          sessionStorage.setItem("autosavecob", fieldcob.value);
        }); 

     	//identity card number
    	if (sessionStorage.getItem("autosaveicn")) {
    		fieldicn.value = sessionStorage.getItem("autosaveicn");	  
    	}   
    	fieldicn.addEventListener("change", function() {
          sessionStorage.setItem("autosaveicn", fieldicn.value);
        }); 

        //home phone
        if (sessionStorage.getItem("autosavehp")) {
        	fieldhp.value = sessionStorage.getItem("autosavehp");	  
    	}   
        fieldhp.addEventListener("change", function() {
          sessionStorage.setItem("autosavehp", fieldhp.value);
        });
        
        //mobile phone
        if (sessionStorage.getItem("autosavemp")) {
        	fieldmp.value = sessionStorage.getItem("autosavemp");	  
    	}   
        fieldmp.addEventListener("change", function() {
          sessionStorage.setItem("autosavemp", fieldmp.value);
        });
                
        //npwp
        if (sessionStorage.getItem("autosavenpwp")) {
        	fieldnpwp.value = sessionStorage.getItem("autosavenpwp");	  
    	}   
        fieldnpwp.addEventListener("change", function() {
          sessionStorage.setItem("autosavenpwp", fieldnpwp.value);
        });
        
        //status
        fieldmtss.onchange = function(e) {
                localStorage.setItem('mtss', e.target.value);
            }
            var mtss = localStorage.getItem('mtss');
            if (mtss) {
				var maritalOptions = document.getElementsByClassName('marital_option');
				var marital_status = document.getElementById("maritalstatus");
				for (var i in maritalOptions) {
					console.log(i);
				}				
				for (var i = 0; i < maritalOptions.length; i++) {
					if (maritalOptions[i].value == mtss) {
						marital_status.selectedIndex = i;
						break;
					}
				}
            }    
        //married date
        var dateInputdate = document.querySelector('input[name="marrieddate"]');

        dateInputdate.addEventListener('change', function(e) {
              localStorage.setItem(dateInputdate.name, dateInputdate.value);
            });            
            document.addEventListener("DOMContentLoaded", function() {
              var datemdt = localStorage.getItem(dateInputdate.name);
              if (datemdt) {
            	  dateInputdate.value = datemdt;
              }
            });
             
        //religion
         fieldrg.onchange = function(e) {
                localStorage.setItem('rg', e.target.value);
            }
            var rg = localStorage.getItem('rg');
            if (rg) {
				var rgOptions = document.getElementsByClassName('rg_option');
				var mreg = document.getElementById("religion");
				for (var i in rgOptions) {
					console.log(i);
				}				
				for (var i = 0; i < rgOptions.length; i++) {
					if (rgOptions[i].value == rg) {
						mreg.selectedIndex = i;
						break;
					}
				}
            }            
        //address
        if (sessionStorage.getItem("autosaveaddr")) {
        	fieldaddr.value = sessionStorage.getItem("autosaveaddr");	  
    	}   
        fieldaddr.addEventListener("change", function() {
          sessionStorage.setItem("autosaveaddr", fieldaddr.value);
        });
        
        //sub district
        if (sessionStorage.getItem("autosavesds")) {
        	fieldsds.value = sessionStorage.getItem("autosavesds");	  
    	}   
        fieldsds.addEventListener("change", function() {
          sessionStorage.setItem("autosavesds", fieldsds.value);
        });
        
        //cityorregency 
        if (sessionStorage.getItem("autosavecrr")) {
        	fieldcrr.value = sessionStorage.getItem("autosavecrr");	  
    	}   
        fieldcrr.addEventListener("change", function() {
          sessionStorage.setItem("autosavecrr", fieldcrr.value);
        });

        //province
         if (sessionStorage.getItem("autosavepv")) {
        	 fieldpv.value = sessionStorage.getItem("autosavepv");	  
    	}   
         fieldpv.addEventListener("change", function() {
          sessionStorage.setItem("autosavepv", fieldpv.value);
        });        
              
        //postal code
        if (sessionStorage.getItem("autosavepc")) {
        	fieldpc.value = sessionStorage.getItem("autosavepc");	  
    	}   
        fieldpc.addEventListener("change", function() {
          sessionStorage.setItem("autosavepc", fieldpc.value);
        });
        
        //opty              
        fieldopt.onchange = function(e) {
            localStorage.setItem('opt', e.target.value);
        }
        var opt = localStorage.getItem('opt');
        if (opt) {
			var optOptions = document.getElementsByClassName('optyfrom_option');
			var optf = document.getElementById("optyfrom");
			for (var i in optOptions) {
				console.log(i);
			}			
			for (var i = 0; i < optOptions.length; i++) {
				if (optOptions[i].value == opt) {
					optf.selectedIndex = i;
					break;
				}
			}
        }            
        //your interest
        
         	var fieldyi = document.getElementById("yourinterests");       
            var checkedAttr = [];            
            
            $('#yourinterests :checkbox').change(function() 
            {
                checkedAttr = [];
                $('#yourinterests :checkbox').each(function(i, item){
                    if($(item).is(':checked'))
                    {
                        checkedAttr.push($(item).val()); 
                    }
                });
               console.log("checkedAttr:", checkedAttr);
               localStorage.setItem("checkedAttr", JSON.stringify(checkedAttr));        
            });
                         
            if (localStorage.getItem("checkedAttr")) {
                var arrPref = JSON.parse(localStorage.getItem("checkedAttr"));
                for(var a=0; a < arrPref.length; a++){
                	$('input.cps_options[value="'+arrPref[a]+'"]').prop('checked',true);
                }
            	console.log('Ada pref'); 
            	console.log(arrPref.length); 
        	} 	

        //your interest
    
	//flush session data
    function flushsession() {
     	sessionStorage.removeItem('autosavetp');
    	sessionStorage.removeItem('autosavepob');
     	sessionStorage.removeItem('autosavecob');
     	sessionStorage.removeItem('autosaveicn');
     	sessionStorage.removeItem('autosavehp');
     	sessionStorage.removeItem('autosavemp');
     	sessionStorage.removeItem('autosavenpwp');
     	sessionStorage.removeItem('autosaveaddr');
     	sessionStorage.removeItem('autosavesds');
     	sessionStorage.removeItem('autosavecrr');    	
     	sessionStorage.removeItem('autosavepv'); 
     	sessionStorage.removeItem('autosavepc');      	
    }	
//end auto save


// $(document).ready(function () {
//     $('#submitchanges').click(function() {
//       checked = $("input[type=checkbox]:checked").length;

//       if(!checked) {
//         alert("You must check at least one checkbox Your Career Preference.");
//         return false;
//       }

//     });
// });



</script>

<section>




@endsection


