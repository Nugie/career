@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> View </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">
		<div class="sbox">
			<div class="sbox-title clearfix">
				<div class="sbox-tools " >
					<a href="{{ url('applicant-menu/ApplicantDetail/'.$user->id) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
				</div>
				<div class="sbox-tools pull-left" >
					<h5>Psychology Detail</h5>
				</div>
			</div>	

			<div class="sbox-content clearfix">
				<ul class="parsley-error-list">
					@foreach($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>		
				
						<div style="background-color: white;" >
								<!-- Table Grid -->
								<div class="table" style="padding-bottom: 70px;">
								
									<table class="table table-striped">
										<tbody>	
											@if(count($rowData) > 0)
													<h4>{{$title}}     <a href="{{ url('download-pdf/'.$type.'/'.$user->id) }}" title="Download file" ><i class="fa fa-download"></i></a></h4>
													<tr>
														<td width='30%' class='label-view text-left'><strong>Fullname</strong></td>
														<td colspan="7">{{ $user->first_name }}  {{ $user->last_name }}</td>
													</tr>
													<tr>
														<td width='30%' class='label-view text-left'><strong>Date</strong></td>
														<td colspan="7">{{ date('d-M-y', strtotime($rowData->date_test)) }}</td>
													</tr>
													<tr>
														<td width='30%' class='label-view text-left'><strong>Score</strong></td>
														<td width="3%">
															E <br>
															S <br>
															T <br>
															J 
														</td>
														<td width="3%">
															: <br>
															: <br>
															: <br>
															:
														</td>
														<td width="10%">
															{{ $hasil->E }} <br>
															{{ $hasil->S }} <br>
															{{ $hasil->T }} <br>
															{{ $hasil->J }}
														</td>
														<td width="5%">
															
														</td>
														<td width="3%">
															I <br>
															N <br>
															F <br>
															P
														</td>
														<td width="3%">
															: <br>
															: <br>
															: <br>
															:
														</td>
														<td width="46%">
															{{ $hasil->I }} <br>
															{{ $hasil->N }} <br>
															{{ $hasil->F }} <br>
															{{ $hasil->P }}
														</td>
													</tr>
													<tr>
														<td width='30%' class='label-view text-left'><strong>Final Score</strong></td>
														<td>{{ $rowData->hasil_final }}</td>
													</tr>
													<tr>
														<td width='30%' class='label-view text-left'><strong>Desription</strong></td>
														@if($rowData->hasil_final == 'ISTJ')
															<td colspan="7">Idealistic, loyal to their values and to people who are important to them. Want an external life that is congruent with their values. Curious, quick to see possibilities, can be catalysts for implementing ideas. Seek to understand people and to help them fulfill their potential. Adaptable, flexible, and accepting unless a value is threatened.</td>
														@elseif($rowData->hasil_final == 'ISFJ')
															<td colspan="7">Quiet, friendly, responsible, and conscientious. Committed and steady in meeting their obligations. Thorough, painstaking, and accurate. Loyal, considerate, notice and remember specifics about people who are important to them, concerned with how others feel. Strive to create an orderly and harmonious environment at work and at home.</td>
														@elseif($rowData->hasil_final == 'INFJ')
															<td colspan="7">Seek meaning and connection in ideas, relationships, and material possessions. Want to understand what motivates people and are insightful about others. Conscientious and committed to their firm values. Develop a clear vision about how best to serve the common good. Organized and decisive in implementing their vision.</td>
														@elseif($rowData->hasil_final == 'INTJ')
															<td colspan="7">Have original minds and great drive for implementing their ideas and achieving their goals. Quickly see patterns in external events and develop long-range explanatory perspectives. When committed, organize a job and carry it through. Skeptical and independent, have high standards of competence and performance - for themselves and others.</td>
														@elseif($rowData->hasil_final == 'ISTP')
															<td colspan="7">Tolerant and flexible, quiet observers until a problem appears, then act quickly to find workable solutions. Analyze what makes things work and readily get through large amounts of data to isolate the core of practical problems. Interested in cause and effect, organize facts using logical principles, value efficiency.</td>
														@elseif($rowData->hasil_final == 'ISFP')
															<td colspan="7">Quiet, friendly, sensitive, and kind. Enjoy the present moment, what's going on around them. Like to have their own space and to work within their own time frame. Loyal and committed to their values and to people who are important to them. Dislike disagreements and conflicts, do not force their opinions or values on others.</td>
														@elseif($rowData->hasil_final == 'INFP')
															<td colspan="7">Idealistic, loyal to their values and to people who are important to them. Want an external life that is congruent with their values. Curious, quick to see possibilities, can be catalysts for implementing ideas. Seek to understand people and to help them fulfill their potential. Adaptable, flexible, and accepting unless a value is threatened.</td>
														@elseif($rowData->hasil_final == 'INTP')
															<td colspan="7">Seek to develop logical explanations for everything that interests them. Theoretical and abstract, interested more in ideas than in social interaction. Quiet, contained, flexible, and adaptable. Have unusual ability to focus in depth to solve problems in their area of interest. Skeptical, sometimes critical, always analytical.</td>
														@elseif($rowData->hasil_final == 'ESTP')
															<td colspan="7">Flexible and tolerant, they take a pragmatic approach focused on immediate results. Theories and conceptual explanations bore them - they want to act energetically to solve the problem. Focus on the here-and-now, spontaneous, enjoy each moment that they can be active with others. Enjoy material comforts and style. Learn best through doing.</td>
														@elseif($rowData->hasil_final == 'ESFP')
															<td colspan="7">Outgoing, friendly, and accepting. Exuberant lovers of life, people, and material comforts. Enjoy working with others to make things happen. Bring common sense and a realistic approach to their work, and make work fun. Flexible and spontaneous, adapt readily to new people and environments. Learn best by trying a new skill with other people.</td>
														@elseif($rowData->hasil_final == 'ENFP')
															<td colspan="7">Warmly enthusiastic and imaginative. See life as full of possibilities. Make connections between events and information very quickly, and confidently proceed based on the patterns they see. Want a lot of affirmation from others, and readily give appreciation and support. Spontaneous and flexible, often rely on their ability to improvise and their verbal fluency.</td>
														@elseif($rowData->hasil_final == 'ENTP')
															<td colspan="7">Quick, ingenious, stimulating, alert, and outspoken. Resourceful in solving new and challenging problems. Adept at generating conceptual possibilities and then analyzing them strategically. Good at reading other people. Bored by routine, will seldom do the same thing the same way, apt to turn to one new interest after another.</td>
														@elseif($rowData->hasil_final == 'ESTJ')
															<td colspan="7">Practical, realistic, matter-of-fact. Decisive, quickly move to implement decisions. Organize projects and people to get things done, focus on getting results in the most efficient way possible. Take care of routine details. Have a clear set of logical standards, systematically follow them and want others to also. Forceful in implementing their plans.</td>
														@elseif($rowData->hasil_final == 'ESFJ')
															<td colspan="7">Warmhearted, conscientious, and cooperative. Want harmony in their environment, work with determination to establish it. Like to work with others to complete tasks accurately and on time. Loyal, follow through even in small matters. Notice what others need in their day-by-day lives and try to provide it. Want to be appreciated for who they are and for what they contribute.</td>
														@elseif($rowData->hasil_final == 'ENFJ')
															<td colspan="7">Warm, empathetic, responsive, and responsible. Highly attuned to the emotions, needs, and motivations of others. Find potential in everyone, want to help others fulfill their potential. May act as catalysts for individual and group growth. Loyal, responsive to praise and criticism. Sociable, facilitate others in a group, and provide inspiring leadership.</td>
														@elseif($rowData->hasil_final == 'ENTJ')
															<td colspan="7">Frank, decisive, assume leadership readily. Quickly see illogical and inefficient procedures and policies, develop and implement comprehensive systems to solve organizational problems. Enjoy long-term planning and goal setting. Usually well informed, well read, enjoy expanding their knowledge and passing it on to others. Forceful in presenting their ideas.</td>
														@endif
													</tr>
											@else
												<tr>
													<td colspan="6">No data</td>
												</tr>
											@endif
										</tbody>	
									</table>
								</div>

						</div>
					
			</div>
		</div>
	</div>
</div>		 
@stop