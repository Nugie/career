<section>
    <div>
       <div>
			<img src="{!! asset('uploads/images/contactus-photo.jpeg') !!}" />
         <div class="wrap">
         </div>
       </div>
     </div>
</section>

<section>
    <div class="col-md-12" style="text-align: center;">
		<h2 style="color: #fa9300;">Astra Credit Companies</h2>
    </div>
    <div class="col-md-12">
    	<h5 class="contactus-text">Astra Credit Companies atau biasa di singkat dengan ACC adalah perusahaan pembiayaan 
    	mobil dan alat berat. sesuai dengan Peraturan Otoritas Jasa Keuangan No. 29/POJK.05/2014 ACC melakukan 
    	perluasan usaha di bidang Pembiayaan Investasi,Pembiayaan Modal Kerja, Pembiayaan Multiguna dan Sewa 
    	Operasi (Operating Lease), baik dengan skema konvensional maupun syariah. 
    	PT Astra Sedaya Finance yang merupakan cikal bakal ACC berdiri pada 15 Juli 1982 dengan nama PT Rahardja Sedaya, 
    	didirikan guna mendukung bisnis otomotif kelompok Astra.</h5>
    </div>
</section>


&nbsp;
&nbsp;
&nbsp;

<section>
 <div class="row" style="background-color: #e2e3e7!important; margin-top: 0px; padding-top: 20px; border-bottom-width: 20px;
         padding-bottom: 20px;">
        <div class="col-md-6">
			<iframe  class="maps-contactus" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d507394.8799538428!2d106.64353209194813!3d-6.517827390687336!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1d355c6ae0614efb!2sAstra+Credit+Companies!5e0!3m2!1sen!2sid!4v1537457446237" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
        <div class="col-md-4" style="margin: 60px; border: 2px solid #ff9c25; padding: 10px;box-shadow: 5px 10px #0072bb;
         border-top-left-radius: 10px;border-bottom-left-radius: 10px; border-top-right-radius: 10px; border-bottom-right-radius: 10px;">
        <h4 style="line-height: 1.6; font-size: 17px;">
			 Jl. TB. Simatupang No. 90,<br>
			 RT.3/RW.2,Tanjung Barat, Jagakarsa,<br>
			 Tj. Bar., Jagakarsa, Jakarta Selatan, <br>
			 Daerah Khusus Ibukota Jakarta 12530<br>
			 TLP: 02175589000 Ext. 1551/1502<br>
			 Email: career@acc.co.id<br>
		</h4>
        </div>
 </div>
</section>
