@if( count($rowData) > 0)
<section class="section" style="background-color:#eceff1 !important;padding:19px!important; margin-top:120px!important">
    <div class="container">
		<div class="row">
			<div class="col-md-6" style="padding-left: 30px;padding-right: 30px;">
				<h2><b>{{ $rowData->JobTitleName }}</b></h2>
			</div>
			<!-- <div class="col-md-6" style="text-align:right;padding-top:18px;">
				<span class="bread-job">
					<a href="{{url('')}}">Home</a> / <a href="{{url('job-field/')}}">Job Fields</a>  / <a href="{{url('job-list/' . $rowData->IDJobField)}}">{{ $cat }}</a> / {{ $rowData->JobTitleName }} 
				</span>
			</div>-->
		</div>
    </div>
</section>
<section class="section" style="background-color:white!important; padding-top:20px!important">
    <div class="container">
		<div class="col-md-8">
			@if($rowData->img_jobfield)
			<img class="port-image" src="{!! asset('uploads/jobfield/'.$rowData->img_jobfield.'') !!}" style="width:70%; height:auto"/>     
			@else
			<img class="port-image" src="{!! asset('uploads/images/no-image.png') !!}" style="width:70%; height:auto"/>  
			@endif
			<h3>Detail</h3>
		  <p><span class="span-title">Category</span><span> : </span><span class="span-value">{{ $rowData->JobCategory }}</span></p>
		  <p><span class="span-title">Status</span><span> : </span><span class="span-value">{{ $rowData->JobStatusType }}</span></p>
		  <p><span class="span-title">Job Field</span><span> : </span><span class="span-value">{{ $cat }}</span></p>
		  <p><span class="span-title">Placement</span><span> : </span><span class="span-value">{{ $rowData->placement }}</span></p>
		  <p><span class="span-title">Degree</span><span> : </span><span class="span-value">{{ $rowData->Degree }}</span></p>
		  <p><span class="span-title">GPA</span><span> : </span><span class="span-value">{{ $rowData->GPA }}</span></p>
		  <p><span class="span-title">Experience</span><span > : </span><span class="span-value">{{ $rowData->MinWorkExp }} year(s)</span></p>
		  <p><span class="span-title">Max Age</span><span> : </span><span class="span-value">{{ $rowData->MaxAge }}</span></p>
	  
	  <h3>Description</h3>
	  <p>{{ $rowData->JobDescription }}</p>
	  
	  <h3>Requirements</h3>
	  <?php $resultRequirements = str_replace("-","<br>-",$rowData->JobRequirements); ?>
	  <p class="req-br">{!! $resultRequirements !!}</p>
	@if(Auth::check())
      <div class="col-md-12" style="padding-top: 40px; text-align:center">
	@if($dataNull)
      @if($checkUser)
      	<a href="{{ url('prescreening-list/'. $rowData->id. '/' . str_slug($rowData->JobTitleName, '-') ) }}" class="btn btn-primary" style="width: 139px;height: 35px;border-bottom-left-radius: 5px;
      border-bottom-right-radius: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px; background-color: #0089d1;border-color: #0089d1;">
      <b>APPLY NOW</b></a> 
      <!-- penambahan validasi memunculkan tombol apply walaupun sudah pernah apply -->
      @else
      <!-- bootstrap warning, tinggal diganti isi dari warning yang diinginkan pada header dan body setelah login tetapi sudah apply -->
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"
      style="width: 139px;height: 35px;border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px; background-color: #0089d1;border-color: #0089d1">APPLY NOW W</button>
      <div id="myModal" class="modal fade" role="dialog">
      	<div class="modal-dialog">
      		<div class="modal-content" style="text-align:left;">
      			<div class="modal-header">
      				<!-- header yang ingin diubah -->
      				<h4 class="modal-title">WARNING</h4>
      			</div>
      			<div class="modal-body">
      				<!-- isi message yang ingin diubah -->
      				<p>{{ Lang::get('acc.samejob') }}</p>
      			</div>
      			<div class="modal-footer">
      				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      			</div>
      		</div>
      	</div>
      </div>
      @endif
	  @else
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal"
      style="width: 139px;height: 35px;border-bottom-left-radius: 5px; border-bottom-right-radius: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px; background-color: #0089d1;border-color: #0089d1">APPLY NOW T</button>
      <div id="myModal" class="modal fade" role="dialog">
      	<div class="modal-dialog">
      		<div class="modal-content" style="text-align:left;">
      			<div class="modal-header">
      				<!-- header yang ingin diubah -->
      				<h4 class="modal-title">WARNING</h4>
      			</div>
      			<div class="modal-body">
      				<!-- isi message yang ingin diubah -->
      				<p>{{ Lang::get('acc.completeprofile') }}</p>
      			</div>
      			<div class="modal-footer">
      				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      			</div>
      		</div>
      	</div>
      </div>		  
	@endif
     </div>
     @else
           @php
              $actual_link = "{$_SERVER['REQUEST_URI']}";
              $str = ltrim($actual_link, '/');
  
            @endphp
        	<script type="text/javascript">
		      //swal("Update Your profile", "Please complete your Personal Details and Educational Background.", "warning");
         var redirect = "{!! $str !!}";
         $( document).ready(function() {
            $(".login-first").click(function(){
              //alert('test');
               swal({
                title: "Info",
                text: "Please login before submit job.",
                icon: "info",
                  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, delete it!",
  closeOnConfirm: false
                }).then(function() {
                // Redirect the user
                window.location.href = "/user/login?redirect="+redirect+"";
               });
            });
        });
      	</script>
     	<div class="col-md-12" style="padding-top: 40px; text-align:center">
     		<!-- bootstrap warning, tinggal diganti isi dari warning yang diinginkan pada header dan body untuk sebelum login -->
      		<button type="button" class="btn btn-primary login-first"
      		style="width: 139px;height: 35px;border-bottom-left-radius: 5px;
      border-bottom-right-radius: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px; background-color: #0089d1;border-color: #0089d1;">APPLY NOW </button>
      
   
            		<!--<div id="myModal" class="modal fade" role="dialog">
      			<div class="modal-dialog">
      				<div class="modal-content"  style="text-align:left;">
      					<div class="modal-header">
      				
      						<h4 class="modal-title">WARNING</h4>
      					</div>
      					<div class="modal-body">
      		
      						<p>{{ Lang::get('acc.notlogin') }}</p>
      					</div>
      					<div class="modal-footer">
      						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      					</div>
      				</div>
      			</div>
      		</div>-->
     	</div>
	 @endif
    </div>
    <div class="col-md-4 ">
      <h3 style="margin-bottom:20px"><b>Preference Job</b></h3></i>
	  @if (count($pref) > 0)
	  	@foreach($pref as $items)
	  	<div class="row">
      		<div class="col-md-3">
				@if($items->img_jobfield)
	      		<img class="port-image img-pref-job" src="{!! asset('uploads/jobfield/'.$items->img_jobfield.'') !!}"/>
				@else
				<img class="port-image  img-pref-job" src="{!! asset('uploads/images/no-image.png') !!}" />
				@endif
	      		<br>
      		</div>
      		
      		<div class="col-md-9">
      			<h4><b><a  style="color:#36bf54 !important;" href="{{ url('job-list/'. $items->IDJobField . '/'. $items->id . '/' . str_slug($items->JobTitleName, '-') ) }}">{{ $items->JobTitleName }}</a></b></h4>
      			<h5 style="Color:#001dff"><b>{{ $items->JobCategory }}</b></h5>
      			<h5>{{ $items->name }}</h5>
				<br>
    		</div>
    	</div>
    	@endforeach

      @else
      		<div class="col-md-2" style="padding-left:0px;padding-right:0px; margin-bottom:30px;">
	      		&nbsp;
      		</div>		
      		<div class="col-md-2" style="width:220px;">
				&nbsp;
    		</div>      	
      @endif
		@if($rowData->LinkVideo)
		<iframe class="vid-job" src=<?php echo $rowData->LinkVideo; ?> frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
		@endif
    </div>
  </div>

</section>
@else
<section class="section" style="background-color:#eceff1 !important;padding:19px!important; margin-top:120px!important">
    <div class="container">
		<div class="row">
			<div class="col-md-6" style="padding-left: 30px;padding-right: 30px;">
				<h2><b>Job not found</b></h2>
			</div>
		</div>
    </div>
</section>
@endif