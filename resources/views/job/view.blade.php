@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url($pageModule) }}"> {{ $pageTitle }} </a></li>
		<li class="active"> View  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">
	
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools pull-left" >
		   		<a href="{{ ($prevnext['prev'] != '' ? url('job/'.$prevnext['prev'].'?return='.$return ) : '#') }}" class="tips btn btn-sm"><i class="fa fa-arrow-left"></i>  </a>	
				<a href="{{ ($prevnext['next'] != '' ? url('job/'.$prevnext['next'].'?return='.$return ) : '#') }}" class="tips btn btn-sm "> <i class="fa fa-arrow-right"></i>  </a>					
			</div>	

			<div class="sbox-tools" >
				@if($access['is_add'] ==1)
		   		<a href="{{ url('job/'.$id.'/edit?return='.$return) }}" class="tips btn btn-sm  " title="{{ __('core.btn_edit') }}"><i class="fa  fa-pencil"></i></a>
				@endif
				<a href="{{ url('job?return='.$return) }}" class="tips btn btn-sm  " title="{{ __('core.btn_back') }}"><i class="fa  fa-times"></i></a>		
			</div>
		</div>
		<div class="sbox-content">
			<div class="table-responsive">
				<table class="table table-striped " >
					<tbody>	
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Job Title', (isset($fields['JobTitleName']['language'])? $fields['JobTitleName']['language'] : array())) }}</td>
						<td>{{ $row->JobTitleName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Location', (isset($fields['Location']['language'])? $fields['Location']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->Location,'Location','1:acc_placement:id:name') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Job Field', (isset($fields['IDJobField']['language'])? $fields['IDJobField']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->IDJobField,'IDJobField','1:acc_jobfield:id:jobfield') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Category', (isset($fields['JobCategory']['language'])? $fields['JobCategory']['language'] : array())) }}</td>
						<td>{{ $row->JobCategory}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Sub Category', (isset($fields['JobSubCategory']['language'])? $fields['JobSubCategory']['language'] : array())) }}</td>
						<td>{{ $row->JobSubCategory}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Description', (isset($fields['JobDescription']['language'])? $fields['JobDescription']['language'] : array())) }}</td>
						<td>{{ $row->JobDescription}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Requirements', (isset($fields['JobRequirements']['language'])? $fields['JobRequirements']['language'] : array())) }}</td>
						<td>{{ $row->JobRequirements}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('GPA', (isset($fields['GPA']['language'])? $fields['GPA']['language'] : array())) }}</td>
						<td>{{ $row->GPA}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Degree', (isset($fields['Degree']['language'])? $fields['Degree']['language'] : array())) }}</td>
						<td>{{ $row->Degree}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Minimum Work Experience', (isset($fields['MinWorkExp']['language'])? $fields['MinWorkExp']['language'] : array())) }}</td>
						<td>{{ $row->MinWorkExp}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Maximum Age', (isset($fields['MaxAge']['language'])? $fields['MaxAge']['language'] : array())) }}</td>
						<td>{{ $row->MaxAge}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Type', (isset($fields['JobStatusType']['language'])? $fields['JobStatusType']['language'] : array())) }}</td>
						<td>{{ $row->JobStatusType}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Prescreening Question', (isset($fields['id_kategori_soal']['language'])? $fields['id_kategori_soal']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->id_kategori_soal,'id_kategori_soal','1:acc_kategori_soal:id:name') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Link Video', (isset($fields['LinkVideo']['language'])? $fields['LinkVideo']['language'] : array())) }}</td>
						<td>{{ $row->LinkVideo}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status', (isset($fields['StatusJob']['language'])? $fields['StatusJob']['language'] : array())) }}</td>
						<td>{{ $row->StatusJob}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('View Job', (isset($fields['ViewJob']['language'])? $fields['ViewJob']['language'] : array())) }}</td>
						<td>{{ $row->ViewJob}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Start Date', (isset($fields['StartDate']['language'])? $fields['StartDate']['language'] : array())) }}</td>
						<td>{{ $row->StartDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('End Date', (isset($fields['EndDate']['language'])? $fields['EndDate']['language'] : array())) }}</td>
						<td>{{ $row->EndDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Created Date', (isset($fields['CreatedDate']['language'])? $fields['CreatedDate']['language'] : array())) }}</td>
						<td>{{ $row->CreatedDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Modified Date', (isset($fields['LastModifiedDate']['language'])? $fields['LastModifiedDate']['language'] : array())) }}</td>
						<td>{{ $row->LastModifiedDate}} </td>
						
					</tr>
				
					</tbody>	
				</table>   

			 	

			</div>
		</div>
	</div>
	</div>
</div>
@stop
