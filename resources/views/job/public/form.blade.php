

		 {!! Form::open(array('url'=>'job', 'class'=>'form-horizontal','files' => true , 'parsley-validate'=>'','novalidate'=>' ')) !!}

	@if(Session::has('messagetext'))
	  
		   {!! Session::get('messagetext') !!}
	   
	@endif
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		


<div class="col-md-12">
						<fieldset><legend> Job</legend>
				{!! Form::hidden('CreatedByID', $row['CreatedByID']) !!}					
									  <div class="form-group  " >
										<label for="StartDate" class=" control-label col-md-4 text-left"> StartDate <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('StartDate', $row['StartDate'],array('class'=>'form-control input-sm date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="EndDate" class=" control-label col-md-4 text-left"> EndDate <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('EndDate', $row['EndDate'],array('class'=>'form-control input-sm date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="FromGrade" class=" control-label col-md-4 text-left"> FromGrade <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='FromGrade' id='FromGrade' value='{{ $row['FromGrade'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="JobFamilyName" class=" control-label col-md-4 text-left"> JobFamilyName <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='JobFamilyName' id='JobFamilyName' value='{{ $row['JobFamilyName'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="JobTitleDescription" class=" control-label col-md-4 text-left"> JobTitleDescription <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='JobTitleDescription' rows='5' id='editor' class='form-control input-sm editor '  
						 >{{ $row['JobTitleDescription'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="JobTitleName" class=" control-label col-md-4 text-left"> JobTitleName <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='JobTitleName' id='JobTitleName' value='{{ $row['JobTitleName'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="JobTitleType" class=" control-label col-md-4 text-left"> JobTitleType <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $JobTitleType = explode(',',$row['JobTitleType']);
					$JobTitleType_opt = array( 'Freshgraduate' => 'Freshgraduate' ,  'Experience' => 'Experience' ,  'Internship' => 'Internship' , ); ?>
					<select name='JobTitleType' rows='5'   class='select2 '  > 
						<?php 
						foreach($JobTitleType_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['JobTitleType'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="ToGrade" class=" control-label col-md-4 text-left"> ToGrade <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='ToGrade' id='ToGrade' value='{{ $row['ToGrade'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Location" class=" control-label col-md-4 text-left"> Location <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='Location' id='Location' value='{{ $row['Location'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Image" class=" control-label col-md-4 text-left"> Image <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='file' name='Image' id='Image' class='inputfile  @if($row['Image'] =='') class='required' @endif '  />

							<label for='Image'><i class='fa fa-upload'></i> Choose a file</label>
							<div class='Image_preview'></div>
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['Image'],'/uploads/jobs') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="GPA" class=" control-label col-md-4 text-left"> GPA <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='GPA' id='GPA' value='{{ $row['GPA'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Degree" class=" control-label col-md-4 text-left"> Degree <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='Degree' id='Degree' value='{{ $row['Degree'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="MinWorkExp" class=" control-label col-md-4 text-left"> MinWorkExp <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='MinWorkExp' id='MinWorkExp' value='{{ $row['MinWorkExp'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="MaxAge" class=" control-label col-md-4 text-left"> MaxAge <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='MaxAge' id='MaxAge' value='{{ $row['MaxAge'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

			<div style="clear:both"></div>	
				
					
				  <div class="form-group">
					<label class="col-sm-4 text-right">&nbsp;</label>
					<div class="col-sm-8">	
					<button type="submit" name="apply" class="btn btn-info btn-sm" ><i class="fa  fa-check-circle"></i> {{ Lang::get('core.sb_apply') }}</button>
					<button type="submit" name="submit" class="btn btn-primary btn-sm" ><i class="fa  fa-save "></i> {{ Lang::get('core.sb_save') }}</button>
				  </div>	  
			
		</div> 
		 <input type="hidden" name="action_task" value="public" />
		 {!! Form::close() !!}
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		 

		$('.removeCurrentFiles').on('click',function(){
			var removeUrl = $(this).attr('href');
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
