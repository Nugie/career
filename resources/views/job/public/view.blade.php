<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('StartDate', (isset($fields['StartDate']['language'])? $fields['StartDate']['language'] : array())) }}</td>
						<td>{{ $row->StartDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('EndDate', (isset($fields['EndDate']['language'])? $fields['EndDate']['language'] : array())) }}</td>
						<td>{{ $row->EndDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('FromGrade', (isset($fields['FromGrade']['language'])? $fields['FromGrade']['language'] : array())) }}</td>
						<td>{{ $row->FromGrade}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('JobFamilyName', (isset($fields['JobFamilyName']['language'])? $fields['JobFamilyName']['language'] : array())) }}</td>
						<td>{{ $row->JobFamilyName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('JobTitleDescription', (isset($fields['JobTitleDescription']['language'])? $fields['JobTitleDescription']['language'] : array())) }}</td>
						<td>{{ $row->JobTitleDescription}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('JobTitleMetadata', (isset($fields['JobTitleMetadata']['language'])? $fields['JobTitleMetadata']['language'] : array())) }}</td>
						<td>{{ $row->JobTitleMetadata}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('JobTitleName', (isset($fields['JobTitleName']['language'])? $fields['JobTitleName']['language'] : array())) }}</td>
						<td>{{ $row->JobTitleName}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('JobTitleType', (isset($fields['JobTitleType']['language'])? $fields['JobTitleType']['language'] : array())) }}</td>
						<td>{{ $row->JobTitleType}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('ToGrade', (isset($fields['ToGrade']['language'])? $fields['ToGrade']['language'] : array())) }}</td>
						<td>{{ $row->ToGrade}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Image', (isset($fields['Image']['language'])? $fields['Image']['language'] : array())) }}</td>
						<td>{{ $row->Image}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('CreatedDate', (isset($fields['CreatedDate']['language'])? $fields['CreatedDate']['language'] : array())) }}</td>
						<td>{{ $row->CreatedDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('LastModifiedDate', (isset($fields['LastModifiedDate']['language'])? $fields['LastModifiedDate']['language'] : array())) }}</td>
						<td>{{ $row->LastModifiedDate}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Location', (isset($fields['Location']['language'])? $fields['Location']['language'] : array())) }}</td>
						<td>{{ $row->Location}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('GPA', (isset($fields['GPA']['language'])? $fields['GPA']['language'] : array())) }}</td>
						<td>{{ $row->GPA}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Degree', (isset($fields['Degree']['language'])? $fields['Degree']['language'] : array())) }}</td>
						<td>{{ $row->Degree}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('MinWorkExp', (isset($fields['MinWorkExp']['language'])? $fields['MinWorkExp']['language'] : array())) }}</td>
						<td>{{ $row->MinWorkExp}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('MaxAge', (isset($fields['MaxAge']['language'])? $fields['MaxAge']['language'] : array())) }}</td>
						<td>{{ $row->MaxAge}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	