@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
<?php
$userId = Auth::user()->id;
$is_user= AccHelpers::is_user($userId);
 ?>
<section class="page-header row">
	<h2> {{ $pageTitle }} </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> {{ $pageTitle }} </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">
		<div class="sbox">
			@if(!$is_user)
			<div class="sbox-title">
				<h1> All Records <small> </small></h1>
				<div class="sbox-tools">
					@if(Session::get('gid') ==1)
						<a href="{{ url('prescreening-management') }}" class="tips btn btn-sm  " title=" {{ __('core.btn_reload') }}" ><i class="fa  fa-refresh"></i></a>
					@endif 	
				</div>				
			</div>
			<div class="sbox-content">
			<!-- Toolbar Top -->
			<div class="row">
				<div class="col-md-4"> 	
					<a href="{{ url('kategorisoal/create?return='.$return) }}" class="btn btn-default btn-sm"  
						title="{{ __('core.btn_create') }}"><i class=" fa fa-plus "></i> Create New </a>
				</div>  
			</div>					
			<!-- End Toolbar Top -->
			<!-- Table Grid -->
			<div class="table-responsive" style="padding-bottom: 70px;">
			
		    <table class="table table-striped table-hover " id="{{ $pageModule }}Table">
		        <thead>
					<tr>
						<th style="width: 3% !important;" class="number"> No </th>
						<?php $i = ($rowData->currentPage()-1)* $rowData->perPage();?>
						<th align="center">Group Name</th>
						<th align="center">Description</th>
						<th align="center">Action</th>
					  </tr>
		        </thead>
				<tbody>
				@if(count($rowData) > 0)
					@foreach ($rowData as $row)
						<tr>
							<td> {{ ++$i }} </td>
							<td>{{ $row->name }}</td>
							<td>{{ $row->description }}</td>
							<td>
								<a href="{{ url('prescreening-management/question/'. $row->id .'/view') }}"><button class="btn btn-primary btn-xs" type="button" title="View Question"><i class="fa fa-question"></i> </button></a>
								<a href="{{ url('kategorisoal/'.$row->id.'/edit?return='.$return) }}"><button class="btn btn-primary btn-xs" type="button" title="Edit Group"><i class="fa fa-edit"></i> </button></a>  
								<!-- <a href="{{ url('prescreening-management/group/'. $row->id .'/delete') }}"><button class="btn btn-primary btn-xs" type="button" title="Remove"><i class="fa fa-times"></i> </button></a>  -->
							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="6">No data</td>
						</tr>
					@endif
				</tbody>
		    </table>

			<input type="hidden" name="action_task" value="" />
			
			{!! $rowData->links() !!}
			</div>
			<!-- End Table Grid -->
			

			</div>
		</div>
		@else
			<div>Permission not allowed</div>
		@endif
		
	</div>
</div>

@stop