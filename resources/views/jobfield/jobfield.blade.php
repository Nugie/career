<section>
    <div>
       <div style="background: url('{!! asset('uploads/images/TorontoSkyline.jpg') !!}');  background-size: cover; background-size: 100% 100%; height:350px !important;">
       </div>
	   <div class="search-div">
         <div class="input-group row" style="margin: 2% auto !important; width: 50%;">
         	<form action="{{url('search')}}" method="POST" role="search">
         	{{ csrf_field() }}
         	<div class="col-md-10 padding-0">
             <input type="text" class="search-query form-control" placeholder="Search your job" name="q"/>
             <div style="background-color: #555;">
				<div class="btn-group">
					<select class="form-control custdrop-search"  style="width: 110px;" name="jf">
						<option value="" selected disabled>Job Field</option>
						@if (count($jobFieldList) > 0)
							@foreach($jobFieldList as $optionField)
								<option value="{{ $optionField->id }}">{{ $optionField->jobfield }}</option>
							@endforeach
						@endif
 					</select>
				</div>
				<div class="btn-group">
					<select class="form-control custdrop-search"  style="width: 140px;" name="jc">
						<option value="" selected disabled>Job Category</option>
						@if (count($jobCatList) > 0)
							@foreach($jobCatList as $optionCat)
								<option value="{{ $optionCat->JobCategory }}">{{ $optionCat->JobCategory }}</option>
							@endforeach
						@endif
 					</select>
				</div>
				<div class="btn-group">
					<select class="form-control custdrop-search" style="width: 120px;" name="loc">
						<option value="" selected disabled>Location</option>
						@if (count($locList) > 0)
							@foreach($locList as $optionLoc)
								<option value="{{ $optionLoc->id }}">{{ $optionLoc->name }}</option>
							@endforeach
						@endif
 					</select>
				</div>
             </div>
             </div>
             
             <div class="input-group-btn col-md-2 padding-0">
                 <button class="btn btn-primary" type="submit" style="height:68px;">
                     <span>GET YOUR JOB!</span>
                 </button>
             </div>
             </form>
         </div>
		</div>
     </div>
</section>


    <section class="section  bg-light filtercategory" style="padding-top:10px!important">
      <!-- WELCOME -->
      <div class="container filtercategory">

          <br/>
          @if (count($rowData) > 1)
          <div class="row">
          	@foreach($rowData as $items)
              <div class="col-md-3 col-sm-6 filter photo effect" style="margin: 10px 0; padding-top:10px !important;">
              <a href="{{ url('job-list/'. $items->id)}}">
              <figure class="effect-winston">
					@if($items->img_jobfield)
              		<img class="port-image" src="{!! asset('uploads/jobfield/'.$items->img_jobfield.'') !!}"/>
					@else
					<img class="port-image" src="{!!  asset('uploads/jobs/1518592336-14524170.jpg') !!}"/>
					@endif
              		<figcaption>
						@if(strlen($items->jobfield) < 30)
              			<h3 style="font-size: 16px"><b>{{ $items->jobfield }}</b></h3>
						@else
						<h3 style="font-size: 14px"><b>{{ $items->jobfield }}</b></h3>
						@endif
              		</figcaption>
              </figure>
              </a>
              </div>
             @endforeach
          </div>
          &nbsp;
          @else
    		<p>No Data</p>
    	  @endif
          </div>
      <!-- /WELCOME -->

    </section>

    <script>
    $(document).ready(function(){

        $(".filter-button").click(function(){
            var value = $(this).attr('data-filter');

            if(value == "all")
            {
                $('.filter').show('1000');
            }
            else
            {
                $(".filter").not('.'+value).hide('3000');
                $('.filter').filter('.'+value).show('3000');

            }
        });

    });
    </script>
