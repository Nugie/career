@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url($pageModule) }}"> {{ $pageTitle }} </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'jobfield?return='.$return, 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<button name="apply" class="tips btn btn-sm btn-apply  "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-check"></i> {{ __('core.sb_apply') }} </button>
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.btn_back') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="col-md-12">
						<fieldset><legend> Job Field</legend>
				{!! Form::hidden('id', $row['id']) !!}					
									  <div class="form-group  " >
										<label for="Jobfield" class=" control-label col-md-4 text-left"> Jobfield <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='jobfield' id='jobfield' value='{{ $row['jobfield'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Img Jobfield" class=" control-label col-md-4 text-left"> Img Jobfield <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='file' name='img_jobfield' id='img_jobfield' class='inputfile  @if($row['img_jobfield'] =='') class='required' @endif '  />

							<label for='img_jobfield'><i class='fa fa-upload'></i> Choose a file - Recommendation Image Dimension 511 x 288 px</label>
							<div class='img_jobfield_preview'></div>
					 	<div >
						{!! SiteHelpers::showUploadedFile($row['img_jobfield'],'/uploads/jobfield') !!}
						
						</div>					
					 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Status Jobfield" class=" control-label col-md-4 text-left"> Status Jobfield <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $status_jobfield = explode(',',$row['status_jobfield']);
					$status_jobfield_opt = array( 'active' => 'Active' ,  'inactive' => 'Inactive' , ); ?>
					<select name='status_jobfield' rows='5'   class='select2 '  > 
						<?php 
						foreach($status_jobfield_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['status_jobfield'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> {!! Form::hidden('idExternal', $row['idExternal']) !!}</fieldset>
			</div>
			
			

		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		 		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("jobfield/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop