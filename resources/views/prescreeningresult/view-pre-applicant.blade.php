@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }}</h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url($pageModule) }}"> {{ $pageTitle }} </a></li>
		<li class="active"> View  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">
	
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools pull-left" >				
			</div>	

			<div class="sbox-tools" >
				<a href="{{ url('list-prescreening-result/'.$statusRes.'/'.$job->id) }}" class="tips btn btn-sm  " title="{{ __('core.btn_back') }}"><i class="fa  fa-times"></i></a>		
			</div>
		</div>
		<div class="sbox-content">
		<div class="row">
			<div class="col-md-5">
				<p><span style="width:120px;display:inline-block;">{{ Lang::get('acc.viewpre-name') }}</span><span> : </span><span style="padding-left:10px;">{{ $user->first_name }} {{ $user->last_name }}</span></p>
				<p><span style="width:120px;display:inline-block;">Applied Date</span><span> : </span><span style="padding-left:10px;">{{ $applyDate->apply_date }}</span></p>
				<p><span style="width:120px;display:inline-block;">{{ Lang::get('acc.viewpre-job') }}</span><span> : </span><span style="padding-left:10px;">{{ $job->JobTitleName }}</span></p>
				<p><span style="width:120px;display:inline-block;">{{ Lang::get('acc.viewpre-group') }}</span><span> : </span><span style="padding-left:10px;">{{ $job->name }}</span></p>
				<p><span style="width:120px;display:inline-block;">{{ Lang::get('acc.viewpre-status') }}</span><span> : </span><span style="padding-left:10px;">{{ $statusRes }}</span></p>
			</div>
			
		</div>
			<div class="table-responsive">
				<table class="table " >
					<thead>
						<tr>
							<th>{{ Lang::get('acc.viewpre-soal') }}</th>
							<th>{{ Lang::get('acc.viewpre-jwb') }}</th>
							<th>{{ Lang::get('acc.viewpre-dis') }}</th>
						</tr>
					</thead>
					<tbody>	
					@if(count($setSoal) > 0)
						@foreach($setSoal as $items)
							@if($items->type == 3)
								<tr>
									<td width='30%' class='label-view'>{{ $items->soal }}</td>
									<?php 
											$essayJwb= AccHelpers::get_essay_jwb($items->id_soal,$user->id,$job->id);
									?>
									@if($essayJwb)
										<td colspan = '2'>{{ !empty($essayJwb->jawaban) ? $essayJwb->jawaban : 'No answer'  }}</td>
									@else
										<td colspan = '2'>No answer</td>
									@endif
								</tr>
							@else
							<?php 
								$jwb= AccHelpers::get_jwb($items->id_soal);
							?>
							@foreach($jwb as $itemJwb)
						<tr>
							@if($loop->first)<td width='30%' class='label-view' <?php if($jwb){ echo "rowspan=".count($jwb);} ?> >{{ $items->soal }}</td>@endif
							<td> {{ $itemJwb->jawaban }}
								<?php
										foreach($preRes as $res){
											if($itemJwb->id == $res->id_jawaban){
												echo '<i class="fa fa-check-circle" style="color:green;"></i>';
											}
										}
								?>
							</td>
							<td>@if($itemJwb->disqualified == "2") {{Lang::get('acc.no')}} @elseif($itemJwb->disqualified == "1") {{Lang::get('acc.yes')}} @endif</td>
						</tr>
							@endforeach
							@endif
						@endforeach
					@else
					<tr>
						<td class='label-view'>{{ Lang::get('acc.no-data')}}</td>
					</tr>
					@endif
					</tbody>	
				</table>   

			 	

			</div>
		</div>
	</div>
	</div>
</div>
@stop
