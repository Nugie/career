<section id="blog">
    <div id="home-slider">
       <div style="background: url('{!! asset('uploads/images/TorontoSkyline.jpg') !!}');  background-size: cover; background-size: 100% 100%; height:350px !important;">
         <div class="wrap">
         </div>
       </div>
     </div>
</section>


    <section class="section  bg-light filtercategory" style="padding-top:10px!important">
      <!-- WELCOME -->
      <div class="container filtercategory">
          <br>
        
        

       </div>
      <!-- /WELCOME -->
    </section>

    <style>
    .subtitle-job{
    	font-size: 12px;
    	color: #aaa;
    }
    
    .details-link{
    	float: right;
    }
    
    #custom-search-input {
        margin:0;
        margin-top: 10px;
        padding: 0;
    }

    #custom-search-input .search-query {
        padding-right: 3px;
        padding-right: 4px \9;
        padding-left: 3px;
        padding-left: 4px \9;
        /* IE7-8 doesn't have border-radius, so don't indent the padding */

        margin-bottom: 0;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
    }

    #custom-search-input button {
        border: 0;
        background: none;
        /** belows styles are working good */
        padding: 2px 5px;
        margin-top: 2px;
        position: relative;
        left: -28px;
        /* IE7-8 doesn't have border-radius, so don't indent the padding */
        margin-bottom: 0;
        -webkit-border-radius: 3px;
        -moz-border-radius: 3px;
        border-radius: 3px;
        color:#D9230F;
    }

    .search-query:focus + button {
        z-index: 3;
    }
    
    .effect:hover {
    	box-shadow: 0 0 40px #ddd;
    }
    
    .link-div{
    	text-decoration: none!important;
    	color: #555 !important;
    }
    </style>


    <script>
    $(document).ready(function(){

        $(".filter-button").click(function(){
            var value = $(this).attr('data-filter');

            if(value == "all")
            {
                $('.filter').show('1000');
            }
            else
            {
                $(".filter").not('.'+value).hide('3000');
                $('.filter').filter('.'+value).show('3000');

            }
        });

    });
    </script>
