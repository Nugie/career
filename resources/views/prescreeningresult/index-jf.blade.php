@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
<section class="page-header row">
	<h2> {{ $pageTitle }} </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> {{ $pageTitle }} </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

		<div class="sbox">
			<div class="sbox-title">
				<h1> All Records <small> </small></h1>
				<div class="sbox-tools">
					@if(Session::get('gid') ==1)
						<a href="{{ url('list-prescreening-result') }}" class="tips btn btn-sm  " title=" {{ __('core.btn_reload') }}" ><i class="fa  fa-refresh"></i></a>
					@endif 	
				</div>				
			</div>
			<div class="sbox-content">
			<!-- Toolbar Top -->
			<div class="row">
				<div class="col-md-1 pull-right">
					<div class="input-group">
					      <div class="input-group-btn">
					        <button type="button" class="btn btn-default btn-sm " 
					        onclick="SximoModalTest('{{ url($moduleJob."/search") }}','Advance Search'); " ><i class="fa fa-filter"></i> Filter </button>
					      </div><!-- /btn-group -->
			<!-- <input type="text" class="form-control input-sm onsearch" data-target="{{ url($moduleJob) }}" aria-label="..." placeholder=" Type And Hit Enter "> -->
					    </div>
				</div>    
			</div>		
			<!-- End Toolbar Top -->

			<!-- Table Grid -->
			<div class="table-responsive" style="padding-bottom: 70px;">
			
		    <table class="table table-striped table-hover " id="{{ $pageModule }}Table">
		        <thead>
					<tr>
						<th style="width: 3% !important;" class="number"> No </th>
						<?php $i = ($rowData->currentPage()-1)* $rowData->perPage();?>
						<th align="center">Job Title</th>
						<th align="center">Placement</th>
						<th align="center">Pending Applicants</th>
						<th align="center">Disqualified Applicants</th>
						<th align="center">Accepted Applicants</th>
						<th align="center">Failed Applicants</th>
					  </tr>
		        </thead>
				<tbody>
				@if(count($rowData) > 0)
					@foreach ($rowData as $row)
						<tr>
							<td> {{ ++$i }} </td>
							<td>{{ $row->Job_Title }}</td>
							<td>{{ $row->Placement }}</td>
							<td>
								<div class="dropdown">
									{{ $row->Total_Applicants }} 
									@if( intval($row->Total_Applicants) != 0)
									<a class="dropdown-toggle" type="button" data-toggle="dropdown"></a>
									<ul class="dropdown-menu">
									<li><a  href="{{ url('list-prescreening-result/pending/'.$row->id_job) }}" class="tips"> View</a></li>
									</ul>
									@endif
								</div>
							</td>
							<td>
								<div class="dropdown">
									{{ $row->Total_Disqualified }}
									@if( intval($row->Total_Disqualified) != 0)
									<a class="dropdown-toggle" type="button" data-toggle="dropdown"></a>
									<ul class="dropdown-menu">
									<li><a  href="{{ url('list-prescreening-result/disqualified/'.$row->id_job) }}" class="tips"> View</a></li>
									</ul>
									@endif
								</div>
							</td>
							<td>
								<div class="dropdown">
									{{ $row->Accepted_Applicants }}
									@if( intval($row->Accepted_Applicants) != 0)
									<a class="dropdown-toggle" type="button" data-toggle="dropdown"></a>
									<ul class="dropdown-menu">
									<li><a  href="{{ url('list-prescreening-result/accepted/'.$row->id_job) }}" class="tips"> View</a></li>
									</ul>
									@endif
								</div>
							</td>
							<td>
								<div class="dropdown">
									{{ $row->Failed_Applicants }}
									@if( intval($row->Failed_Applicants) != 0)
									<a class="dropdown-toggle" type="button" data-toggle="dropdown"></a>
									<ul class="dropdown-menu">
									<li><a  href="{{ url('list-prescreening-result/failed/'.$row->id_job) }}" class="tips"> View</a></li>
									</ul>
									@endif
								</div>
							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="6">No data</td>
						</tr>
					@endif
				</tbody>
		    </table>

			<input type="hidden" name="action_task" value="" />
			
			<!--{!! $rowData->appends(request()->input())->links() !!} -->
			</div>
			<!-- End Table Grid -->
			

			</div>
		</div>
	</div>
</div>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
 <style>
 .dataTables_filter {
    display: initial; 
}
</style>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
   <script type="text/javascript">
   
   $(document).ready(function() {
		$('#bulkButton').prop('disabled', true);
    $('#prescreeningresultTable').DataTable();
});

	
		function SximoModalTest( url , title)
		{
			$('#sximo-modal-content').html(' ....Loading content , please wait ...');
			$('.modal-title').html(title);
			$('#sximo-modal-content').load('search-prescreening-result',function(){
			});
			$('#sximo-modal').modal('show');	
		}		
		
	</script>	

@stop