@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }} </h2>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">
{!! Form::open(array('url'=>'change-status-comments', 'class'=>'form-horizontal validated' )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('view-comments/'.$rowData->pageID) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.sb_save') }}" ><i class="fa  fa-check"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="col-md-12">
						<fieldset><legend> Status Comment</legend>	
							{!! Form::hidden('id', $rowData->commentID) !!}
							{!! Form::hidden('pageid', $rowData->pageID) !!}
									  <div class="form-group  " >
										<label for="DatePosted" class=" control-label col-md-4 text-left"> Date </label>
										<div class="col-md-6">
										  <p>{{ $rowData->posted }}</p>
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Comment" class=" control-label col-md-4 text-left"> Comment </label>
										<div class="col-md-6">
										  <p>{{ $rowData->comments }}</p>
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 
									  <div class="form-group  " >
										<label for="Status" class=" control-label col-md-4 text-left"> Status </label>
										<div class="col-md-6">
										  
					<?php
					$type_opt = array( 'pending' => 'Pending' ,  'approved' => 'Approved' , 'rejected' => 'Rejected', ); ?>
					<select name='type' rows='5'   class='select2 '  > 
						<?php 
						foreach($type_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($rowData->comment_status == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 									  													
						</fieldset>
			</div>
			
		
		</div>
	</div>
	{!! Form::close() !!}
	</div>
</div>
		 
@stop