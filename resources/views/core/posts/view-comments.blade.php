@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
<section class="page-header row">
	<h2> {{ $pageTitle }} </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> {{ $pageTitle }} </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

		<div class="sbox">
			<div class="sbox-title">
				<h1> All Records <small> </small></h1>
				<div class="sbox-tools">
					@if(Session::get('gid') ==1)
						<a href="{{ url('list-prescreening-result') }}" class="tips btn btn-sm  " title=" {{ __('core.btn_reload') }}" ><i class="fa  fa-refresh"></i></a>
					@endif 	
				</div>				
			</div>
			<div class="sbox-content">
			<div class="row">
				<div class="col-md-4">
					<a href="{{ url('core/posts') }}" class="btn btn-default btn-sm"  
						title="Back"><i class=" fa fa-arrow-left "></i> Back</a>
				</div>    
			</div>
			<div class="row">
				<h5 style="padding-left: 15px; padding-right:15px;">Title : {{$title}}</h5>
			</div>
			<!-- Table Grid -->
			<div class="table-responsive" style="padding-bottom: 70px;">
			
		    <table class="table table-striped table-hover " id="{{ $pageModule }}Table">
		        <thead>
					<tr>
						<th style="width: 3% !important;" class="number"> No </th>
						<?php $i = ($rowData->currentPage()-1)* $rowData->perPage();?>
						<th align="center" style="width: 10%;">Date</th>
						<th align="center"style="width: 10%;">Name</th>
						<th align="center" style="width: 50%;">Comment</th>
						<th align="center">Status</th>
						<th align="center">Action</th>
					  </tr>
		        </thead>
				<tbody>
				@if(count($rowData) > 0)
					@foreach ($rowData as $row)
						<tr>
							<td> {{ ++$i }} </td>
							<td>{{ $row->posted }}</td>
							<td>{{ $row->comment_name }}</td>
							<td>{{ $row->comments }}</td>
							<td>{{ $row->comment_status }}</td>
							<td>
							 	<div class="dropdown">
								  <button class="btn btn-primary btn-xs dropdown-toggle" type="button" data-toggle="dropdown"> Action </button>
								  <ul class="dropdown-menu">
									<li><a  href="{{ url('status-comments/'.$row->commentID) }}" class="tips"> Change Status</a></li>
									<li><a  href="{{ url('posts/remove/'.$row->pageID.'/'. $row->alias.'/'.$row->commentID) }}" class="tips remove"> Remove</a></li>
								  </ul>
								</div>
							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="4">No data</td>
						</tr>
					@endif
				</tbody>
		    </table>

			<input type="hidden" name="action_task" value="" />
			
			{!! $rowData->links() !!}
			</div>
			<!-- End Table Grid -->
			

			</div>
		</div>
	</div>
</div>

   <script type="text/javascript">
	
		function SximoModalTest( url , title)
		{
			$('#sximo-modal-content').html(' ....Loading content , please wait ...');
			$('.modal-title').html(title);
			$('#sximo-modal-content').load('search-prescreening-result',function(){
			});
			$('#sximo-modal').modal('show');	
		}		
		
        $(function(){
            $('.remove').on('click',function(){
                if(confirm('Remove comment ?'))
                {
                    return true;
                }
                return false;
            })
        })
		
	</script>	

@stop