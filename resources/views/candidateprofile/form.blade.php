@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url($pageModule) }}"> {{ $pageTitle }} </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'candidateprofile?return='.$return, 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<button name="apply" class="tips btn btn-sm btn-apply  "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-check"></i> {{ __('core.sb_apply') }} </button>
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.btn_back') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="col-md-12">
						<fieldset><legend> Candidate Profile</legend>
				{!! Form::hidden('id', $row['id']) !!}{!! Form::hidden('id_user', $row['id_user']) !!}					
									  <div class="form-group  " >
										<label for="Firstname" class=" control-label col-md-4 text-left"> Firstname <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='firstname' id='firstname' value='{{ $row['firstname'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Lastname" class=" control-label col-md-4 text-left"> Lastname <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='lastname' id='lastname' value='{{ $row['lastname'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Title" class=" control-label col-md-4 text-left"> Title <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $title = explode(',',$row['title']);
					$title_opt = array( 'Mr' => 'Mr' ,  'Mrs' => 'Mrs' ,  'Ms' => 'Ms' , ); ?>
					<select name='title' rows='5'   class='select2 '  > 
						<?php 
						foreach($title_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['title'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Gender" class=" control-label col-md-4 text-left"> Gender <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $gender = explode(',',$row['gender']);
					$gender_opt = array( 'female' => 'Female' ,  'Male' => 'Male' , ); ?>
					<select name='gender' rows='5'   class='select2 '  > 
						<?php 
						foreach($gender_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['gender'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Dateofbirth" class=" control-label col-md-4 text-left"> Dateofbirth <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('dateofbirth', $row['dateofbirth'],array('class'=>'form-control input-sm date')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Provinceofbirth" class=" control-label col-md-4 text-left"> Provinceofbirth <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='provinceofbirth' id='provinceofbirth' value='{{ $row['provinceofbirth'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Birthplace" class=" control-label col-md-4 text-left"> Birthplace <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='birthplace' id='birthplace' value='{{ $row['birthplace'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nomorktp" class=" control-label col-md-4 text-left"> Nomorktp <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='nomorktp' id='nomorktp' value='{{ $row['nomorktp'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Homephone" class=" control-label col-md-4 text-left"> Homephone <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='homephone' id='homephone' value='{{ $row['homephone'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Mobilephone" class=" control-label col-md-4 text-left"> Mobilephone <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='mobilephone' id='mobilephone' value='{{ $row['mobilephone'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Maritalstatus" class=" control-label col-md-4 text-left"> Maritalstatus <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $maritalstatus = explode(',',$row['maritalstatus']);
					$maritalstatus_opt = array( 'Single' => 'Single' ,  'Married' => 'Married' ,  'Divorced' => 'Divorced' ,  'Widowed' => 'Widowed' , ); ?>
					<select name='maritalstatus' rows='5'   class='select2 '  > 
						<?php 
						foreach($maritalstatus_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['maritalstatus'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Religion" class=" control-label col-md-4 text-left"> Religion <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $religion = explode(',',$row['religion']);
					$religion_opt = array( 'Moslem' => 'Moslem' ,  'Buddha' => 'Buddha' ,  'Catholic' => 'Catholic' ,  'Christian' => 'Christian' ,  'Konghucu' => 'Konghucu' ,  'Hindu' => 'Hindu' ,  'Others' => 'Others' , ); ?>
					<select name='religion' rows='5'   class='select2 '  > 
						<?php 
						foreach($religion_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['religion'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Lastestducation" class=" control-label col-md-4 text-left"> Lastestducation <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $lastestducation = explode(',',$row['lastestducation']);
					$lastestducation_opt = array( 'SMA' => 'SMA' ,  'SMK' => 'SMK' ,  'D1' => 'D1' ,  'D2' => 'D2' ,  'D3' => 'D3' ,  'D4' => 'D4' ,  'S1' => 'S1' ,  'S2' => 'S2' , ); ?>
					<select name='lastestducation' rows='5'   class='select2 '  > 
						<?php 
						foreach($lastestducation_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['lastestducation'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Universityorschool" class=" control-label col-md-4 text-left"> Universityorschool <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='universityorschool' id='universityorschool' value='{{ $row['universityorschool'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Faculty" class=" control-label col-md-4 text-left"> Faculty <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='faculty' id='faculty' value='{{ $row['faculty'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Major" class=" control-label col-md-4 text-left"> Major <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='major' id='major' value='{{ $row['major'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Gpa" class=" control-label col-md-4 text-left"> Gpa <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='gpa' id='gpa' value='{{ $row['gpa'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Startdate" class=" control-label col-md-4 text-left"> Startdate <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='startdate' id='startdate' value='{{ $row['startdate'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Endate" class=" control-label col-md-4 text-left"> Endate <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='endate' id='endate' value='{{ $row['endate'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Country" class=" control-label col-md-4 text-left"> Country <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='country' id='country' value='{{ $row['country'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Address" class=" control-label col-md-4 text-left"> Address <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='address' id='address' value='{{ $row['address'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Kecamatan" class=" control-label col-md-4 text-left"> Kecamatan <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='kecamatan' id='kecamatan' value='{{ $row['kecamatan'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Kotaorkabupaten" class=" control-label col-md-4 text-left"> Kotaorkabupaten <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='kotaorkabupaten' id='kotaorkabupaten' value='{{ $row['kotaorkabupaten'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Provinsi" class=" control-label col-md-4 text-left"> Provinsi <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='provinsi' id='provinsi' value='{{ $row['provinsi'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Kodepos" class=" control-label col-md-4 text-left"> Kodepos <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='kodepos' id='kodepos' value='{{ $row['kodepos'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Organizationname" class=" control-label col-md-4 text-left"> Organizationname <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='organizationname' id='organizationname' value='{{ $row['organizationname'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Organizationscope" class=" control-label col-md-4 text-left"> Organizationscope <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $organizationscope = explode(',',$row['organizationscope']);
					$organizationscope_opt = array( 'International' => 'International' ,  'National' => 'National' ,  'Region' => 'Region' ,  'University' => 'University' ,  'faculty/major' => 'faculty/major' ,  'Others' => 'Others' , ); ?>
					<select name='organizationscope' rows='5'   class='select2 '  > 
						<?php 
						foreach($organizationscope_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['organizationscope'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Organization Experience Period" class=" control-label col-md-4 text-left"> Organization Experience Period <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='organization_experience_period' id='organization_experience_period' value='{{ $row['organization_experience_period'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Rolefunction" class=" control-label col-md-4 text-left"> Rolefunction <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='rolefunction' id='rolefunction' value='{{ $row['rolefunction'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Roleorposition" class=" control-label col-md-4 text-left"> Roleorposition <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='roleorposition' id='roleorposition' value='{{ $row['roleorposition'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Company" class=" control-label col-md-4 text-left"> Company <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='company' id='company' value='{{ $row['company'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Workingexperienceperiod" class=" control-label col-md-4 text-left"> Workingexperienceperiod <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $workingexperienceperiod = explode(',',$row['workingexperienceperiod']);
					$workingexperienceperiod_opt = array( 'Fulltime' => 'Fulltime' ,  'Freelance' => 'Freelance' ,  'Internship' => 'Internship' , ); ?>
					<select name='workingexperienceperiod' rows='5'   class='select2 '  > 
						<?php 
						foreach($workingexperienceperiod_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['workingexperienceperiod'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Position" class=" control-label col-md-4 text-left"> Position <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='position' id='position' value='{{ $row['position'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Category" class=" control-label col-md-4 text-left"> Category <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $category = explode(',',$row['category']);
					$category_opt = array( 'Marketing/sales' => 'Marketing/sales' ,  'Operation' => 'Operation' ,  'Human Resource' => 'Human Resource' ,  'Finance/Accounting' => 'Finance/Accounting' ,  'Procurement/ Purchasing / GA' => 'Procurement/ Purchasing / GA' ,  'Information Techonology' => 'Information Techonology' ,  'Legal/ Litigation' => 'Legal/ Litigation' , ); ?>
					<select name='category' rows='5'   class='select2 '  > 
						<?php 
						foreach($category_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['category'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Status Working Experience" class=" control-label col-md-4 text-left"> Status Working Experience <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='status_working_experience' id='status_working_experience' value='{{ $row['status_working_experience'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Jobdescription" class=" control-label col-md-4 text-left"> Jobdescription <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='jobdescription' id='jobdescription' value='{{ $row['jobdescription'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Salary" class=" control-label col-md-4 text-left"> Salary <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='salary' id='salary' value='{{ $row['salary'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Reasonofleaving" class=" control-label col-md-4 text-left"> Reasonofleaving <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='Reasonofleaving' id='Reasonofleaving' value='{{ $row['Reasonofleaving'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Careerpreference" class=" control-label col-md-4 text-left"> Careerpreference <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='Careerpreference' id='Careerpreference' value='{{ $row['Careerpreference'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Expectedsalary" class=" control-label col-md-4 text-left"> Expectedsalary <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='expectedsalary' id='expectedsalary' value='{{ $row['expectedsalary'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Additionalinformation" class=" control-label col-md-4 text-left"> Additionalinformation <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $additionalinformation = explode(',',$row['additionalinformation']);
					$additionalinformation_opt = array( 'Acc Employees' => 'Acc Employees' ,  'Campus / University' => 'Campus / University' ,  'Company / Brand Website' => 'Company / Brand Website' ,  'Print Advertisement' => 'Print Advertisement' ,  'Friends / Relatives' => 'Friends / Relatives' ,  'Others' => 'Others' , ); ?>
					<select name='additionalinformation' rows='5'   class='select2 '  > 
						<?php 
						foreach($additionalinformation_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['additionalinformation'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Cv" class=" control-label col-md-4 text-left"> Cv <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='cv' id='cv' value='{{ $row['cv'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		 		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("candidateprofile/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop