@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }} <small> {{ $pageNote }} </small></h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url($pageModule) }}"> {{ $pageTitle }} </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'soaldisc?return='.$return, 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url($pageModule.'?return='.$return) }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<button name="apply" class="tips btn btn-sm btn-apply  "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-check"></i> {{ __('core.sb_apply') }} </button>
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.btn_back') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="col-md-12">
						<fieldset><legend> Soal DISC</legend>
						<div class="form-group  " style="display:none">
										<label for="Id" class=" control-label col-md-4 text-left"> Id <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='id' id='id' value='{{ $row['id'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Nomor Soal" class=" control-label col-md-4 text-left"> Nomor Soal <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='nomor_soal' id='nomor_soal' value='{{ $row['nomor_soal'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Jawaban" class=" control-label col-md-4 text-left"> Jawaban <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='jawaban' id='jawaban' value='{{ $row['jawaban'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Point Pos" class=" control-label col-md-4 text-left"> Point Pos <span class="asterix"> * </span></label>
										<div class="col-md-6">
											<select name="point_pos" id="point_pos" class="form-control">
												<option value="X" {{(!empty($row['point_pos']) && $row['point_pos'] == 'X') ?  'selected' : ''}}>X</option>
												<option value="Y" {{(!empty($row['point_pos']) && $row['point_pos'] == 'Y') ?  'selected' : ''}}>Y</option>
												<option value="Z" {{(!empty($row['point_pos']) && $row['point_pos'] == 'Z') ?  'selected' : ''}}>Z</option>
												<option value="R" {{(!empty($row['point_pos']) && $row['point_pos'] == 'R') ?  'selected' : ''}}>R</option>
												<option value="+/-" {{(!empty($row['point_pos']) && $row['point_pos'] == '+/-') ?  'selected' : ''}}>+/-</option>												
											</select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Point Neg" class=" control-label col-md-4 text-left"> Point Neg <span class="asterix"> * </span></label>
										<div class="col-md-6">
											<select name="point_neg" id="point_neg	" class="form-control">
												<option value="X" {{(!empty($row['point_neg']) && $row['point_neg'] == 'X') ?  'selected' : ''}}>X</option>
												<option value="Y" {{(!empty($row['point_neg']) && $row['point_neg'] == 'Y') ?  'selected' : ''}}>Y</option>
												<option value="Z" {{(!empty($row['point_neg']) && $row['point_neg'] == 'Z') ?  'selected' : ''}}>Z</option>
												<option value="R" {{(!empty($row['point_neg']) && $row['point_neg'] == 'R') ?  'selected' : ''}}>R</option>
												<option value="+/-" {{(!empty($row['point_neg']) && $row['point_neg'] == '+/-') ?  'selected' : ''}}>+/-</option>												
											</select>
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Created At" class=" control-label col-md-4 text-left"> Created At <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
				<div class="input-group m-b" style="width:150px !important;">
					{!! Form::text('created_at', $row['created_at'],array('class'=>'form-control input-sm datetime', 'style'=>'width:150px !important;')) !!}
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				</div>
				 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> </fieldset>
			</div>
			
			

		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		
		
		 		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("soaldisc/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		


		
		var now = new Date();

		var day = ("0" + now.getDate()).slice(-2);
		var month = ("0" + (now.getMonth() + 1)).slice(-2);
		var cHour = now.getHours();
		var cMin = now.getMinutes();
		var cSec = now.getSeconds();

		var today = now.getFullYear()+"-"+(month)+"-"+(day)+" "+(cHour)+":"+(cMin)+":"+(cSec) ;
		$('[name="created_at"]').val(today)
		
	});
	</script>		 
@stop