<section class="section" style="padding-top:40px!important">
		@if(count($soalan) > 0)
			<div class="container">
        <br>
				<h2 id="focusHere">Personality Question : <b> {{$title}} </b></h2>
				<h4 style="margin-top:20px"><b>INSTRUKSI</b> : Anda diharapkan untuk memilih 1 (satu) dari 2 pernyataan yang <u><b>PALING MENGGAMBARKAN</b></u> diri Anda. </h4>
				<h4>Soal ini terdiri dari 60 soal. Pastikan seluruh soal sudah terjawab sebelum Anda menyelesaikan pengerjaan test ini.</h4><br>

                <form class="form" id="soalForm" method="POST" action="{{ URL('save/mbti')}}">
                <fieldset>
									@php $temp = 1; @endphp
										<table class=" table table-responsive">
										<thead>
											<tr>
												<th style="text-align:center">Question Number</th>
												<th colspan="2" style="text-align:center">First Statement</th>
												<th colspan="2" style="text-align:center">Second Statement</th>
											</tr>
										</thead>
										
										@foreach($soalanKat1 as $question) 
										<!-- <tbody> -->
											<tr>
												<td class="col-sm-1" style="text-align:center ; ">{{$question->nomor_soal}}</td>
														@foreach($jawaban as $answer)
															@if($question->nomor_soal == $answer->nomor_soal)
																@if($temp == 1)
																	<td class="col-sm-4; paragraf" style="text-align:right ; ">
																		<label for="rb{{ $answer->nomor_soal }}{{ $answer->point }}" style="font-weight:initial"> {{ $answer->jawaban }} </label> 
																	</td>
																	<td class="col-sm-1" style="text-align:center ; ">
																		<input name="radiobutton{{ $answer->nomor_soal }}" id = "rb{{ $answer->nomor_soal }}{{ $answer->point }}" type="radio" value="{{ $answer->point }}" class="jawaban" data-ques = "{{ $answer->nomor_soal}}"/>
																	</td>
																	@php $temp = 2; @endphp
																@else
																	<td class="col-sm-1" style="text-align:center; ">
																		<input name="radiobutton{{ $answer->nomor_soal }}" id = "rb{{ $answer->nomor_soal }}{{ $answer->point }}" type="radio" value="{{ $answer->point }}" class="jawaban" data-ques = "{{ $answer->nomor_soal}}"/>
																	</td>
																	<td class="col-sm-4; paragraf">
																		<label for="rb{{ $answer->nomor_soal }}{{ $answer->point }}" style="font-weight:initial"> {{ $answer->jawaban }} </label>
																	</td>
																	<td class="col-sm-1">
																		<input type="text" style="display:none" value="0" id="temppoint{{$question->nomor_soal}}">
																	</td>
																	
																	@php $temp = 1; @endphp
																@endif
															@endif
														@endforeach
												</td>
											</tr>
										<!-- </tbody>							 -->
										
										@endforeach
										
										
										</table>

										<button type="button" id="nextbtn" class="btn btn-primary submitpre-btn next">Next</button>
									
								</fieldset>
								
                <fieldset>
									
										@php $temp = 1; @endphp
										<table class=" table table-responsive">
										<thead>
											<tr>
												<th style="text-align:center">Question Number</th>
												<th colspan="2" style="text-align:center">First Statement</th>
												<th colspan="2" style="text-align:center">Second Statement</th>
											</tr>
										</thead>
										
										@foreach($soalanKat2 as $question) 
										<!-- <tbody> -->
											<tr>
												<td class="col-sm-1" style="text-align:center ; ">{{$question->nomor_soal}}</td>
														@foreach($jawaban2 as $answer)
															@if($question->nomor_soal == $answer->nomor_soal)
																@if($temp == 1)
																	<td class="col-sm-4; paragraf" style="text-align:right ; ">
																		<label for="rb{{ $answer->nomor_soal }}{{ $answer->point }}" style="font-weight:initial"> {{ $answer->jawaban }} </label> 
																	</td>
																	<td class="col-sm-1" style="text-align:center ; ">
																		<input name="radiobutton{{ $answer->nomor_soal }}" id = "rb{{ $answer->nomor_soal }}{{ $answer->point }}" type="radio" value="{{ $answer->point }}" class="jawaban" data-ques = "{{ $answer->nomor_soal}}"/>
																	</td>
																	@php $temp = 2; @endphp
																@else
																	<td class="col-sm-1" style="text-align:center; ">
																		<input name="radiobutton{{ $answer->nomor_soal }}" id = "rb{{ $answer->nomor_soal }}{{ $answer->point }}" type="radio" value="{{ $answer->point }}" class="jawaban" data-ques = "{{ $answer->nomor_soal}}"/>
																	</td>
																	<td class="col-sm-4; paragraf">
																		<label for="rb{{ $answer->nomor_soal }}{{ $answer->point }}" style="font-weight:initial"> {{ $answer->jawaban }} </label>
																	</td>
																	
																	@php $temp = 1; @endphp
																@endif
															@endif
														@endforeach
												</td>
											</tr>
										<!-- </tbody>							 -->
										
										@endforeach
										
										
										</table>
										
										<button type="button" id="prebtn" class="btn btn-primary submitpre-btn prev">Previous</button>
										<button type="button" id="nextbtn" class="btn btn-primary submitpre-btn next">Next</button>
								
								
								</fieldset>
								<fieldset>
									
										@php $temp = 1; @endphp
										<table class=" table table-responsive">
										<thead>
											<tr>
												<th style="text-align:center">Question Number</th>
												<th colspan="2" style="text-align:center">First Statement</th>
												<th colspan="2" style="text-align:center">Second Statement</th>
											</tr>
										</thead>
										
										@foreach($soalanKat3 as $question) 
										<!-- <tbody> -->
											<tr>
												<td class="col-sm-1" style="text-align:center ; ">{{$question->nomor_soal}}</td>
														@foreach($jawaban3 as $answer)
															@if($question->nomor_soal == $answer->nomor_soal)
																@if($temp == 1)
																	<td class="col-sm-4; paragraf" style="text-align:right ; ">
																		<label for="rb{{ $answer->nomor_soal }}{{ $answer->point }}" style="font-weight:initial"> {{ $answer->jawaban }} </label> 
																	</td>
																	<td class="col-sm-1" style="text-align:center ; ">
																		<input name="radiobutton{{ $answer->nomor_soal }}" id = "rb{{ $answer->nomor_soal }}{{ $answer->point }}" type="radio" value="{{ $answer->point }}" class="jawaban" data-ques = "{{ $answer->nomor_soal}}"/>
																	</td>
																	@php $temp = 2; @endphp
																@else
																	<td class="col-sm-1" style="text-align:center; ">
																		<input name="radiobutton{{ $answer->nomor_soal }}" id = "rb{{ $answer->nomor_soal }}{{ $answer->point }}" type="radio" value="{{ $answer->point }}" class="jawaban" data-ques = "{{ $answer->nomor_soal}}"/>
																	</td>
																	<td class="col-sm-4; paragraf">
																		<label for="rb{{ $answer->nomor_soal }}{{ $answer->point }}" style="font-weight:initial"> {{ $answer->jawaban }} </label>
																	</td>
																	
																	@php $temp = 1; @endphp
																@endif
															@endif
														@endforeach
												</td>
											</tr>
										<!-- </tbody>							 -->
										
										@endforeach
										
										
										</table>

										<button type="button" id="prebtn" class="btn btn-primary submitpre-btn prev">Previous</button>
										<button type="button" id="nextbtn" class="btn btn-primary submitpre-btn next">Next</button>
								
								
								</fieldset>
								<fieldset>
									
										@php $temp = 1; @endphp
										<table class=" table table-responsive">
										<thead>
											<tr>
												<th style="text-align:center">Question Number</th>
												<th colspan="2" style="text-align:center">First Statement</th>
												<th colspan="2" style="text-align:center">Second Statement</th>
											</tr>
										</thead>
										
										@foreach($soalanKat4 as $question) 
										<!-- <tbody> -->
											<tr>
												<td class="col-sm-1" style="text-align:center ; ">{{$question->nomor_soal}}</td>
														@foreach($jawaban4 as $answer)
															@if($question->nomor_soal == $answer->nomor_soal)
																@if($temp == 1)
																	<td class="col-sm-4; paragraf" style="text-align:right ; ">
																		<label for="rb{{ $answer->nomor_soal }}{{ $answer->point }}" style="font-weight:initial"> {{ $answer->jawaban }} </label> 
																	</td>
																	<td class="col-sm-1" style="text-align:center ; ">
																		<input name="radiobutton{{ $answer->nomor_soal }}" id = "rb{{ $answer->nomor_soal }}{{ $answer->point }}" type="radio" value="{{ $answer->point }}" class="jawaban" data-ques = "{{ $answer->nomor_soal}}"/>
																	</td>
																	@php $temp = 2; @endphp
																@else
																	<td class="col-sm-1" style="text-align:center; ">
																		<input name="radiobutton{{ $answer->nomor_soal }}" id = "rb{{ $answer->nomor_soal }}{{ $answer->point }}" type="radio" value="{{ $answer->point }}" class="jawaban" data-ques = "{{ $answer->nomor_soal}}"/>
																	</td>
																	<td class="col-sm-4; paragraf">
																		<label for="rb{{ $answer->nomor_soal }}{{ $answer->point }}" style="font-weight:initial"> {{ $answer->jawaban }} </label>
																	</td>
																	
																	@php $temp = 1; @endphp
																@endif
															@endif
														@endforeach
												</td>
											</tr>
										<!-- </tbody>							 -->
										
										@endforeach
										
										
										</table>
										<button type="button" id="prebtn" class="btn btn-primary submitpre-btn prev">Previous</button>
										<button type="button" id="submitpre" class="btn btn-primary submitpre-btn" onclick="validate()">SUBMIT</button>
								
								
								</fieldset>
									
										
										  

				<!-- <button type="button" id="submitpre" class="btn btn-primary submitpre-btn" data-toggle="modal" data-target="#confirmpre">
					SUBMIT
				</button> -->
				
				
				
				
				<div class="modal fade" id="confirmpre" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
					  </div>
					  <div class="modal-body">
						Apakah Anda yakin untuk melakukan submit ?
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						<button type="button" id="save" class="btn btn-primary">Submit</button>
					  </div>
					</div>
				  </div>
				</div>
				</form>
				<br>
				<div  id="success-alert" class="alert alert-success" style="display:none;">
					Anda telah berhasil melakukan submit prescreening ini. Harap cek email Anda.
					<button type="button" class="close" onclick="closee()" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div id="error-alert" class="alert alert-danger" style="display:none;">
					Terjadi kesalahan pada sistem. Silakan coba lagi atau hubungi administrator.
					<button type="button" class="close" onclick="closee()" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div  id="warning-alert" class="alert alert-warning" style="display:none;">
					Terdapat soal yang belum dijawab
					<button type="button" class="close" onclick="closee()" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				
				<!-- <div class="modal fade" id="confirmpre" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				  <div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title" id="myModalLabel">Confirmation</h4>
									</div>
										<div class="modal-body">
											Apakah Anda yakin untuk melakukan submit ?
										</div>
									<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" id="save" class="btn btn-primary">Submit</button>
							</div>
						</div>
				  </div>
				</div> -->
				<!-- ntar dulu  -->
				
			</div>
		@else
			<div class="container">
				<h2 style="margin-top:40px;">Sorry, question not available.</h2>
			</div>
		@endif
</section>

<style>
.soal {
	margin: 5px 0 5px 5px;
	font-size: 16px;
	font-weight: bold;
}

.span-jwb{
	font-size: 16px;
}

.group-jwb{
	margin: 5px;
}

.submitpre-btn{
	width: 139px;
	height: 35px;
	border-radius: 5px;
	background-color: #0089d1;
	border-color: #0089d1
}

 
form#soalForm fieldset:not(:first-of-type), #plotTable {
    display: none;
  }


</style>


<script>

var temp = 1;
var back = 2;
$(document).ready(function(){
	var current = 1, current_step, next_step, steps;
	steps = $("fieldset").length;
		$(".next").click(function(){
			current_step = $(this).parent("fieldset")
			next_step = $(this).parent("fieldset").next()
			next_step.show()
			current_step.hide()
			$('html,body').animate({
        scrollTop: $("#focusHere").offset().top}, 1000);
		})
		$(".prev").click(function(){
			current_step = $(this).parent("fieldset");
			next_step = $(this).parent("fieldset").prev();
			next_step.show();
			current_step.hide();
			$('html,body').animate({
        scrollTop: $("#focusHere").offset().top}, 1000);
		})
		$('input[type=radio]').on('change',function(e){
			var temp = $(this).data("ques")
			$('#temppoint'+temp).val(e.target.value).change
		})

})
	
	function validate(){
		var flag = true;
			$(':radio').each(function () {
				name = $(this).attr('name');
				if (flag && !$(':radio[name="' + name + '"]:checked').length) {	
					flag = false;
					//return alert(name + ' group not checked');
				}
			});
			
			if(flag){
				$("#confirmpre").modal('show');
			} else {
				$('#warning-alert').fadeIn();
			}
	}
	
	function closee(){
		$('.alert').fadeOut();
	}
	
	$(document).ready(function(){
		var baseUrl = {!! json_encode(url('/')) !!}
		$.urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if(results)
        return results[1] || 0;
    }

		if($.urlParam('type') == 'success')
		{
			swal({
        title   : "Berhasil disubmit!",  
        text    : "Good Luck!!",
        icon    : "success",
        customClass: 'sweetalert-lg',
        button   : "OK!",
			}).then((result) => {
				window.location = baseUrl + '/user/profile';
			});
		}
		if($.urlParam('type') == 'fail')
		{
			swal({
        title   : "Terjadi kesalahan saat menginputkan data",  
        text    : "Silakan mencoba lagi",
        icon    : "error",
        customClass: 'sweetalert-lg',
        button   : "OK!",	
			}).then((result) => {
				window.location = baseUrl + '/show/mbti';
			});
		}
		$("#save").on("click",function(){
			var form = $("#soalForm");
			// var length = $('[id^=temppoint]').length
			// var value = $('[id^=temppoint]')
			// console.log(length)
			$('#submitpre').prop("disabled", true);
			$("#confirmpre").modal('hide');
			// var resI=0, resE=0, resS=0, resN=0, resT=0, resF=0, resJ=0, resP=0
			var urlBack = $('#urlback').val();
			
			form.submit();
			// let sendvalue = {};
			// for(i=0; i<length; i++)
			// {
			// 	// Object.assign(sendvalue,{[i+1]:value[i].value})
			// 	if(value[i].value == "I")
			// 	{
			// 		resI = resI + 1;
			// 	}
			// 	else if(value[i].value == "E")
			// 	{
			// 		resE = resE + 1;
			// 	}
			// 	else if(value[i].value == "S")
			// 	{
			// 		resS = resS + 1;
			// 	}
			// 	else if(value[i].value == "N")
			// 	{
			// 		resN = resN + 1;
			// 	}
			// 	else if(value[i].value == "T")
			// 	{
			// 		resT = resT + 1;
			// 	}
			// 	else if(value[i].value == "F")
			// 	{
			// 		resF = resF + 1;
			// 	}
			// 	else if(value[i].value == "J")
			// 	{
			// 		resJ = resJ + 1;
			// 	}
			// 	else if(value[i].value == "P")
			// 	{
			// 		resP = resP + 1;
			// 	}
			// }
			// if(resI > resE)
			// {
			// 	Object.assign(sendvalue, {"I": resI})
			// }
			// else if(resI < resE)
			// {
			// 	Object.assign(sendvalue, {"E": resE})
			// }
			// if(resS > resN)
			// {
			// 	Object.assign(sendvalue, {"S": resS})
			// }
			// else if(resS < resN)
			// {
			// 	Object.assign(sendvalue, {"N": resN})
			// }
			// if(resT > resF)
			// {
			// 	Object.assign(sendvalue, {"T": resT})
			// }
			// else if(resT < resF)
			// {
			// 	Object.assign(sendvalue, {"F": resF})
			// }
			// if(resJ > resP)
			// {
			// 	Object.assign(sendvalue, {"J": resJ})
			// }
			// else if(resJ < resP)
			// {
			// 	Object.assign(sendvalue, {"P": resP})
			// }
			
			// $.ajax({
			// 	url     : "{{ url('save/mbti') }}",
			// 	method  : "POST",
			// 	data    : {"hasil" : sendvalue} ,
			// 	success : function(){
			// 		$('#submitpre').prop("disabled", true);
			// 		$('#success-alert').fadeIn();
			// 		setTimeout(function(){
			// 			window.location = baseUrl + '/' + 'user/profile';
			// 		}, 5000);
			// 	}, 
			// 	error : function(){
			// 		$('#error-alert').fadeIn();
			// 		$('#submitpre').prop("disabled", false);
			// 	}
			// });
					// console.log(essayData);

		}); 

});
</script>

	