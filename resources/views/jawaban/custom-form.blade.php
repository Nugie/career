@extends('layouts.app')

@section('content')
<section class="page-header row">
	<h2> {{ $pageTitle }}</h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li><a href="{{ url($pageModule) }}"> {{ $pageTitle }} </a></li>
		<li class="active"> Form  </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

	{!! Form::open(array('url'=>'prescreening-management/answer/save', 'class'=>'form-horizontal validated','files' => true )) !!}
	<div class="sbox">
		<div class="sbox-title clearfix">
			<div class="sbox-tools " >
				<a href="{{ url('prescreening-management/answer/'.$id_soal.'/view') }}" class="tips btn btn-sm "  title="{{ __('core.btn_back') }}" ><i class="fa  fa-times"></i></a> 
			</div>
			<div class="sbox-tools pull-left" >
				<button name="apply" class="tips btn btn-sm btn-apply  "  title="{{ __('core.sb_apply') }}" ><i class="fa  fa-check"></i> {{ __('core.sb_apply') }} </button>
				<button name="save" class="tips btn btn-sm btn-save"  title="{{ __('core.sb_save') }}" ><i class="fa  fa-paste"></i> {{ __('core.sb_save') }} </button> 
			</div>
		</div>	
		<div class="sbox-content clearfix">
	<ul class="parsley-error-list">
		@foreach($errors->all() as $error)
			<li>{{ $error }}</li>
		@endforeach
	</ul>		
<div class="col-md-12">
						<fieldset><legend> Jawaban</legend>
				{!! Form::hidden('id', $row['id']) !!}
				{!! Form::hidden('id_soal',$id_soal) !!}								
									  <div class="form-group  " >
										<label for="Jawaban" class=" control-label col-md-4 text-left"> Jawaban <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <textarea name='jawaban' rows='5' id='jawaban' class='form-control input-sm '  
				           >{{ $row['jawaban'] }}</textarea> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Disqualified" class=" control-label col-md-4 text-left"> Disqualified <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  
					<?php $disqualified = explode(',',$row['disqualified']);
					$disqualified_opt = array( '1' => 'Yes' ,  '2' => 'No' , ); ?>
					<select name='disqualified' rows='5'   class='select2 '  > 
						<?php 
						foreach($disqualified_opt as $key=>$val)
						{
							echo "<option  value ='$key' ".($row['disqualified'] == $key ? " selected='selected' " : '' ).">$val</option>"; 						
						}						
						?></select> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
									  </div> 					
									  <div class="form-group  " >
										<label for="Skor" class=" control-label col-md-4 text-left"> Skor <span class="asterix"> * </span></label>
										<div class="col-md-6">
										  <input  type='text' name='skor' id='skor' value='{{ $row['skor'] }}' 
						     class='form-control input-sm ' /> 
										 </div> 
										 <div class="col-md-2">
										 	
										 </div>
										 <?php $now = date("Y-m-d H:i:s"); ?>
									  </div> {!! Form::hidden('createdOn', $now) !!}</fieldset>
			</div>
			
			

		</div>
	</div>
	<input type="hidden" name="action_task" value="save" />
	{!! Form::close() !!}
	</div>
</div>		
	
		 
   <script type="text/javascript">
	$(document).ready(function() { 
		 		 

		$('.removeMultiFiles').on('click',function(){
			var removeUrl = '{{ url("jawaban/removefiles?file=")}}'+$(this).attr('url');
			$(this).parent().remove();
			$.get(removeUrl,function(response){});
			$(this).parent('div').empty();	
			return false;
		});		
		
	});
	</script>		 
@stop