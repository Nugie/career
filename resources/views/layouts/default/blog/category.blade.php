<section>
  <div class="col-md-12" style="margin-top: 30px; margin-bottom:0px!important; padding-left: 0px;padding-right: 0px;">
    <div class="row" style="margin:0;">   
      <div class="col-md-12" style=" margin-top: 30px; margin-bottom:0px!important;">
      <div class="col-md-1"></div>
              <div class="col-md-11">         
                    <ul class="nav">
                    <?php $categories = PostHelpers::cloudtags();
                    foreach($categories as $cat) { ?>
                      <li style="display: inline-block; background-color:white; border: 1px solid #c8c9ca;border-radius: 5px;" id="listcategorynews">
                      <a href="{{  url('posts?label='.$cat['tags']) }}">
                        <!-- penghapusan angka-->
                      {{ $cat['tags'] }} <span class="pull-right badge label-primary" style="margin-left: 10px;"></span>
                      </a> 
                      </li>                      
                    <?php } ?>
                    </ul>
              </div>
            </div>
       </div>

<?php
$jumlah_artikel = count($posts);
$no = 1;
?>

@foreach($posts as $post)

@if (($no % 2) == 0)

        <div class="row" style="padding: 20px 0;">  
                  <div class="col-md-6 col-lg-push-6" style="padding-left: 0px;padding-right: 0px;margin-left: 0px;">
                   <a href="{{ url('posts/'.$post->alias) }}">
                             @if(file_exists('./uploads/images/'.$post->image) && $post->image !='' )
							 <br>
                              <img src="{{ asset('uploads/images/'.$post->image) }}" alt="" class="img-responisve" />
				<br>
                              @else
                              <img src="{{ asset('uploads/images/no-image.png') }}" alt="">
                              @endif 
                    </a>         
                  </div>  		
                  <div class="col-md-6 col-lg-pull-6"  style="padding: 0 50px;">
                    <h3 class="small-title" style="font-size:25px;color: black; font-style:bold!important;"><a href="{{ url('posts/'.$post->alias) }}">{{ $post->title }}</a></h3>
                    <p style="font-size:15px"><i class="fa fa-calendar"></i> {{ Carbon\Carbon::parse($post->updated)->format('d M Y') }} </p>
                    <h5 style="line-height: 1.6;text-align: justify;">{{ strip_tags(str_limit($post->note, $limit = 500, $end = '.......')) }}</h5>              
                    &nbsp;      
                    <a href="{{ url('posts/'.$post->alias) }}" style="color:#ffffff;"><button class="btn btn-danger seemore-btn" type="button"> Read More</button></a>
                  </div>        
                </div>
@else
                      <div class="row" style="padding: 20px 0;">          
                          <div class="col-md-6" style="padding-left: 0px;padding-right: 0px;margin-left: 0px;">
                            <a href="{{ url('posts/'.$post->alias) }}">
								@if(file_exists('./uploads/images/'.$post->image) && $post->image !='' )
                              <img src="{{ asset('uploads/images/'.$post->image) }}" alt="" class="img-responisve">
                              @else
                              <img src="{{ asset('uploads/images/no-image.png') }}" alt="">
                              @endif
                            </a>          
                          </div>
                          <div class="col-md-6" style="padding: 0 50px;">
                            <p style="font-size:25px;color: black; font-style:bold!important;"><a href="{{ url('posts/'.$post->alias) }}">{{ $post->title }}</a><p>
                            <p style="font-size:15px"><i class="fa fa-calendar"></i> {{ Carbon\Carbon::parse($post->updated)->format('d M Y') }} </p>
                            <h5 style="line-height: 1.6;text-align: justify;">{{ strip_tags(str_limit($post->note, $limit = 500, $end = '.......')) }}</h5>
                            
                            <a href="{{ url('posts/'.$post->alias) }}" style="color:#fff"><button class="btn btn-danger seemore-btn" type="button">Read More</button></a>
                          </div>          
                        </div>
                    <div>
@endif
<?php $no++ ?>
@endforeach

    <div>
</div>
</div>
</section>


  <div class="col-md-12 custom-pagination">
   {!! $posts->appends(request()->except('page'))->links() !!}
  </div>
    
<section>
    <div class="row" style="margin:0;">
		<div class="col-md-12" style="padding:0;">
    <img src="{{ asset('uploads/config/footer.png') }}" alt="Flowers in Chania" style="background-size: cover; background-size: 100% 100%; width:1517px">
    </div>
</section>