<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ACC Career</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#0072BC" style="padding: 40px 0 30px 0; color: #fff; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<p>ACC Career</p>
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>Hello {{$firstname}} {{$lastname}},</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p>Good news!</p>
										<p>Astra Credit Companies (ACC) sedang mencari kandidat untuk :</p>
										@foreach($arrayjob as $job)
										<table border="0" cellpadding="10" width="80%" style="border: 3px solid #0072bc">
											<tr>
												<td width="80%">
													<p style="text-transform: uppercase; font-weight: bold; color: #000000;">{{$job->job_title}}</p>
													<span><img src="{!! asset('public/frontend/default/images/location.png') !!}" /> {{$job->location}}</p></span>
													<p>
														@if(!empty($job->max_age))
															&#8226; Max {{$job->max_age}} tahun<br/>
														@endif
														@if(!empty($job->min_degree))
															&#8226; Pendidikan min {{$job->min_degree}}<br/>
														@endif
														@if(!empty($job->min_ipk))
															&#8226; Min IPK  {{$job->min_ipk}}
														@endif
													</p>
												</td>
												<td width="20%" valign="bottom">
													<a href="{{url('job-list/'.$job->id_jobfield.'/'.$job->id_job.'/'.str_slug($job->job_title, '-'))}}" style="cursor: pointer;">
														<input style="background-color:#F7941D; color: #fff; padding: 15px 25px; font-size: 16px; border-radius: 5px; border: 0 solid transparent;" type="button" value="Apply Now">
													</a>
												</td>
											</tr>
										</table>
										@endforeach
										<p>Jika Anda tertarik untuk melihat lowongan kerjanya, silakan klik di <a href="{{ url('job-list') }}" style="color: #000; cursor: pointer;">SINI</a>.</p>
										<p>Mohon berhati-hati untuk penipuan proses recruitment dalam bentuk apapun yang mengatas namakan ACC. Kami tidak pernah memungut biaya apapun untuk kandidat melakukan proses rekrutmen. Seluruh informasi dan undangan terkait dengan proses rekrutment akan dikirimkan melalui email dengan domain “@acc.co.id”. </p><br/>
										<p>Terima kasih.</p><br/>
										<p>Salam,<br/>
										Tim Rekrutmen<br/>
										Astra Credit Companies</p>
									</td>
								</tr>
								<tr>
									<td>
										<p style="padding: 0 2%; float: right;">Klik disini untuk <a href="{{ url('unsubscribe/'.$id_user) }}" style="color: #000; cursor: pointer;">unsubscribe</a></p>
									</td>
								</tr>
								<tr>
									<td>
										<hr/>
										<br/>
									</td>
								</tr>
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>Hello {{$firstname}} {{$lastname}},</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p>Good news!</p>
										<p>Astra Credit Companies (ACC) is looking candidates for :</p>
										@foreach($arrayjob as $job)
										<table border="0" cellpadding="10" width="80%" style="border: 3px solid #0072bc">
											<tr>
												<td width="80%">
													<p style="text-transform: uppercase; font-weight: bold; color: #000000;">{{$job->job_title}}</p>
													<span><img src="{!! asset('public/frontend/default/images/location.png') !!}" /> {{$job->location}}</p></span>
													<p>
														@if(!empty($job->max_age))
															&#8226; Max {{$job->max_age}} tahun<br/>
														@endif
														@if(!empty($job->min_degree))
															&#8226; Pendidikan min {{$job->min_degree}}<br/>
														@endif
														@if(!empty($job->min_ipk))
															&#8226; Min IPK  {{$job->min_ipk}}
														@endif
													</p>
												</td>
												<td width="20%" valign="bottom">
													<a href="{{url('job-list/'.$job->id_jobfield.'/'.$job->id_job.'/'.str_slug($job->job_title, '-'))}}">
														<input style="background-color:#F7941D; color: #fff; padding: 15px 25px; cursor: pointer; font-size: 16px; border-radius: 5px; border: 0 solid transparent;" type="button" value="Apply Now">
													</a>
												</td>
											</tr>
										</table>
										@endforeach
										<p>If you are interested in other job opportunities, you can click <a href="{{ url('job-list') }}" style="color: #000; cursor: pointer;">HERE</a>.</p>
										<p>Please beware for any kind of fake recruitment process on behalf of ACC. We never take any recruitment cost for this process. All of the information regarding to our recruitment process will be sent through email with “@acc.co.id” domain.</p>
										<p>Thank you.</p><br/>
										<p>Best regards,<br/>
										Recruitment Team<br/>
										Astra Credit Companies</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<p style="padding: 0 2%; float: right;">Click here to <a href="{{ url('unsubscribe/'.$id_user) }}" style="color: #000; cursor: pointer;">unsubscribe</a></p>
						</td>
					</tr>
					<tr>
						<td bgcolor="#0072BC" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="100%">
										<p class="copyright-text col-md-6">&copy; <?php echo date('Y');?>  <b> {{ config('sximo.cnf_comname')}}</b> 
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>