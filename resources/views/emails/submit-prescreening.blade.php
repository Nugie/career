<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ACC Career</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="800" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="center" bgcolor="#0072BC" style="padding: 40px 0 30px 0; color: #fff; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
							<p>ACC Career</p>
						</td>
					</tr>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>Dear {{$firstname}} {{$lastname}},</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p>Terima kasih atas ketertarikan Anda bergabung di Astra Credit Companies (ACC) sebagai <b><i>{{ $job }} {{ $placement }}</i></b>. Kami akan review aplikasi lamaran kerja Anda dengan seksama dan akan segera merespon Anda untuk informasi proses recruitment berikutnya.</p>
										<p>Dalam sementara ini, Anda tidak dapat apply untuk posisi lainnya hingga aplikasi Anda untuk posisi ini berakhir. Anda dapat selalu menggunakan username/email dan password Anda untuk cek status terakhir dari aplikasi Anda pada web career kami.</p>
										<p>Mohon berhati-hati untuk penipuan proses recruitment dalam bentuk apapun yang mengatas namakan ACC. Kami tidak pernah memungut biaya apapun untuk kandidat melakukan proses rekrutmen. Seluruh informasi dan undangan terkait dengan proses rekrutment akan dikirimkan melalui email dengan domain “@acc.co.id”. </p>
										<p>Terima kasih.</p>
										<p>Salam,<br/>
										Tim Rekrutmen<br/>
										Astra Credit Companies</p>
									</td>
								</tr>
								<tr>
									<td>
										<hr/>
										<br/>
									</td>
								</tr>
								<tr>
									<td style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>Dear {{$firstname}} {{$lastname}},</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p>Thank you for your interest in joining Astra Credit Companies (ACC) as <b><i>{{ $job }} {{ $placement }}</i></b>. We will review your application thoroughly and soon response the information for your next recruitment process.</p>
										<p>In the meantime, you are not able to apply for other vacancies until your application for this position is ended. You may use your username/email and password to check you last status on our career website.</p>
										<p>Please beware for any kind of fake recruitment process on behalf of ACC. We never take any recruitment cost for this process. All of the information regarding to our recruitment process will be sent through email with “@acc.co.id” domain.</p>
										<p>Thank you.</p>
										<p>Best regards,<br/>
										Recruitment Team<br/>
										Astra Credit Companies</p>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#0072BC" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="100%">
										<p class="copyright-text col-md-6">&copy; <?php echo date('Y');?>  <b> {{ config('sximo.cnf_comname')}}</b> 
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>