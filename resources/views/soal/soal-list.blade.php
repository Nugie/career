@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
<?php
$userId = Auth::user()->id;
$is_user= AccHelpers::is_user($userId);
 ?>
<section class="page-header row">
	<h2> {{ $pageTitle }} </h2>
	<ol class="breadcrumb">
		<li><a href="{{ url('') }}"> Dashboard </a></li>
		<li class="active"> {{ $pageTitle }} </li>		
	</ol>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

		<div class="sbox">
		@if(!$is_user)
			<div class="sbox-title">
				<h1> {{ $kategori->name}} <small> </small></h1>
				<div class="sbox-tools">
					@if(Session::get('gid') ==1)
						<a href="{{ url('prescreening-management/question/'.$kategori->id.'/view') }}" class="tips btn btn-sm  " title=" {{ __('core.btn_reload') }}" ><i class="fa  fa-refresh"></i></a>
					@endif 	
				</div>				
			</div>
			<div class="sbox-content">
			<!-- Toolbar Top -->
			<div class="row">
				<div class="col-md-4"> 	
					<a href="{{ url('prescreening-management/question/'.$kategori->id.'/create') }}" class="btn btn-default btn-sm"  
						title="{{ __('core.btn_create') }}"><i class=" fa fa-plus "></i> Create New Question </a>
					<a href="{{ url('prescreening-management') }}" class="btn btn-default btn-sm"  
						title="Back"><i class=" fa fa-arrow-left "></i> Back</a>
				</div>  
			</div>					
			<!-- End Toolbar Top -->
			<!-- Table Grid -->
			<div class="table-responsive" style="padding-bottom: 70px;">
			
		    <table class="table table-striped table-hover " id="{{ $pageModule }}Table">
		        <thead>
					<tr>
						<th style="width: 3% !important;" class="number"> No </th>
						<?php $i = ($rowData->currentPage()-1)* $rowData->perPage();?>
						<th style="width: 60% !important;" align="center">Question</th>
						<th align="center">Type</th>
						<th align="center">Order</th>
						<th align="center">Action</th>
					  </tr>
		        </thead>
				<tbody>
				@if(count($rowData) > 0)
					@foreach ($rowData as $row)
						<tr>
							<td> {{ ++$i }} </td>
							<td>{{ $row->soal }}</td>
							@if($row->type == 1)
							<td>Checkbox</td>
							@elseif ($row->type == 2)
							<td>Multiple Choice</td>
							@elseif ($row->type == 3)
							<td>Essay</td>
							@endif
							<td>{{ $row->order_number }}</td>
							<td>
								@if($row->type!=3)
								<a href="{{ url('prescreening-management/answer/'. $row->id .'/view') }}"><button class="btn btn-primary btn-xs" type="button" title="View Answer"><i class="fa fa-question"></i> </button></a>  
								@endif
								<a href="{{ url('prescreening-management/question/'.$kategori->id.'/'.$row->id.'/edit') }}"><button class="btn btn-primary btn-xs" type="button" title="Edit Question"><i class="fa fa-edit"></i> </button></a>  
								<button class="btn btn-primary btn-xs" type="button" title="Remove" onclick="confirmDelete({{$kategori->id}},{{$row->id}})"><i class="fa fa-times"></i> </button> 
							</td>
						</tr>
					@endforeach
					@else
						<tr>
							<td colspan="6">No data</td>
						</tr>
					@endif
				</tbody>
		    </table>

			<input type="hidden" name="action_task" value="" />
			
			{!! $rowData->links() !!}
			</div>
			<!-- End Table Grid -->
			

			</div>
		</div>
		@else
			<div>Permission not allowed</div>
		@endif
	</div>
</div>

@stop

<script type="text/javascript">

	function confirmDelete(idGroup, id){
		if(confirm('Are you sure you want to delete this?')){
			$.ajax({
                url: "{{ url('prescreening-management/question/delete') }}",
                method: "POST",
                data: {"idGroup" : idGroup, "id": id },
				success : function(){
						alert('Deleted successfully');
						window.location.reload();
						}, 
				error : function(){
						alert('There was a problem. Please contact administrator');
						window.location.reload();
						}
            });
		}
	}

</script>