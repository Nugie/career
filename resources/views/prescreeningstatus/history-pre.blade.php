@extends('layouts.app')

@section('content')
{{--*/ usort($tableGrid, "SiteHelpers::_sort") /*--}}
<section class="page-header row">
	<h2> {{ $pageTitle }} </h2>
</section>
<div class="page-content row">
	<div class="page-content-wrapper no-margin">

		<div class="sbox">
			<div class="sbox-title">
				<h1> All Records <small> </small></h1>
				<div class="sbox-tools">
					<a href="{{ url('history-prescreening') }}" class="tips btn btn-sm  " title=" {{ __('core.btn_reload') }}" ><i class="fa  fa-refresh"></i></a>	
				</div>				
			</div>
			<div class="sbox-content">
			<div class="row">
				<div class="col-md-12">
					<?php /* <h4>Current Status: @if($statProfile) {{ $statProfile }} @else - @endif</h4> */?>
				</div>
			</div>
			<!-- Table Grid -->
			<div class="table-responsive" style="padding-bottom: 70px;">
			
		    <table class="table table-striped table-hover ">
		        <thead>
					<tr>
						<th style="width: 3% !important;" class="number"> No </th>
						<?php $i = ($rowData->currentPage()-1)* $rowData->perPage();?>
						<th align="center">Apply Date</th>
						<th align="center">Job</th>
						<th align="center">Status</th>
					  </tr>
		        </thead>
				<tbody>
				@if(count($rowData) > 0)
					@foreach ($rowData as $row)
						<tr>
							<td> {{ ++$i }} </td>
							<?php $tempApplyDate = new DateTime($row->apply_date);
										$resApplyDate = $tempApplyDate->format('d/m/Y'); ?>
							<td>{{ $resApplyDate }}</td>
							<td>{{ $row->JobTitleName }}</td>
							<td>{{ $row->status_prescreening }}</td>
						</tr>
					@endforeach
				@else
						<tr>
							<td colspan="5">No data</td>
						</tr>
				@endif
				</tbody>
		    </table>

			<input type="hidden" name="action_task" value="" />
			
			{!! $rowData->links() !!}
			</div>
			<!-- End Table Grid -->


			</div>
		</div>
	</div>
</div>

@stop