<div class="m-t" style="padding-top:25px;">	
    <div class="row m-b-lg animated fadeInDown delayp1 text-center">
        <h3> {{ $pageTitle }} <small> {{ $pageNote }} </small></h3>
        <hr />       
    </div>
</div>
<div class="m-t">
	<div class="table-responsive" > 	

		<table class="table table-striped table-bordered" >
			<tbody>	
		
			
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Job', (isset($fields['id_job']['language'])? $fields['id_job']['language'] : array())) }}</td>
						<td>{{ SiteHelpers::formatLookUp($row->id_job,'id_job','1:acc_job:id:JobTitleName') }} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Id User', (isset($fields['id_user']['language'])? $fields['id_user']['language'] : array())) }}</td>
						<td>{{ $row->id_user}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Skor', (isset($fields['skor_akhir']['language'])? $fields['skor_akhir']['language'] : array())) }}</td>
						<td>{{ $row->skor_akhir}} </td>
						
					</tr>
				
					<tr>
						<td width='30%' class='label-view text-right'>{{ SiteHelpers::activeLang('Status', (isset($fields['status_prescreening']['language'])? $fields['status_prescreening']['language'] : array())) }}</td>
						<td>{{ $row->status_prescreening}} </td>
						
					</tr>
						
					<tr>
						<td width='30%' class='label-view text-right'></td>
						<td> <a href="javascript:history.go(-1)" class="btn btn-primary"> Back To Grid <a> </td>
						
					</tr>					
				
			</tbody>	
		</table>   

	 
	
	</div>
</div>	