<?php
class AccHelpers
{
	public static function acc_validate($id){
		$flag = 0;
		$queryUser=DB::table('tb_users')->leftJoin('tb_groups','tb_users.group_id','=','tb_groups.group_id')
		->whereIn('level', [1,2])->where('tb_users.id', $id)->get();
		
		if(count($queryUser)>=1){
			$flag = 1;
		}
		
		return $flag;
	}
	
	public static function is_user($id) {
		$flag = 0;
		$query=DB::table('tb_users')->leftJoin('tb_groups','tb_users.group_id','=','tb_groups.group_id')
		->where([['tb_users.id', $id],['level', '3']])->get();
		
		if(count($query)>=1){
			$flag = 1;
		}
		
		return $flag;
	}
	
	public static function get_jwb($idsoal){
		$query = \DB::table('acc_jawaban')->where('id_soal',$idsoal)->get();	
		return $query;
	}
	
	public static function get_essay_jwb($idsoal,$iduser,$idjob){
		$query = \DB::table('acc_essay')->where([['id_soal',$idsoal],['id_user',$iduser],['id_job',$idjob]])->first();
		return $query;
	}
	
	public static function pref_career($pref){
		$resTemp = [];
		$stringRes="";
		$res = explode(",",$pref);
		$query = \DB::table('acc_jobfield')->whereIn('id', $res)->get();
		if(count($query)>0){
			foreach($query as $qr){
				$resTemp[] = $qr->jobfield;
			}
			$stringRes = implode(",", $resTemp);
		}
		return $stringRes;
	}
	
	public static function flag_profile($id){
		$stateFlag = false;
		$queryProfile = \DB::table('acc_candidate_profile')->where('id_user',$id)->first();
		$queryEdu = \DB::table('acc_educational_background')->where('id_user',$id)->get();
		
		if(count($queryProfile) == 0){
			$stateFlag = true;
			}  else if(empty($queryProfile->dateofbirth)){
				$stateFlag = true;
				} else if($queryProfile->dateofbirth == '0000-00-00'){
					$stateFlag = true;
					}
		
		if(count($queryEdu) == 0){
			$stateFlag = true;
		}
		
		return $stateFlag;
	}
	
	public static function getUserComment($iduser){
		$query = \DB::table('tb_users')->where('id',$iduser)->first();
		return $query;
	}
	
}