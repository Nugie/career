<?php namespace App\Http\Controllers;

use App\Models\Soalmbti;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 

use App\User;


class SoalmbtiController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'soalmbti';
	static $per_page	= '10';

	public function __construct()
	{		
		parent::__construct();
		$this->model = new Soalmbti();	
		
		$this->info = $this->model->makeInfo( $this->module);	
		$this->data = array(
			'pageTitle'	=> 	!empty($this->info['title']) ? $this->info['title'] : '',
			'pageNote'	=>  !empty($this->info['note']) ? $this->info['note'] : '',
			'pageModule'=> 'soalmbti',
			'return'	=> self::returnUrl()
			
		);
		
	}

	public function index( Request $request )
	{
		// Make Sure users Logged 
		if(!\Auth::check()) 
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		$this->grab( $request) ;
		if($this->access['is_view'] ==0) 
			return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');				
		// Render into template
		return view( $this->module.'.index',$this->data);
	}	

	function create( Request $request , $id =0 ) 
	{
		$this->hook( $request  );
		if($this->access['is_add'] ==0) 
			return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');

		$this->data['row'] = $this->model->getColumnTable( $this->info['table']); 
		
		$this->data['id'] = '';
		return view($this->module.'.form',$this->data);
	}
	function edit( Request $request , $id ) 
	{
		$this->hook( $request , $id );
		if(!isset($this->data['row']))
			return redirect($this->module)->with('message','Record Not Found !')->with('status','error');
		if($this->access['is_edit'] ==0 )
			return redirect('dashboard')->with('message',__('core.note_restric'))->with('status','error');
		$this->data['row'] = (array) $this->data['row'];
		
		$this->data['id'] = $id;
		// dd($this->data['row']['point']);
		return view($this->module.'.form',$this->data);
	}	
	function show( Request $request , $id ) 
	{
		/* Handle import , export and view */
		$task =$id ;
		switch( $task)
		{
			case 'search':
				return $this->getSearch();
				break;
			case 'lookup':
				return $this->getLookup($request );
				break;
			case 'comboselect':
				return $this->getComboselect( $request );
				break;
			case 'import':
				return $this->getImport( $request );
				break;
			case 'export':
				return $this->getExport( $request );
				break;
			default:
				$this->hook( $request , $id );
				if(!isset($this->data['row']))
					return redirect($this->module)->with('message','Record Not Found !')->with('status','error');

				if($this->access['is_detail'] ==0) 
					return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');

				return view($this->module.'.view',$this->data);	
				break;		
		}
	}
	function store( Request $request  )
	{
		$task = $request->input('action_task');
		switch ($task)
		{
			default:
				$rules = $this->validateForm();
				$validator = Validator::make($request->all(), $rules);
				// dd($request);
				if($validator->passes()) 
				{
					//edit
					if(!empty($request->id))
					{
						$cek = Soalmbti::where('point', $request->point)
						->where('jawaban', $request->jawaban)
						->where('id','<>', $request->id)
						->orWhere('jawaban', $request->jawaban)
						->where('id','<>', $request->id)
						->count();
						// dd($cek);
						// $cek2 = Soalmbti::find($request->id);
						if($cek == 0)
						{
							// dd("masuk ini");
							$data = $this->validatePost( $request );
							$id = $this->model->insertRow($data , $request->input( $this->info['key']));
							
							/* Insert logs */
							$this->model->logs($request , $id);
							if(!is_null($request->input('apply')))
								return redirect( $this->module .'/'.$id.'/edit?'. $this->returnUrl() )->with('message',__('core.note_success'))->with('status','success');
		
							return redirect( $this->module .'?'. $this->returnUrl() )->with('message',__('core.note_success'))->with('status','success');
						}
						else
						{
							return redirect($this->module.'/'. $this->returnUrl() )->with('message','Question number '.$request->nomor_soal.' already has that answer')->with('status','error');
						}
					}
					//create
					else
					{
						$cek = Soalmbti::where('nomor_soal', $request->nomor_soal)
								->where('point', $request->point)
								->count();
						if($cek < 1 )
						{
							$data = $this->validatePost( $request );
							$id = $this->model->insertRow($data , $request->input( $this->info['key']));
							
							/* Insert logs */
							$this->model->logs($request , $id);
							if(!is_null($request->input('apply')))
								return redirect( $this->module .'/'.$id.'/edit?'. $this->returnUrl() )->with('message',__('core.note_success'))->with('status','success');
		
							return redirect( $this->module .'?'. $this->returnUrl() )->with('message',__('core.note_success'))->with('status','success');
						}
						else
						{
							return redirect($this->module.'/'. $this->returnUrl() )->with('message','Question number '.$request->nomor_soal.' already has that answer')->with('status','error');
						}
					}
				} 
				else {
					return redirect($this->module.'/'. $request->input(  $this->info['key'] ).'/edit')
							->with('message',__('core.note_error'))->with('status','error')
							->withErrors($validator)->withInput();

				}
				break;
			case 'public':
				return $this->store_public( $request );
				break;

			case 'delete':
				$result = $this->destroy( $request );
				return redirect($this->module.'?'.$this->returnUrl())->with($result);
				break;

			case 'import':
				return $this->PostImport( $request );
				break;

			case 'copy':
				$result = $this->copy( $request );
				return redirect($this->module.'?'.$this->returnUrl())->with($result);
				break;		
		}	
	
	}	

	public function destroy( $request)
	{
		// Make Sure users Logged 
		if(!\Auth::check()) 
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');

		$this->access = $this->model->validAccess($this->info['id'] , session('gid'));
		if($this->access['is_remove'] ==0) 
			return redirect('dashboard')
				->with('message', __('core.note_restric'))->with('status','error');
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
        	return ['message'=>__('core.note_success_delete'),'status'=>'success'];	
	
		} else {
			return ['message'=>__('No Item Deleted'),'status'=>'error'];				
		}

	}	
	
	public static function display(  )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Soalmbti();
		$info = $model::makeInfo('soalmbti');
		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']			
		);	
		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('soalmbti.public.view',$data);			
			}			
		} 
		else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> $info['key'] ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return view('soalmbti.public.index',$data);	
		}

	}
	function store_public( $request)
	{
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost(  $request );		
			 $this->model->insertRow($data , $request->input('id'));
			return  Redirect::back()->with('message',__('core.note_success'))->with('status','success');
		} else {

			return  Redirect::back()->with('message',__('core.note_error'))->with('status','error')
			->withErrors($validator)->withInput();

		}	
	
	}

	public function getSoalMBTI()
	{
			$querySoal = \DB::table('acc_soal_mbti')->groupBy('nomor_soal')->get();
			$querySoalKat1 = \DB::table('acc_soal_mbti')->groupBy('nomor_soal')
			->where('point', 'E')
			->orWhere('point', 'I')
			->get();
			$querySoalKat2 = \DB::table('acc_soal_mbti')->groupBy('nomor_soal')
			->where('point', 'S')
			->orWhere('point', 'N')
			->get();
			$querySoalKat3 = \DB::table('acc_soal_mbti')->groupBy('nomor_soal')
			->where('point', 'T')
			->orWhere('point', 'F')
			->get();
			$querySoalKat4 = \DB::table('acc_soal_mbti')->groupBy('nomor_soal')
			->where('point', 'J')
			->orWhere('point', 'P')
			->get();
			// dd($querySoalKat1);
			$jawab = \DB::table('acc_soal_mbti')->where('point', 'E')
			->orWhere('point', 'I')
			->get();
			$jawab2 = \DB::table('acc_soal_mbti')->where('point', 'S')
			->orWhere('point', 'N')
			->get();
			$jawab3 = \DB::table('acc_soal_mbti')->where('point', 'T')
			->orWhere('point', 'F')
			->get();
			$jawab4 = \DB::table('acc_soal_mbti')->where('point', 'J')
			->orWhere('point', 'P')
			->get();
			$this->data['soalan'] = $querySoal;
			$this->data['soalanKat1'] = $querySoalKat1;
			$this->data['soalanKat2'] = $querySoalKat2;
			$this->data['soalanKat3'] = $querySoalKat3;
			$this->data['soalanKat4'] = $querySoalKat4;
			$this->data['jawaban'] = $jawab;
			$this->data['jawaban2'] = $jawab2;
			$this->data['jawaban3'] = $jawab3;
			$this->data['jawaban4'] = $jawab4;
			$this->data['title'] = 'MBTI';
			$this->data['pages']	= 'soalmbti.show_soal_user';
			$page = 'layouts.'.config('sximo.cnf_theme').'.index';
		// dd($this->data);
		return view($page, $this->data);
	}

	public function saveAnswerMBTI(Request $request)
	{
		if(!\Auth::check())
		{
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		}
		else{
			$hasil = [];
			$hasilFinal1;
			$hasilFinal2;
			$hasilFinal3;
			$hasilFinal4;
			$hasilFinal;
			$E = 0;
			$I = 0;
			$N = 0;
			$S = 0;
			$T = 0;
			$F = 0;
			$J = 0;
			$P = 0;
			foreach($request->all() as $data)
			{
				if($data == "E")
				{
					$E = $E+1;
				}
				else if($data == "I")
				{
					$I = $I+1;
				}
				else if($data == "S")
				{
					$S = $S+1;
				}
				else if($data == "N")
				{
					$N = $N+1;
				}
				else if($data == "T")
				{
					$T = $T+1;
				}
				else if($data == "F")
				{
					$F = $F+1;
				}
				else if($data == "J")
				{
					$J = $J+1;
				}
				else if($data == "P")
				{
					$P = $P+1;
				}
				// dd($data);
			}
			if($I < $E)
			{
				$hasilFinal1 = "E";
			}
			else if($I > $E)
			{
				$hasilFinal1 = "I";
			}
			if($S < $N)
			{
				$hasilFinal2 = "N";
			}
			elseif($S > $N)
			{
				$hasilFinal2 = "S";
			}
			if($T < $F)
			{
				$hasilFinal3 = "F";
			}
			else if($T > $F)
			{
				$hasilFinal3 = "T";
			}
			if($J < $P)
			{
				$hasilFinal4 = "P";
			}
			else if($J > $P)
			{
				$hasilFinal4 = "J";
			}
			$hasil['E']= $E; 
			$hasil['I']= $I;
			$hasil['S']= $S;
			$hasil['N']= $N;
			$hasil['T']= $T;
			$hasil['F']= $F;
			$hasil['J']= $J;
			$hasil['P']= $P;
			$hasil = json_encode($hasil);
			// dd(\Auth::user()->id);
			$hasilFinal = $hasilFinal1.$hasilFinal2.$hasilFinal3.$hasilFinal4;
			// dd($hasil);
			$items = array(
				'id_user' 		=> \Auth::user()->id,	
				'hasil'			=> $hasil,
				'hasil_final'	=> $hasilFinal,
				'date_test'		=> date('Y-m-d H:i:s')
			);
			try{
				\DB::table('acc_hasil_mbti')->insert($items);
				return redirect(url('/show/mbti?type=success'));
			} catch(\Illuminate\Database\QueryException $ex){
				\Log::error("Error submit");
				\Log::error($ex);
				return redirect(url('/show/mbti?type=fail'));
			}
		}
		

		
	}

}
