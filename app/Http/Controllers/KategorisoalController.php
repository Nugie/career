<?php namespace App\Http\Controllers;

use App\Models\Kategorisoal;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Validator, Input, Redirect ; 
use Mail;


class KategorisoalController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'kategorisoal';
	static $per_page	= '10';

	public function __construct()
	{		
		parent::__construct();
		$this->model = new Kategorisoal();	
		
		$this->info = $this->model->makeInfo( $this->module);	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'kategorisoal',
			'return'	=> self::returnUrl()
			
		);
		
	}

	public function index( Request $request )
	{
		// Make Sure users Logged 
		if(!\Auth::check()) 
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		$this->grab( $request) ;
		if($this->access['is_view'] ==0) 
			return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');				
		// Render into template
		return view( $this->module.'.index',$this->data);
	}	

	function create( Request $request , $id =0 ) 
	{
		$this->hook( $request  );
		if($this->access['is_add'] ==0) 
			return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');

		$this->data['row'] = $this->model->getColumnTable( $this->info['table']); 
		
		$this->data['id'] = '';
		return view($this->module.'.custom-form',$this->data);
	}
	function edit( Request $request , $id ) 
	{
		$this->hook( $request , $id );
		if(!isset($this->data['row']))
			return redirect($this->module)->with('message','Record Not Found !')->with('status','error');
		if($this->access['is_edit'] ==0 )
			return redirect('dashboard')->with('message',__('core.note_restric'))->with('status','error');
		$this->data['row'] = (array) $this->data['row'];
		
		$this->data['id'] = $id;
		return view($this->module.'.custom-form',$this->data);
	}	
	function show( Request $request , $id ) 
	{
		/* Handle import , export and view */
		$task =$id ;
		switch( $task)
		{
			case 'search':
				return $this->getSearch();
				break;
			case 'lookup':
				return $this->getLookup($request );
				break;
			case 'comboselect':
				return $this->getComboselect( $request );
				break;
			case 'import':
				return $this->getImport( $request );
				break;
			case 'export':
				return $this->getExport( $request );
				break;
			default:
				$this->hook( $request , $id );
				if(!isset($this->data['row']))
					return redirect($this->module)->with('message','Record Not Found !')->with('status','error');

				if($this->access['is_detail'] ==0) 
					return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');

				return view($this->module.'.view',$this->data);	
				break;		
		}
	}
	function store( Request $request  )
	{
		$task = $request->input('action_task');
		switch ($task)
		{
			default:
				$rules = $this->validateForm();
				$validator = Validator::make($request->all(), $rules);
				if ($validator->passes()) 
				{
					$data = $this->validatePost( $request );
					$id = $this->model->insertRow($data , $request->input( $this->info['key']));
					
					$resultInsert = \DB::table('acc_kategori_soal')->where('id', $id)->first();
					$name = $resultInsert->name;
					$desc =$resultInsert->description;
					$post_data = array(
												'items' => array(
														array("WebCareer_ID__c"=>$id,"Name"=>$name, "Description__c"=>$desc)
												));
		try{
			$client = new \GuzzleHttp\Client();
			$requestJson = $client->post(\Lang::get('acc.loginsf'));
			$responseJson = $requestJson->getBody()->getContents();
			$resultJson = json_decode($responseJson,true);
			if($requestJson->getStatusCode() == "200"){
				$token = $resultJson['access_token'];
				$headers = [
					'Authorization' => 'Bearer ' . $token,        
					'Content-Type'        => 'application/json',
				];
				
				$requestJson2 = $client->post(\Lang::get('acc.postkategorisoal'), [
					'headers' => $headers,
					'json'=>  $post_data,
					]
				);
				
				$responseJson2 = $requestJson2->getBody()->getContents();
				$resultJson2 = json_decode($responseJson2,true);
				if($requestJson2->getStatusCode() == "200"){
					$idExt = $resultJson2['results'][0]['Id'];
					
					try{
						\DB::table('acc_kategori_soal')->where('id',$id)->update(['idExternal' => $idExt]);
					} catch(\Illuminate\Database\QueryException $ex){
						redirect($this->module.'/'. $request->input(  $this->info['key'] ).'/edit')->with('message',__('core.note_error'))->with('status','error');
					}
					
				}
			}	
		} catch(TransferException $e)	{
				return redirect($this->module.'/'. $request->input(  $this->info['key'] ).'/edit')
											->with('message',__('core.note_error'))->with('status','error');			
		} 
					/* Insert logs */
					$this->model->logs($request , $id);
					if(!is_null($request->input('apply')))
						return redirect( $this->module .'/'.$id.'/edit?'. $this->returnUrl() )->with('message',__('core.note_success'))->with('status','success');

					return redirect('prescreening-management')->with('message',__('core.note_success'))->with('status','success');
				} 
				else {
					return redirect($this->module.'/'. $request->input(  $this->info['key'] ).'/edit')
							->with('message',__('core.note_error'))->with('status','error')
							->withErrors($validator)->withInput();

				}
				break;
			case 'public':
				return $this->store_public( $request );
				break;

			case 'delete':
				$result = $this->destroy( $request );
				return redirect($this->module.'?'.$this->returnUrl())->with($result);
				break;

			case 'import':
				return $this->PostImport( $request );
				break;

			case 'copy':
				$result = $this->copy( $request );
				return redirect($this->module.'?'.$this->returnUrl())->with($result);
				break;		
		}	
	
	}	

	public function destroy( $request)
	{
		// Make Sure users Logged 
		if(!\Auth::check()) 
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');

		$this->access = $this->model->validAccess($this->info['id'] , session('gid'));
		if($this->access['is_remove'] ==0) 
			return redirect('dashboard')
				->with('message', __('core.note_restric'))->with('status','error');
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
        	return ['message'=>__('core.note_success_delete'),'status'=>'success'];	
	
		} else {
			return ['message'=>__('No Item Deleted'),'status'=>'error'];				
		}

	}	
	
	public static function display(  )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Kategorisoal();
		$info = $model::makeInfo('kategorisoal');
		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']			
		);	
		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('kategorisoal.public.view',$data);			
			}			
		} 
		else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> $info['key'] ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return view('kategorisoal.public.index',$data);	
		}

	}
	function store_public( $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost(  $request );		
			 $this->model->insertRow($data , $request->input('id'));
			return  Redirect::back()->with('message',__('core.note_success'))->with('status','success');
		} else {

			return  Redirect::back()->with('message',__('core.note_error'))->with('status','error')
			->withErrors($validator)->withInput();

		}	
	
	}
	
	function getCategoryPre(){
		$result = \DB::table('acc_kategori_soal')->paginate(10);
		$this->data['rowData'] = $result;
		$this->data['pages'] = 'kategorisoal.kategorisoal-list';
		$this->data['title'] = 'Category Prescreening Group';
		return view( $this->module.'.kategorisoal-list',$this->data);		
	}
	
	function groupDelete($id){
		try{
		\DB::table('acc_kategori_soal')->where('id', $id)->delete();
		return redirect('prescreening-management')->with('message',__('core.note_success'))->with('status','success');
		} catch (\Illuminate\Database\QueryException $ex){
			return redirect('prescreening-management')->with('message',__('core.note_error'))->with('status','error');
		}
	}
	
	public function  getMail(){
		Mail::send('emails.test-mail', array('Test' => 'Test User'), function ($message) {
			    		$message->to("akoswamin@gmail.com")->subject('Test Test Test');
			    	});	
		/* Mail::send('kategorisoal.email', array('Test' => 'Test User'), function($message)
		{
			$message->to('akoswamin@gmail.com', 'Test')->subject('Welcome ! Mail is sent');
		}); */
		echo "Mail sent";
	}
	
	function testDoc(){
		
		$id = "1";
		$userId =  "a0z0l000000CqJOAA0";
		$company = "Company1";
		$fileUrl = "http://acc.getclarity.online/tesdoc";
		$name="Name1";
		$title = "Dokumen 1";
		$post_data = array(
										'items' => array(
																		array(
																			"WebCareer_ID__c"=> $id,
																			"Applicant_Id__c"=> $userId,
																			"File_Url__c"=> $fileUrl,
																			"Name"=> $name,
																			"Title__c"=> $title
																			)));

        $client = new \GuzzleHttp\Client();
        $request = $client->post('https://test.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9AJuBE3rTYDg6xqNgcfzIt0yKktBvgS_EGAKJUa3FUAE9Ehfq.kjP.d6sOU8loQaSGVsjT2BUu3CRc4Qt&client_secret=5249588143570196746&username=mustika.murni@acc.co.id.dev&password=TalentsProd1238JUREqjoBZJeOochpBOct6jpH');
        $response = $request->getBody()->getContents();
		$result = json_decode($response,true);
		if($request->getStatusCode() == "200"){
			$token = $result['access_token'];
			$headers = [
				'Authorization' => 'Bearer ' . $token,        
				'Content-Type'        => 'application/json',
			];
			
			$request2 = $client->post('https://cs58.salesforce.com/services/apexrest/ApplicantInsertUpdate?SyncObject=REC_APPLICANT_DOCUMENT__c', [
				'headers' => $headers,
				'json'=>  $post_data,
				]
			);
			
			$response2 = $request2->getBody()->getContents();
			$result2 = json_decode($response2,true);
		} else {
			$result2= "Ini bukan 200";
		}
		
        echo '<pre>';
        print_r($result2);
        exit;	
	} 
	
}