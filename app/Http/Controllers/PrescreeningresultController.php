<?php namespace App\Http\Controllers;

use App\Models\Prescreeningresult;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use Illuminate\Support\Facades\Input;
use Validator, Redirect ; 
use DB;


class PrescreeningresultController extends Controller {

	protected $layout = "layouts.main";
	protected $data = array();	
	public $module = 'prescreeningresult';
	static $per_page	= '10';

	public function __construct()
	{		
		parent::__construct();
		$this->model = new Prescreeningresult();	
		
		$this->info = $this->model->makeInfo( $this->module);	
		$this->data = array(
			'pageTitle'	=> 	$this->info['title'],
			'pageNote'	=>  $this->info['note'],
			'pageModule'=> 'prescreeningresult',
			'return'	=> self::returnUrl()
			
		);
		
	}

	public function index( Request $request )
	{
		// Make Sure users Logged 
		if(!\Auth::check()) 
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		$this->grab( $request) ;
		if($this->access['is_view'] ==0) 
			return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');				
		// Render into template
		return view( $this->module.'.index',$this->data);
	}	

	function create( Request $request , $id =0 ) 
	{
		$this->hook( $request  );
		if($this->access['is_add'] ==0) 
			return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');

		$this->data['row'] = $this->model->getColumnTable( $this->info['table']); 
		
		$this->data['id'] = '';
		return view($this->module.'.form',$this->data);
	}
	function edit( Request $request , $id ) 
	{
		$this->hook( $request , $id );
		if(!isset($this->data['row']))
			return redirect($this->module)->with('message','Record Not Found !')->with('status','error');
		if($this->access['is_edit'] ==0 )
			return redirect('dashboard')->with('message',__('core.note_restric'))->with('status','error');
		$this->data['row'] = (array) $this->data['row'];
		
		$this->data['id'] = $id;
		return view($this->module.'.form',$this->data);
	}	
	function show( Request $request , $id ) 
	{
		/* Handle import , export and view */
		$task =$id ;
		switch( $task)
		{
			case 'search':
				return $this->getSearch();
				break;
			case 'lookup':
				return $this->getLookup($request );
				break;
			case 'comboselect':
				return $this->getComboselect( $request );
				break;
			case 'import':
				return $this->getImport( $request );
				break;
			case 'export':
				return $this->getExport( $request );
				break;
			default:
				$this->hook( $request , $id );
				if(!isset($this->data['row']))
					return redirect($this->module)->with('message','Record Not Found !')->with('status','error');

				if($this->access['is_detail'] ==0) 
					return redirect('dashboard')->with('message', __('core.note_restric'))->with('status','error');

				return view($this->module.'.view',$this->data);	
				break;		
		}
	}
	function store( Request $request  )
	{
		$task = $request->input('action_task');
		switch ($task)
		{
			default:
				$rules = $this->validateForm();
				$validator = Validator::make($request->all(), $rules);
				if ($validator->passes()) 
				{
					$data = $this->validatePost( $request );
					$id = $this->model->insertRow($data , $request->input( $this->info['key']));
					
					/* Insert logs */
					$this->model->logs($request , $id);
					if(!is_null($request->input('apply')))
						return redirect( $this->module .'/'.$id.'/edit?'. $this->returnUrl() )->with('message',__('core.note_success'))->with('status','success');

					return redirect( $this->module .'?'. $this->returnUrl() )->with('message',__('core.note_success'))->with('status','success');
				} 
				else {
					return redirect($this->module.'/'. $request->input(  $this->info['key'] ).'/edit')
							->with('message',__('core.note_error'))->with('status','error')
							->withErrors($validator)->withInput();

				}
				break;
			case 'public':
				return $this->store_public( $request );
				break;

			case 'delete':
				$result = $this->destroy( $request );
				return redirect($this->module.'?'.$this->returnUrl())->with($result);
				break;

			case 'import':
				return $this->PostImport( $request );
				break;

			case 'copy':
				$result = $this->copy( $request );
				return redirect($this->module.'?'.$this->returnUrl())->with($result);
				break;		
		}	
	
	}	

	public function destroy( $request)
	{
		// Make Sure users Logged 
		if(!\Auth::check()) 
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');

		$this->access = $this->model->validAccess($this->info['id'] , session('gid'));
		if($this->access['is_remove'] ==0) 
			return redirect('dashboard')
				->with('message', __('core.note_restric'))->with('status','error');
		// delete multipe rows 
		if(count($request->input('ids')) >=1)
		{
			$this->model->destroy($request->input('ids'));
			
			\SiteHelpers::auditTrail( $request , "ID : ".implode(",",$request->input('ids'))."  , Has Been Removed Successfull");
			// redirect
        	return ['message'=>__('core.note_success_delete'),'status'=>'success'];	
	
		} else {
			return ['message'=>__('No Item Deleted'),'status'=>'error'];				
		}

	}	
	
	public static function display(  )
	{
		$mode  = isset($_GET['view']) ? 'view' : 'default' ;
		$model  = new Prescreeningresult();
		$info = $model::makeInfo('prescreeningresult');
		$data = array(
			'pageTitle'	=> 	$info['title'],
			'pageNote'	=>  $info['note']			
		);	
		if($mode == 'view')
		{
			$id = $_GET['view'];
			$row = $model::getRow($id);
			if($row)
			{
				$data['row'] =  $row;
				$data['fields'] 		=  \SiteHelpers::fieldLang($info['config']['grid']);
				$data['id'] = $id;
				return view('prescreeningresult.public.view',$data);			
			}			
		} 
		else {

			$page = isset($_GET['page']) ? $_GET['page'] : 1;
			$params = array(
				'page'		=> $page ,
				'limit'		=>  (isset($_GET['rows']) ? filter_var($_GET['rows'],FILTER_VALIDATE_INT) : 10 ) ,
				'sort'		=> $info['key'] ,
				'order'		=> 'asc',
				'params'	=> '',
				'global'	=> 1 
			);

			$result = $model::getRows( $params );
			$data['tableGrid'] 	= $info['config']['grid'];
			$data['rowData'] 	= $result['rows'];	

			$page = $page >= 1 && filter_var($page, FILTER_VALIDATE_INT) !== false ? $page : 1;	
			$pagination = new Paginator($result['rows'], $result['total'], $params['limit']);	
			$pagination->setPath('');
			$data['i']			= ($page * $params['limit'])- $params['limit']; 
			$data['pagination'] = $pagination;
			return view('prescreeningresult.public.index',$data);	
		}

	}
	function store_public( $request)
	{
		
		$rules = $this->validateForm();
		$validator = Validator::make($request->all(), $rules);	
		if ($validator->passes()) {
			$data = $this->validatePost(  $request );		
			 $this->model->insertRow($data , $request->input('id'));
			return  Redirect::back()->with('message',__('core.note_success'))->with('status','success');
		} else {

			return  Redirect::back()->with('message',__('core.note_error'))->with('status','error')
			->withErrors($validator)->withInput();

		}	
	
	}
	
	public function adminView(Request $request){
		if(!\Auth::check())
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		
		if(!\AccHelpers::acc_validate(\Auth::user()->id))
			return redirect('dashboard')->with('status', 'error')->with('message','You are not allowed to access this page');
		
			$this->data = array(
					'pageTitle'	=> 	'Prescreening Result',
					'pageModule'=> 'prescreeningresult',
					'moduleJob' => 'job',
					'return'	=> self::returnUrl()
			);
			
			//$this->grab( $request) ;
			$queryResult = \DB::table('acc_prescreening_status')
			->leftJoin('acc_job', 'acc_prescreening_status.id_job','=','acc_job.id')
			->leftjoin('acc_placement', 'acc_job.Location','=','acc_placement.id')
			->select('acc_job.id as id_job','acc_job.JobTitleName as Job_Title','acc_placement.name as Placement', 
							DB::raw('SUM( CASE WHEN acc_prescreening_status.status_prescreening = "pending" THEN 1 ELSE 0 END) as "Total_Applicants"'),
							DB::raw('SUM( CASE WHEN acc_prescreening_status.status_prescreening = "disqualified" THEN 1 ELSE 0 END) as "Total_Disqualified"'),
							DB::raw('SUM( CASE WHEN acc_prescreening_status.status_prescreening = "accepted" THEN 1 ELSE 0 END) as "Accepted_Applicants"'),
							DB::raw('SUM( CASE WHEN acc_prescreening_status.status_prescreening = "failed" THEN 1 ELSE 0 END) as "Failed_Applicants"'))
			->groupBy('acc_job.id')
			->paginate(5000);
			
			$sort = 'id';
			$order = 'desc';
			$this->data['rowData'] = $queryResult;
			$this->data['insort'] = $sort;
			$this->data['inorder']	= $order;

			return view( $this->module.'.index-jf',$this->data);
	}

	public function saveAnswer(Request $request){
	  if(!\Auth::check())
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
      
		$skor = array();
		$dis = array();
		foreach($_POST["hasil"] as $Hsl){
			$TempHsl = explode("#",$Hsl);
			$items = array(
							'id_user'		=> \Session::get('uid'),
							'id_soal'		=> $TempHsl[0],
							'id_jawaban'	=> $TempHsl[1],
							'created_date'	=> date('Y-m-d H:i:s'),
							'skor' => $TempHsl[2],
							'id_job'		=> $_POST["idJob"],
							'entry_by'		=> \Session::get('uid'),
							'createdOn'		=> date('Y-m-d H:i:s'),
						);
			array_push($skor,$TempHsl[2]);
			array_push($dis,$TempHsl[3]);
			try{
				\DB::table('acc_prescreening_result')->insert($items);
			} catch(\Illuminate\Database\QueryException $ex){
				\Log::error("Error submit prescreening");
				\Log::error($ex);
			}
		}
		
		if(!empty($_POST["essay"])){
		foreach($_POST["essay"] as $ess){
			$TempHsl = explode("#",$ess);
			$items = array(
							'id_user'		=> \Session::get('uid'),
							'id_soal'		=> $TempHsl[0],
							'jawaban'	=> $TempHsl[1],
							'created_date'	=> date('Y-m-d H:i:s'),
							'id_job'		=> $_POST["idJob"],
						);
			try{
				\DB::table('acc_essay')->insert($items);
			} catch(\Illuminate\Database\QueryException $ex){
				\Log::error("Error submit prescreening");
				\Log::error($ex);
			}
		}
		}
		
		$average = round((array_sum($skor) / count($skor)), 1);
		if(in_array("1",$dis)){
			$statusHsl = "disqualified";
		} else {
			$statusHsl = "pending";
		}
		$skor_result = array(
			'id_job'		=> $_POST["idJob"],
			'id_user'		=> \Session::get('uid'),
			'skor_akhir' => $average,
			'apply_date' => date("Y-m-d H:i:s"),
			'status_prescreening' =>$statusHsl,
		);
		
		try{
		\DB::table('acc_prescreening_status')->insert($skor_result);
		} catch(\Illuminate\Database\QueryException $ex){
				\Log::error("Error submit prescreening");
				\Log::error($ex);
			}
		
		$this->sendEmailPre(\Session::get('uid'), $_POST["idJob"]);
		
		if($statusHsl == "disqualified"){
			$this->sendEmailDis(\Session::get('uid'), $_POST["idJob"]);
		}
		
	}
	
	public function filterSearch(Request $request){
		$jobtitle= Input::get('jobtitle');
		
		if(!\Auth::check())
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		
		if(!\AccHelpers::acc_validate(\Auth::user()->id))
			return redirect('dashboard')->with('status', 'error')->with('message','You are not allowed to access this page');
		
			$this->data = array(
					'pageTitle'	=> 	'Prescreening Result',
					'pageModule'=> 'prescreeningresult',
					'moduleJob' => 'job',
					'return'	=> self::returnUrl()
			);
			
			//$this->grab( $request) ;
			$queryResult = \DB::table('acc_prescreening_status')
			->leftJoin('acc_job', 'acc_prescreening_status.id_job','=','acc_job.id')
			->leftjoin('acc_placement', 'acc_job.Location','=','acc_placement.id')
			->select('acc_job.id as id_job','acc_job.JobTitleName as Job_Title','acc_placement.name as Placement', 
							DB::raw('SUM( CASE WHEN acc_prescreening_status.status_prescreening = "pending" THEN 1 ELSE 0 END) as "Total_Applicants"'),
							DB::raw('SUM( CASE WHEN acc_prescreening_status.status_prescreening = "disqualified" THEN 1 ELSE 0 END) as "Total_Disqualified"'),
							DB::raw('SUM( CASE WHEN acc_prescreening_status.status_prescreening = "accepted" THEN 1 ELSE 0 END) as "Accepted_Applicants"'),
							DB::raw('SUM( CASE WHEN acc_prescreening_status.status_prescreening = "failed" THEN 1 ELSE 0 END) as "Failed_Applicants"'))
			->when($jobtitle, function ($jobtitleQuery) use ($jobtitle) {
					return $jobtitleQuery->where('acc_job.JobTitleName', 'like', '%'.$jobtitle.'%');
				})
			->groupBy('acc_job.JobTitleName')
			->paginate(20);
			
			$sort = 'id';
			$order = 'desc';
			$this->data['rowData'] = $queryResult;
			$this->data['insort'] = $sort;
			$this->data['inorder']	= $order;

			return view( $this->module.'.index-jf',$this->data);		
	}
	
	public function sendEmailPre($id_user, $id_job){
		$applicant = \DB::table('tb_users')->where('id', $id_user)->select('tb_users.first_name','tb_users.last_name','tb_users.email')->first();
		$job = \DB::table('acc_job')->leftJoin('acc_placement','acc_job.Location','=','acc_placement.id')->where('acc_job.id', $id_job)
				->select('acc_job.JobTitleName','acc_placement.name')->first();
		if($applicant && $job){
		$data = array(
                'firstname'	=> $applicant->first_name ,
                'lastname'	=> $applicant->last_name ,
                'email'		=> $applicant->email,
                'job'		=> $job->JobTitleName,
				'placement' => $job->name,
                'subject'	=> "[ " .$this->config['cnf_appname']." ] Thank you for Applying at Astra Credit Companies"
            );
		$message = view('emails.submit-prescreening', $data);
		\Mail::send('emails.submit-prescreening', $data, function ($message) use ($data) {
			    		$message->to($data['email'])->subject($data['subject']);
		});
		\Log::info('email prescreening success');
		}
		return true;
	}
	
	public function sendEmailDis($id_user, $id_job){
		$applicant = \DB::table('tb_users')->where('id', $id_user)->select('tb_users.first_name','tb_users.last_name','tb_users.email')->first();
		$job = \DB::table('acc_job')->leftJoin('acc_placement','acc_job.Location','=','acc_placement.id')->where('acc_job.id', $id_job)
				->select('acc_job.JobTitleName','acc_placement.name')->first();
		if($applicant && $job){
		$data = array(
                'firstname'	=> $applicant->first_name ,
                'lastname'	=> $applicant->last_name ,
                'email'		=> $applicant->email,
                'job'		=> $job->JobTitleName,
				'placement' => $job->name,
                'subject'	=> "[ " .$this->config['cnf_appname']." ] Your Application for ".$job->JobTitleName." at Astra Credit Companies"
            );
		$message = view('emails.failed-prescreening', $data);
		\Mail::send('emails.failed-prescreening', $data, function ($message) use ($data) {
			    		$message->to($data['email'])->subject($data['subject']);
		});
		\Log::info('email prescreening success');
		}
		return true;
	}
	
	public function getResultApplicant($statusRes, $id_user, $id_job){
		if(!\Auth::check())
			return redirect('user/login')->with('status', 'error')->with('message','You are not login');
		
		if(!\AccHelpers::acc_validate(\Auth::user()->id))
			return redirect('dashboard')->with('status', 'error')->with('message','You are not allowed to access this page');
		$infoJob = \DB::table('acc_job')->leftJoin('acc_kategori_soal','acc_job.id_kategori_soal','=','acc_kategori_soal.id')
						->select('acc_job.*','acc_kategori_soal.name')->where('acc_job.id',$id_job)->first();
		$infoUser = \DB::table('tb_users')->where('id',$id_user)->first();
		$preRes = \DB::table('acc_prescreening_result')->where([['acc_prescreening_result.id_job',$id_job],['acc_prescreening_result.id_user',$id_user]])->get();
		$applyDate = \DB::table('acc_prescreening_status')->where([['acc_prescreening_status.id_job',$id_job],['acc_prescreening_status.id_user',$id_user]])->first();
		$setSoal = \DB::table('acc_soal')->leftJoin('acc_kategori_soal','acc_soal.id_kategori_soal','=','acc_kategori_soal.id')
							->leftJoin('acc_job','acc_kategori_soal.id','=','acc_job.id_kategori_soal')
							->select('acc_soal.soal','acc_soal.id as id_soal','acc_soal.type','acc_soal.order_number')
							->orderBy('acc_soal.order_number','asc')->where('acc_job.id',$id_job)->get();
		$this->data['pageTitle'] = "Prescreening Result Applicant";
		$this->data['statusRes'] = $statusRes;
		$this->data['job'] = $infoJob;
		$this->data['user'] = $infoUser;
		$this->data['preRes'] = $preRes;
		$this->data['applyDate'] = $applyDate;
		$this->data['setSoal'] = $setSoal;
		return view( $this->module.'.view-pre-applicant',$this->data);
	}
	
	public function apiGetResultPres(Request $request){
		$user = $request->getUser();
		$pass = $request->getPassword();
		$id_user = $request->id_user;
		$id_job = $request->id_job;
		$result = [];
		$data = array();
		if($user == \Lang::get('acc.userapi') && $pass == \Lang::get('acc.passapi')){
			$infoJob = \DB::table('acc_job')->leftJoin('acc_kategori_soal','acc_job.id_kategori_soal','=','acc_kategori_soal.id')
							->select('acc_job.*','acc_kategori_soal.name')->where('acc_job.id',$id_job)->first();
			$infoUser = \DB::table('tb_users')->where('id',$id_user)->first();
			$setSoal = \DB::table('acc_soal')->leftJoin('acc_kategori_soal','acc_soal.id_kategori_soal','=','acc_kategori_soal.id')
								->leftJoin('acc_job','acc_kategori_soal.id','=','acc_job.id_kategori_soal')
								->select('acc_soal.soal','acc_soal.id as id_soal','acc_soal.type','acc_soal.order_number')
								->orderBy('acc_soal.order_number','asc')->where('acc_job.id',$id_job)->get();
			$preRes = \DB::table('acc_prescreening_result')->where([['acc_prescreening_result.id_job',$id_job],['acc_prescreening_result.id_user',$id_user]])->get();
			
			foreach($setSoal as $itemSoal){
				switch($itemSoal->type){
					case "1": $data['type_soal'] = "checkbox"; break;
					case "2": $data['type_soal'] = "multiple_choice"; break;
					case "3": $data['type_soal'] = "essay"; break;
					default: unset($data['type_soal']);
				}
				$data['order'] = $itemSoal->order_number;
				$data['soal'] = $itemSoal->soal;
					if($itemSoal->type == '2' || $itemSoal->type == '1'){
						$setJawaban =\DB::table('acc_jawaban')->where('id_soal',$itemSoal->id_soal)->get();
						$data['set_jawaban'] = [];
							foreach($setJawaban as $itemJwb){
								$data['set_jawaban'][] = $itemJwb->jawaban;
							}
					} else { unset($data['set_jawaban']); }
					
				if($itemSoal->type == '3'){	
					$essayJwb = \DB::table('acc_essay')->where([['acc_essay.id_job',$id_job],['acc_essay.id_user',$id_user],['acc_essay.id_soal',$itemSoal->id_soal]])->first();
					if(!empty($essayJwb->jawaban)){$data['jawaban'] = $essayJwb->jawaban;} else{ $data['jawaban']=[]; }
				} else { 
					$resJwb = \DB::table('acc_prescreening_result')->leftJoin('acc_jawaban','acc_prescreening_result.id_jawaban','=','acc_jawaban.id')
					->select('acc_prescreening_result.*','acc_jawaban.jawaban')
					->where([['acc_prescreening_result.id_job',$id_job],['acc_prescreening_result.id_user',$id_user],['acc_prescreening_result.id_soal',$itemSoal->id_soal]])->first();
					if(!empty($resJwb->jawaban)){$data['jawaban'] = $resJwb->jawaban;} else{ $data['jawaban']=[]; }
				}
				$result[] = $data;
			}
			
			$response = [
						'success' => true,
						'applicant_name' => $infoUser->first_name." ".$infoUser->last_name,
						'job' => $infoJob->JobTitleName,
						'result' => $result
			];
		} else {
			$response = [
					'success' => false,
					'message' => "Permission denied",
			];
		}
		return response()->json($response,200);
	}

}
