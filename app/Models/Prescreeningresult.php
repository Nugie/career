<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class prescreeningresult extends Sximo  {
	
	protected $table = 'acc_prescreening_result';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_prescreening_result.* FROM acc_prescreening_result  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_prescreening_result.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
