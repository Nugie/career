<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class hasildisc extends Sximo  {
	
	protected $table = 'acc_hasil_disc';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_hasil_disc.* FROM acc_hasil_disc  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_hasil_disc.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
