<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class contactus extends Sximo  {
	
	protected $table = 'acc_contactus';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_contactus.* FROM acc_contactus  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_contactus.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
