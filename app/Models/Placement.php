<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class placement extends Sximo  {
	
	protected $table = 'acc_placement';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_placement.* FROM acc_placement  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_placement.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
