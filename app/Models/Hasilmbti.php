<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class hasilmbti extends Sximo  {
	
	protected $table = 'acc_hasil_mbti';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_hasil_mbti.* FROM acc_hasil_mbti  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_hasil_mbti.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
