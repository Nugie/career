<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class postcategory extends Sximo  {
	
	protected $table = 'acc_post_category';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_post_category.* FROM acc_post_category  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_post_category.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
