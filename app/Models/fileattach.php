<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class fileattach extends Sximo  {
	
	protected $table = 'acc_file_attachment';
	protected $primaryKey = 'id_file';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_file_attachment.* FROM acc_file_attachment  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_file_attachment.id_file IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
