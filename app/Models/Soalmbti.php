<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class soalmbti extends Sximo  {
	
	protected $table = 'acc_soal_mbti';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_soal_mbti.* FROM acc_soal_mbti  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_soal_mbti.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
