<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class soaldisc extends Sximo  {
	
	protected $table = 'acc_soal_disc';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_soal_disc.* FROM acc_soal_disc  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_soal_disc.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
