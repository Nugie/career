<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;

class jobfield extends Sximo  {
	
	protected $table = 'acc_jobfield';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		return "  SELECT acc_jobfield.* FROM acc_jobfield  ";
	}	

	public static function queryWhere(  ){
		
		return "  WHERE acc_jobfield.id IS NOT NULL ";
	}
	
	public static function queryGroup(){
		return "  ";
	}
	

}
