<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccPrescreeningResult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_prescreening_result', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_user');
            $table->integer('id_soal');
            $table->integer('id_jawaban');
            $table->timestamps('created_date');
            $table->integer('skor');
            $table->integer('id_job');
            $table->string('entry_by');
            $table->datetime('createdOn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_prescreening_result');
    }
}
