<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablesCandidateProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('acc_candidate_profile', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('id_user');
            $table->integer('id_fileattachment');
            $table->enum('title_profile',['Mr','Mrs','Ms']);
            $table->enum('gender',['Female', 'Male']);
            $table->date('dateofbirth',50);
            $table->string('provinceofbirth',50);
            $table->string('cityofbirth',50);
            $table->integer('identitycardnumber');
            $table->integer('homephone');
            $table->integer('mobilephone');
            $table->enum('maritalstatus',['Single','Married','Divorced','Widowed']);
            $table->enum('religion', ['Moslem','Buddha','Catholic','Christian','Konghucu','Hindu','Others']);
            $table->string('country',50);
            $table->string('address',100);
            $table->string('subdisctrict',50);
            $table->string('cityorregency',50);
            $table->string('province',50);
            $table->string('postalcode',50);
            $table->string('Careerpreference',50);
            $table->integer('expectedsalaryfrom');
            $table->integer('expectedsalaryupto');
            $table->enum('additionalinformation', ['Acc Employees', 'Campus / University ', 'Company / Brand Website', 'Print Advertisement','Friends / Relatives', 'Others']);
            $table->string('ApplicantStatus',50);
            
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('acc_candidate_profile');
    }
}
