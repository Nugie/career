<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccFileAttachment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('acc_file_attachment', function (Blueprint $table) {
            
            $table->increments('id_file');
            $table->string('suratlamaran',100);
            $table->string('cv',100);
            $table->string('ijazah',100);
            $table->string('transkripnilai',100);
            $table->string('sertifikattraining',100);
            $table->string('suratketerangankerja',100);
            $table->string('skck',100);
            $table->string('ktp',100);
            $table->string('sim',100);
            $table->string('paspor',100);
            $table->string('aktenikah',100);
            $table->string('kartukeluarga',100);
            $table->string('akte_kelahiran_diri_sendiri',100);
            $table->string('akte_kelahiran_anak_1',100);
            $table->string('akte_kelahiran_anak_2',100);
            $table->string('akte_kelahiran_anak_3',100);
            $table->string('npwp',100);
            $table->string('bpjs',100);
            $table->string('poto_Latar_belakang_orange',100);
            $table->string('nomor_rekening_permata',100);
            $table->string('surat_pernyataan_orang_tua',100);
        });      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('acc_file_attachment');
    }
}
