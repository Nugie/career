<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccFaqApplicant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //
        Schema::create('acc_faq_applicant', function (Blueprint $table) {
            $table->increments('Id');
            $table->dateTime('CreatedDate');
            $table->string('ApplicantFaqQuestion');
            $table->string('ApplicantFaqAnswer');
            $table->string('Image');
            $table->enum('CategoryFaq', ['Admin','Applicant']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('acc_faq_applicant');
    }
}
