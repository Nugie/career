<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccJawaban extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_jawaban', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_soal');
            $table->text('jawaban');
            $table->enum('is_benar',['0','1', '2']);
            $table->double('skor');
            $table->string('entry_by');
            $table->datetime('createdOn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_jawaban');
    }
}
