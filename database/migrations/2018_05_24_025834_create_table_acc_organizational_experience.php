<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccOrganizationalExperience extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
              
        Schema::create('acc_organizational_experience', function (Blueprint $table) {
            
            $table->increments('id_org_exp');
            $table->integer('id_user');
            $table->string('organizationname',50);
            $table->enum('organizationscope',['International','National','Region','University','faculty/major','Others']);
            $table->date('organization_experience_startdate',50);
            $table->date('organization_experience_endate',50);
            $table->string('rolefunction',50);
            $table->string('roleorposition',50);
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('acc_organizational_experience');
    }
}
