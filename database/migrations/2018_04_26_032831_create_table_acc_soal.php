<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccSoal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acc_soal', function (Blueprint $table) {
            $table->increments('id');
            $table->text('soal');
            $table->integer('id_kategori_soal');
			$table->enum('type', ['1', '2']);
            $table->string('entry_by');
            $table->datetime('createdOn');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acc_soal');
    }
}
