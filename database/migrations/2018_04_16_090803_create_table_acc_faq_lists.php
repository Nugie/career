<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAccFaqLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('acc_faq', function (Blueprint $table) {
            $table->increments('Id');
            $table->dateTime('CreatedDate');
            $table->string('FaqTitleName');
            $table->string('FaqShort');
            $table->string('Image');
        });
            
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('acc_faq');
    }
}
